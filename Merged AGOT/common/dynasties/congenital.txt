5700144 = {
	name = ""
	culture = western_valyrian
	coat_of_arms = {
		template = 0
		layer = {
			texture = 23
			texture_internal = 0
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
5700145 = {
	name = "Drace"
	culture = western_valyrian
	divine_blood=yes
	coat_of_arms = {
		template = 0
		layer = {
			texture = 23
			texture_internal = 1
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
99999 = {
	name="Burke"
	culture = valeman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 23
			texture_internal = 2
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}