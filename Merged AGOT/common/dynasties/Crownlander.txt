## Crownlander ##
##non canon for random##
803000 = {
	name = Buckshorn
	culture = crownlander
}
803001 = {
	name = Redwyck
	culture = crownlander
}
803002 = {
	name = Fishwell
	culture = crownlander
}
803003 = {
	name = Newhall
	culture = crownlander
}
803004 = {
	name = Hendy
	culture = crownlander
}
803005 = {
	name = Pickwood
	culture = crownlander
}
803006 = {
	name = Lychfield
	culture = crownlander
}
803007 = {
	name = Durgood
	culture = crownlander
}
803008 = {
	name = Grindell
	culture = crownlander
}
803009 = {
	name = Knollwood
	culture = crownlander
}
803010 = {
	name = Hagtree
	culture = crownlander
}
803011 = {
	name = Gorland
	culture = crownlander
}
803012 = {
	name = Merion
	culture = crownlander
}
803013 = {
	name = Blakyston
	culture = crownlander
}
803014 = {
	name = Brindlewood
	culture = crownlander
}
803015 = {
	name = Crower
	culture = crownlander
}
803016 = {
	name = Claughton
	culture = crownlander
}
803017 = {
	name = Glenshaw
	culture = crownlander
}
803018 = {
	name = Gotwyck
	culture = crownlander
}
803019 = {
	name = Hallamore
	culture = crownlander
}
803020 = {
	name = Shearer
	culture = crownlander
}
803021 = {
	name = Kingwood
	culture = crownlander
}
803022 = {
	name = Gullet
	culture = crownlander
}
803023 = {
	name = Emmerwyck
	culture = crownlander
}
803024 = {
	name = Whitrose
	culture = crownlander
}
803025 = {
	name = Brock
	culture = crownlander
}
174814 = {
	name="Tascer" #non-canon
	culture = crownlander
}
174815 = {
	name="Farrest" #non-canon
	culture = crownlander
}
174816 = {
	name="Brewlan" #non-canon
	culture = crownlander
}
174817 = {
	name="Smithe" #non-canon
	culture = crownlander
}
174818 = {
	name="Malver" #non-canon
	culture = crownlander
}
##canon##
# 803026 = {
	# name = "of the Shady Glen"
	# culture = crownlander
# }
1515 = {
	name = "Byrne"
	culture = crownlander
}
1516 = {
	name = "of the Hollow Hill"
	culture = crownlander
	used_for_random = no
}
1517 = {
	name = Bentley
	culture = crownlander
}
1518 = {
	name = Lansdale
	culture = crownlander
}
1519 = {
	name = Leek
	culture = crownlander
}
1520 = {
	name = "Deem"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 82
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1521 = {
	name="Cargyll"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 41
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1522 = {
	name="Blackberry"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 52
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1523 = {
	name="Suggs"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 53
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1524 = {
	name="Tall"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 54
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1442 = {
	name="Tallyhill"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 50
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1543 = {
	name="Winters"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 51
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1525 = {
	name="Straw"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 49
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
502 = {
	name="Hammer"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 70
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
503 = {
	name="White"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 67
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1526 = {
	name="Tanner"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 58
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1527 = {
	name="Foxglove"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 72
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1528 = {
	name="Darke"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 59
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1529 = {
	name="Darkwood"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 60
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1530 = {
	name="Dargood"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 61
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1531 = {
	name="Quince"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 63
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1532 = {
	name="Largent"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 73
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1533 = {
	name="Correy"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 74
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1534 = {
	name="Goode"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 75
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
501 = {
	name="Truefyre"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 76
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1535 = {
	name="Belgrave"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 77
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1514 = {
	name="Bush"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 78
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1513 = {
	name="Bean"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 79
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1512 = {
	name="of Blackhull"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 80
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1511 = {
	name="Poore"
	culture = crownlander
}
504 = {
	name="Farseed"	# Nettles
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 71
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
21 = {
	name="Follard"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 0
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
22 = {
	name="Blount"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 1
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
23 = {
	name="Gaunt"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 2
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
24 = {
	name="Farring"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 3
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
25 = {
	name="Chelsted"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 4
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
26 = {
	name="Mallery"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 5
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
27 = {
	name="Cressey"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 6
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
28 = {
	name="Chyttering"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 7
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
29 = {
	name="Rollingford"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 8
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
30 = {
	name="Thorne"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 9
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
31 = {
	name="Hogg"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 10
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
32 = {
	name="Hayford"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 11
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
33 = {
	name="Harte"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 12
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
34 = {
	name="Buckwell"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 13
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
35 = {
	name="Stokeworth"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 14
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
36 = {
	name="Rykker"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 15
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
37 = {
	name="Hollard"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 16
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
38 = {
	name="Rosby"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 17
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
39 = {
	name="Byrch"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 18
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
40 = {
	name="Edgerton"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 19
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
41 = {
	name="Staunton"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 20
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}

43 = {
	name="Manning"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 26
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
44 = {
	name="Wendwater"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 22
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
45 = {
	name="Bywater"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 23
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
46 = {
	name="Massey"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 24
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}

48 = {
	name="Langward"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 35
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
55 = {
	name="Pyle"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 25
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
352 = {
	name="Kettleblack"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 36
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
353 = {
	name="Slynt"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 55
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
364 = {
	name="Darklyn"
	culture="crownlander"
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 38
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1440 = {
	name="Blackwater"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 37
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1472 = {
	name="Bourney"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 69
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1473 = {
	name="Corne"		
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 66
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1474 = {
	name="Hall"	
	culture = crownlander
}
1475 = {
	name="Yore"	
	culture = crownlander
}
1480 = {
	name="True"	
	culture = crownlander
}
1575 = {
	name="Baratheon-Lannister"
	culture = crownlander
	used_for_random = no
	can_appear = no
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 68
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
9110 = {
	name="Seaworth"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 12
			texture_internal = 29
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1873 = {
	name="Branfield"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 7
			texture_internal = 80
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
777777 = {
	name="" #Used for random/lowborn high septons
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 57
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1499 = {
	name = "of Dragonstone"
	culture = crownlander
}
# 174807 = {
	# name="Broome"				#Propably obsolete. Broomes are just Brooms with typo -kuczaja
	# culture = crownlander
	# coat_of_arms = {
		# template = 0
		# layer = {
			# texture = 10
			# texture_internal = 62
			# emblem = 0
			# color = 0
			# color = 0
			# color = 0
		# }
	# }
# }
1498 = {
	name = "Bullock"
	culture = crownlander
}
1497 = {
	name = "of Duskendale"
	culture = crownlander
}
1496 = {
	name = "Wormwood"
	culture = crownlander
}
1479 = {				
	name = "Scales"		
	culture = western_valyrian
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 81
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1470 = {
	name="Sweet"			#Canon, one lord with Selyse
	culture = crownlander
	used_for_random = no
	can_appear = no
}
1495 = {
	name="of the Reeds"
	culture=crownlander
}
1494 = {
	name="Stillman"
	culture=crownlander
}
1493 = {
	name="Wheaton"
	culture=crownlander
}
1492 = {
	name="Stafford"
	culture=crownlander
}
1464 = {
	name="Moontown" # Dolen of Moontown
	culture=crownlander
}
1465 = {
	name="Misty Wood" # Will of the Misty Wood
	culture=crownlander
}
1466 = {
	name="Sharp Point" # Malaen of Sharp Point
	culture=crownlander
}
1509 = {
	name = "Tall"
	culture = hedge
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 56
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}

1510 = {
	name = "Strong"
	culture = hedge
}