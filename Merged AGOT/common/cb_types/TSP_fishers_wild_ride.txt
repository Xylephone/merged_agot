# Thousand Islanders can attack any neighbor for any reason

TSP_fishermans_wild_ride= {
	name = CB_NAME_COUNTY_CONQUEST
	war_name = WAR_NAME_COUNTY_CONQUEST
	sprite = 16
	truce_days = 800
	hostile_against_others = yes
	is_permanent = yes
	check_all_titles = yes # if permanent, setting this to true will check against all of someones titles, including vassal held titles
	can_ask_to_join_war = no
	allowed_to_target_tributaries = yes
	
	sort_priority = 700
	
	can_use_gui = {
		always = yes
	}
	
	on_add = {
		ROOT = { piety = -50 }
		hidden_tooltip = { fire_haruspicy_event_effect = yes }
	}
	
	can_use = {
		universal_cb_restrictions_excl_NW_KG_trigger = yes
		ROOT = {
			religion_group = islands_rel
			NOT = { is_liege_or_above = FROM }
		}
	}

	can_use_title = {
		tier = count
		OR = {
			FROM = {
				has_landed_title = PREV
			}
			holder_scope = {
				is_liege_or_above = FROM
			}
		}
		
#		ROOT = {
#			religion_group = islands_rel
#		}

		NOT = {
			OR = {
				ROOT = {
					has_landed_title = PREV
				}
				holder_scope = {
					is_liege_or_above = ROOT
				}
			}
		}
		
		location = {
			any_neighbor_province = {
				owner = {
					OR = {
						character = ROOT
						is_liege_or_above = ROOT
					}
				}
			}
		}

		NOT = { #Cannot be used to take nomadic territory or ruins
			location = { 
				NOT = { 
					any_province_holding = {
						holder_scope = { NOT = { culture_group = unoccupied_group } }
						OR = {
							holding_type = castle
							holding_type = city
						}
					}
				}
			}	
		}
	}

	is_valid_title = {
		OR = {
			FROM = {
				has_landed_title = PREV
			}
			holder_scope = {
				is_liege_or_above = FROM
			}
		}
	}

	on_success = {
		hidden_tooltip = { ROOT = { add_character_modifier = { name = victory_timer duration = 3 } } }
	}

	on_success_title = {
		if = {
			limit = {
				holder_scope = {
					tier = count
					NOT = { num_of_count_titles = 2 }
					lower_tier_than = ROOT
				}
			}

			holder_scope = {
				set_defacto_liege = ROOT
			}
		}

		if = {
			limit = {
				holder_scope = {
					OR = {
						higher_tier_than = count
						num_of_count_titles = 2
						NOT = { lower_tier_than = ROOT }
					}
				}
			}

			usurp_title_plus_barony_if_unlanded = { target = ROOT type = invasion }
			any_de_jure_vassal_title = { # take all baronies under the one we're fighting for
				limit = {
					has_holder = yes
					NOT = {
						de_facto_liege = PREV
					}
					holder_scope = {
						OR = {
							character = FROM
							is_liege_or_above = FROM
						}
					}
				}

				usurp_title_plus_barony_if_unlanded = { target = ROOT type = invasion }
			}
		}
		ROOT = {
			participation_scaled_prestige = 100
		}
		any_attacker = {
			limit = { NOT = { character = ROOT } }
			hidden_tooltip = { participation_scaled_prestige = 300 }
		}
	}

	on_fail_title = {
		ROOT = {
			prestige = -100
		}
		FROM = {
			participation_scaled_prestige = 200
		}
		any_defender = {
			limit = { NOT = { character = FROM } }
			hidden_tooltip = { participation_scaled_prestige = 300 }
		}
	}

	on_reverse_demand = {
		ROOT = {
			prestige = -300
			transfer_scaled_wealth = {
				to = FROM
				value = 5
			}
		}
		FROM = {
			participation_scaled_prestige = 100
		}
		any_defender = {
			limit = { NOT = { character = FROM } }
			hidden_tooltip = { participation_scaled_prestige = 100 }
		}
	}
	
	attacker_ai_victory_worth = {
		factor = -1 # always accept
	}
	
	attacker_ai_defeat_worth = {
		factor = 100
	}

	defender_ai_victory_worth = {
		factor = -1 # always accept
	}
	
	defender_ai_defeat_worth = {
		factor = 100
	}
	
	ai_will_do = { 
		factor = 1
		
		modifier = {
			factor = 0.9 # Prefer using CB's without on_add costs
		}
	}
}
