########################################################
# MINOR TITLES
# Characters can have any number of these
########################################################
#	grant_limit: Max number of holders.
#	realm_in_name: Append the name of the country after the title (Queen [of Sweden])
#	allowed_to_hold: conditions for the character to hold the title
#	allowed_to_grant: conditions for a ruler to grant the title
#	regent: This is the title for regents (not really "minor")
#	gain_effect: fires when the title is granted
#	lose_effect: fires when a title is lost through most causes
#	retire_effect: fires when a title is lost due to the character stepping down
#	death_effect: fires when a title is lost due to the character dying
#	message: should fire a message to the its owner when set
#	The other fields are all character modifiers.

title_court_singer_mbs = {
	
	dignity = 0.05
	grant_limit = 1
	opinion_effect = 10
	show_as_title = no
	revoke_allowed = yes
	attribute = diplomacy
	
	monthly_salary = 0.1
	monthly_prestige = 0.15
	
	allowed_to_hold = {
		trait = poet
		dynasty = 0
	}
	allowed_to_grant = {
		is_ruler = yes
		demesne_size = 1
		OR = {
			culture_group = andal
			culture_group = dornish
			culture_group = first_men
			culture_group = free_folk
			culture_group = iron_isles_culture
			primary_title = {
				capital_scope = { region = world_westeros }
			}
		}
	}
	ai_will_do = {
		factor = 100
	}
	
	gain_effect = {
	}
	lose_effect = {
		opinion = {
			who = FROM
			modifier = opinion_fired_from_council
		}
	}

	message = yes	
}
title_kennelmaster_mbs = {
	
	dignity = 0.05
	grant_limit = 1
	opinion_effect = 10
	show_as_title = no
	revoke_allowed = yes
	attribute = martial
	
	monthly_salary = 0.1
	monthly_prestige = 0.15
	
	allowed_to_hold = {
		trait = hunter
		dynasty = 0
	}
	allowed_to_grant = {
		is_nomadic = no
		is_tribal = no
		OR = {
			is_feudal = yes
			trait = nightswatch
		}
		OR = {
			culture_group = andal
			culture_group = dornish
			culture_group = first_men
			culture_group = free_folk
			culture_group = iron_isles_culture
			primary_title = {
				capital_scope = { region = world_westeros }
			}
		}
	}
	ai_will_do = {
		factor = 100
	}
	
	gain_effect = {
	}
	lose_effect = {
		opinion = {
			who = FROM
			modifier = opinion_fired_from_council
		}
	}

	message = yes	
}
