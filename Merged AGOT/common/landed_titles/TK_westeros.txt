#### Titular Kingdoms - Riverlands ####
k_thecrossingTK = {
	color = { 11 65 144 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_thecrossing }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 77 # The Twins
	creation_requires_capital = no
	culture = riverlander
}

k_oldstonesTK = {
	color = { 88 3 137 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_oldstones }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 79 # Seagard
	creation_requires_capital = no
	culture = riverlander
}

k_tridentTK = {
	color = { 161 62 57 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_trident }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}
	
	capital = 94 # Green Fork
	creation_requires_capital = no
	culture = riverlander
}

k_blackwoodTK = {
	color = { 3 24 4 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_blackwood }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 90 # Blackwood Vale
	creation_requires_capital = no
	culture = riverlander
}

k_riverrunTK = {
	color = { 0 0 191 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_riverrun }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 101 # Riverrun
	creation_requires_capital = no

	culture = riverlander

	title = "riverking_male"
	title_female = "riverking_female"
}

k_southstoneTK = {
	color = { 127 130 130 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_southstone }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 91 # Stone Hedge
	creation_requires_capital = no
	culture = riverlander
}

k_acornsridgeTK = {
	color = { 229 162 158 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_acornsridge }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}
	
	capital = 111 # Acorn Hall
	creation_requires_capital = no
	culture = riverlander

}

k_wayfarersrestTK = {
	color = { 11 65 144 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_wayfarersrest }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 105 # Watfarer's Rest
	creation_requires_capital = no
	culture = riverlander
}

k_harrenhalTK = {
	color = { 134 26 190 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_harrenhal }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 107 # Harrenhall
	creation_requires_capital = no
	culture = riverlander
}

k_bayofclawsTK = {
	color = { 89 81 58 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_bayofclaws }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 109 # Maidenpool
	creation_requires_capital = no
	culture = riverlander
}

k_willowwoodTK = {
	color = { 50 0 120 }
	color2 = { 130 80 110 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_willowwood }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 108 # Willow Wood
	creation_requires_capital = no
	culture = riverlander	
}

#### Titular Kingdoms - Iron Isles ####
k_great_wykTK = {
	color = { 235 72 71 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_great_wyk }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
		#
	}
	
	capital = 152 # Hammerhorn
	creation_requires_capital = no
	
	culture = ironborn
	
	
}
k_harlawTK = {
	color = { 48 26 60 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_harlaw }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
		#
	}
	
	capital = 158 # Ten Towers
	creation_requires_capital = no
	
	culture = ironborn
	
	
}
k_seastone_islesTK = {
	color = { 29 103 219 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_seastone_isles }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
		#
	}
	
	capital = 154 # Old Wyk
	creation_requires_capital = no
	
	culture = ironborn
	
	
}

#### Titular Kingdoms - Westerlands ####
k_casterly_rockTK = {
	color = { 246 10 10 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_casterly_rock }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 182 # Casterly Rock
	creation_requires_capital = no
	
	culture = westerman
	
	
}
k_crakehallTK = {
	color = { 129 79 3 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_crakehall }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 197 # Crakehall
	creation_requires_capital = no
	
	culture = westerman
	
	
}
k_cornfieldTK = {
	color = { 219 197 2 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_cornfield }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 199 # Cornfield
	creation_requires_capital = no
	
	culture = westerman
	
	
}
k_silverhillTK = {
	color = { 180 186 161 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_silverhill }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 195 # Silverhall
	creation_requires_capital = no
	
	culture = westerman
	
	
}
k_goldroadTK = {
	color = { 156 130 39 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_goldroad }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 194 # Payne Hall
	creation_requires_capital = no
	
	culture = westerman
	
	
}
k_hornvaleTK = {
	color = { 136 7 7 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_hornvale }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 180 #Hornvale
	creation_requires_capital = no
	
	culture = westerman
	
	
}
k_kayceTK = {
	color = { 245 138 18 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_kayce }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 169 # Kayce
	creation_requires_capital = no
	
	culture = westerman	
}
k_fair_isleTK = {
	color = { 245 138 18 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_fair_isle }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}
	
	capital = 167 # Fair Isle
	#creation_requires_capital = no
	
	culture = westerman	
}
k_golden_toothTK = {
	color = { 233 196 26 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_golden_tooth }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 174 # Golden Tooth
	creation_requires_capital = no
	
	culture = westerman
	
	
}
k_ashemarkTK = {
	color = { 225 109 0 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_ashemark }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 173 # Ashemark
	creation_requires_capital = no
	
	culture = westerman
	
	
}

k_the_cragTK = {
	color = { 186 163 120 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_the_crag }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 164 # The Crag
	creation_requires_capital = no
	
	culture = westerman
}
#### Titular Kingdoms - Crownlands ####

#k_dragonstoneTK = {
#	color = { 153 20 120 }
#	color2 = { 255 255 255 }
#
#	allow = {
#		num_of_duke_titles = 2
#		independent = yes
#		primary_title = { title = d_dragonstone }
#		primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
#	}
#
#	capital = 221 # Dragonstone
#	creation_requires_capital = no
#
#	culture = crownlander
#
#	title = "dragonking_male"
#	title_female = "dragonking_female"
#}

#k_crackclaw_pointTK = {
#	color = { 205 171 1 }
#	color2 = { 255 255 255 }
#	
#	allow = {
#		num_of_duke_titles = 1
#		independent = yes
#		primary_title = { title = d_crackclaw_point }
#		primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
#	}
#	
#	capital = 214 # Dyre Den
#	creation_requires_capital = no
#	culture = crackclawmen
#}

k_kings_landingTK = {
	color = { 141 75 155 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_kings_landing }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 226 # Kings Landing
	creation_requires_capital = no
	
	culture = crownlander
	
	
}

k_blackwater_rushTK = {
	color = { 129 211 247 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_blackwater_rush }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}

	capital = 204 # Rayonet
	creation_requires_capital = no

	culture = crownlander
}

k_kingswoodTK = {
	color = { 99 199 106 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_kingswood }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 231 # Bywater
	creation_requires_capital = no
	
	culture = crownlander
	
	
}
k_masseys_hookTK = {
	color = { 1 71 163 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_masseys_hook }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
		#
	}
	
	capital = 233 # Stonedance
	creation_requires_capital = no
	
	culture = crownlander
	
	
}

#### Titular Kingdoms - Reach ####
k_northmarchTK = {
	color = { 15 116 219 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_northmarch }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 238 # Coldmoat
	creation_requires_capital = no
	
	culture = reachman
	
	
}
k_oldtownTK = {
	color = { 83 83 83 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_oldtown }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
		#
	}
	
	capital = 286 # Oldtown
	creation_requires_capital = no
	
	culture = reachman
	
	
}
k_the_arborTK = {
	color = { 40 46 140 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_the_arbor }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 292 # The Arbor
	creation_requires_capital = no
	
	culture = reachman
	
	
}
k_brightwaterTK = {
	color = { 189 67 58 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_brightwater }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 274 # Brightwater
	creation_requires_capital = no
	
	culture = reachman
	
	
}
k_west_marchesTK = {
	color = { 118 151 198 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_west_marches }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 280 # Horn Hill
	creation_requires_capital = no
	
	culture = reachman
	
	
}
k_cockleswhentTK = {
	color = { 63 167 149 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_cockleswhent }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 270 # Ashford
	creation_requires_capital = no
	
	culture = reachman
	
	
}
k_blueburnTK = {
	color = { 122 172 201 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_blueburn }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 259 # Grassy Vale
	creation_requires_capital = no
	
	culture = reachman
	
	
}
k_roseroadTK = {
	color = { 254 62 100 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_roseroad }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 253 # Bitterbridge
	creation_requires_capital = no
	
	culture = reachman
	
	
}
k_tumbletonTK = {
	color = { 87 189 190 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_tumbleton }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 249 # Tumbleton
	creation_requires_capital = no
	
	culture = reachman
	
	
}
k_torrentpeakTK = {
	color = { 13 58 33 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_torrentpeak }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 250 # The Ring
	creation_requires_capital = no
	
	culture = reachman
	
	
}
k_goldengroveTK = {
	color = { 241 172 26 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_goldengrove }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 243 # Goldengrove
	creation_requires_capital = no
	culture = reachman
}
k_red_lakeTK = {
	color = { 193 70 70 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_red_lake }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 236 # Red Water Keep
	creation_requires_capital = no
	culture = reachman
}
k_ocean_roadTK = {
	color = { 117 169 179 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_ocean_road }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 235 # Old Oak
	creation_requires_capital = no
	culture = reachman
}
k_the_shield_islesTK = {
	color = { 127 220 230 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_the_shield_isles }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 263 # Oak Castle
	creation_requires_capital = no
	culture = reachman
}
k_mandervaleTK = {
	color = { 12 37 131 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_mandervale }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 268 # Cider Hall
	creation_requires_capital = no
	culture = reachman
}

#### Titular Kingdoms - The Stormlands ####
k_shipbreakerTK = {
	color = { 215 217 52 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_shipbreaker }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}
	
	capital = 304 # Storm's End
	creation_requires_capital = no
	
	culture = stormlander
	
	title = "stormking"
	title_female = "stormking_female"
}
k_wendwaterTK = {
	color = { 41 1 184 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_wendwater }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}

	capital = 296 # Bronzegate
	creation_requires_capital = no
	culture = stormlander
}

k_summerhallTK = {
	color = { 107 12 8 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_summerhall }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 310 # Gallowsgrey
	creation_requires_capital = no
	
	culture = stormlander	
}

k_dornish_marchesTK = {
	color = { 255 253 229 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_dornish_marches }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 307 # Nightsong
	creation_requires_capital = no
	
	culture = stormlander
	
	
}
k_red_watchTK = {
	color = { 200 8 8 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_red_watch }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 316 # Stonehelm
	creation_requires_capital = no
	
	culture = stormlander
	
	
}
k_cape_wrathTK = {
	color = { 36 137 176 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_cape_wrath }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 326 # Estermon
	creation_requires_capital = no
	
	culture = stormlander
	
	
}
k_rainwoodTK = {
	color = { 27 134 50 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_rainwood }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 320 # Rainwood
	creation_requires_capital = no
	
	culture = stormlander
	
	
}

####Titular Kingdoms - Dorne ####
k_brokenarmTK = {
	color = { 254 128 0 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_brokenarm }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 352 # Sunspear
	creation_requires_capital = no
	
	culture = salt_dornish
	
	
	# title = "DORNEPRINCE"
	# title_female = "DORNEPRINCE_female"
	# foa = "DORNEPRINCE_FOA"
	
}
k_ghosthillsTK = {
	color = { 235 210 233 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_ghosthills }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 343 # The Tor
	creation_requires_capital = no
	
	culture = salt_dornish
	
	
	# title = "DORNEPRINCE"
	# title_female = "DORNEPRINCE_female"
	# foa = "DORNEPRINCE_FOA"
	
}
k_vaithTK = {
	color = { 35 69 29 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_vaith }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}
	
	capital = 349 # Godsgrace
	creation_requires_capital = no
	
	culture = sand_dornish
	
	
	# title = "DORNEPRINCE"
	# title_female = "DORNEPRINCE_female"
	# foa = "DORNEPRINCE_FOA"
	
}
k_redmountainsTK = {
	color = { 198 3 3 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { 
			OR = {
				title = d_redmountains
				title = d_princespass
				title = d_stoneway
			}	
		}
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}

	}

	capital = 331 #Kingsgrave
	creation_requires_capital = no
	culture = stone_dornish

	# title = "DORNEPRINCE"
	# title_female = "DORNEPRINCE_female"
	# foa = "DORNEPRINCE_FOA"
}
k_blackmontTK = {
	color={ 130 20 0 }
	color2={ 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		#primary_title = { title = c_blackmont }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
		#
	}
	
	capital = 327 #Blackmont
	#creation_requires_capital = no
	
	culture = stone_dornish
	
	# title = "DORNEPRINCE"
	# title_female = "DORNEPRINCE_female"
	# foa = "DORNEPRINCE_FOA"
		
}
k_princespassTK = {
	color = { 244 164 105 }
	color2 = { 255 255 255 }
	
	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_princespass }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
		#
	}
	
	capital = 333 # Skyreach
	creation_requires_capital = no
	
	culture = stone_dornish
	
	
	# title = "DORNEPRINCE"
	# title_female = "DORNEPRINCE_female"
	# foa = "DORNEPRINCE_FOA"
	
}

k_stonewayTK = {
	color = { 224 204 123}
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_stoneway }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 334 # Yronwood
	creation_requires_capital = no
	culture = stone_dornish

	# title = "DORNEPRINCE"
	# title_female = "DORNEPRINCE_female"
	# foa = "DORNEPRINCE_FOA"
}