summer_islands = {
	graphical_cultures = { africangfx } # buildings

	summer_islander = {
		graphical_cultures = { summerislandergfx westafricangfx muslimgfx }
		color = { 0.0 0.6 0.0 } #Green

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			# Canon
			Balaq:200 Bokokko:500
			Jalabhar:1000
			Malthar:500
			Quhuru:500
			Tal:300
			Xaro:300 Xhoan:300 Xhondo:1000

			# Non-Canon
			Abiodun Abrafo Adebimpe Adedamola Aderounke Adeyemi Adeyemu Afolabi Alabhar Allos Altharo Anulowaposi Anyim Arkamun
			Babajide Babatunde Balabhar Ballarros Banara Bapato Baqual Bhalontar Bharral Bharrala Bhoralthar Bobhas Bolhabhos Bollolhol Bolos Buhuru
			Calabhar Chakoro Chibundo
			Dabas Dadhor Dalabhar Dalaxaar Dalhobos Dallalhor Dalthar Danaba Danadhar Danoquas Delmabhar Dhalla Dhallar Dhobol Dokokko Dolhonara Dolzhallar Donaba Dorral Doshuru Dubaku
			Ekundayo Enitan Eskender
			Gatubhar Gwalahuru
			Isingodo Isodo
			Jadharror Jalahuru Jallo Jallol Jallono Janaba Jarado Jaramogo Jaros Jhabar Jiradhar Jobhal Johuru Jokokko Jonaba Joros
			Kalabhar Kokokko Konde Koroxhar Kuhuru Kwasdo
			Labhar Lalthar Laro Lhoan Lhondo Lonaba Lontar Luhuru Lusaxharo
			Maballa Mabar Mabhor Madhaquar Makhan Malabhar Manahuru Manos Marallar Maras Maro Marral Mermesbhar Mhondo Mokokko Molhonor Morror
			Nalabhar Nuhuru Nyadundo
			Odingaxho Oladele Olanrewaju Oluhuru Olujondo Oluwarantimi Oluwatoyin Omobolanle Orengo
			Paro Punuxharo
			Qal Qalabhar Qalthar Qaro Qhondo Qubhar
			Radharo Ralabhar Ratemo Rudo
			Sadharo Salabhar Salhontal Sallal Salor Salthbor Sango Santarro Santonar Saros Siboxhoan Simbhar Sobhar Sokokko Solhonar Sololhor Solor Solthoquo Sorronar
			Tadhontor Talabhar Talhonar Talla Tallaquol Talthar Tanaba Tanal Tapuhuru Tarras Teniayo Thabo Thoan Thondo Tokokko Tolar Tolhar Tolhor Tollo Tolor Toralhal Torra Tuhuru
			Udo
			Xabo Xadha Xala Xaladaar Xaladhor Xalar Xalla Xalloba Xaros Xhakan Xhal Xhallar Xhelimo Xhonodo Xhononar Xhoquar Xhoquolhor Xobas Xobha Xolhabba Xolhonar Xuhuru
			Yalabhar
			Zabaho Zabhar Zalar Zalor Zantas Zarralha Zhabar Zhadhollar Zhala Zhalla Zhanar Zhobaba Zhoraral Zolhallar Zoltharror Zonar Zontar
		}
		female_names = {
			#Canon
			Alayaya:700
			Chatana:500 Chataya:700
			Kojja:700
			Xanda:500

			#Non-Canon
			Abraya Arjana
			Banda Bapaya Bhatana Bhataya Bujiba
			Chadha Chakoya Chalayaya Chasa Chijja
			Dhalaya Dhatana Dhataya Doshura Dubaya Dumisa
			Emejja
			Fojja Fulataya
			Gebhuya Ghataya Gwahura Gwanda
			Ihataya Isoba
			Jalahura Jalataya Jarada Jhataya Joza
			Khalaya Khataya Kobijja Koraxha Kwasya
			Landa Larana Lhatana Lhataya Lhayaya Lodhara Lujja Lusala
			Makya Malayaya Manahura Manda Manyara Marrara Mhatana
			Natana Nola Nolta
			Oluhura Onyaya
			Palaya Paya Phatana Phataya Pojja
			Qhatana Qojja Quanda Qubya Quhura
			Rana Raradha Raraza Rarrasa Rhatana Rojja Rollaza
			Sala Sanda Shanika Shatana Shataya Simbaya Sojja
			Talyaya Tanta Tataya Tatenda Tayaya Tojja Tolla Tolorra Totara
			Uda Ujja
			Xajja Xara Xatalha Xatalla Xataya Xha Xharra Xhataya Xhola Xhonda Xhonorra Xhotana Xojja Xola Xolla
			Zadhata Zalonta Zhadhata Zhataya Zhonda
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0

		seafarer = yes

		modifier = default_culture_modifier
	}
}

sothoryos_islands = {
	graphical_cultures = { africangfx } # buildings

	basilisk_islander = {
		graphical_cultures = { summerislandergfx africangfx muslimgfx }
		color = { 0.0 0.4 0.0 } #Green

		used_for_random = no #Removed from the mod, retained for save compatibility
		allow_in_ruler_designer = no

		alternate_start = {
			has_alternate_start_parameter = { key = culture value = full_random }
			NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } }
		}

		male_names = {
			# Canon
			Tumco:500

			# Non-Canon
			Abiodun Abo Abrafo Adebimo Adeboye Adedo Aderoko Adesanya Adeyemu Adeymo Adeyo Adisa Afolo Akande Antimo Anulo Anyim Arkamun Ayomido Ayotunde
			Babado Babo Bapato
			Chakoro Chibundo
			Dayo Delmabhar Doshuru Dubaku Durosin
			Ekund Eniolo Enitan Eskender
			Folami
			Gatubhar Gboyega Gwalahuru
			Ifedapo Ifedayo Iodowu Isingodo Isodo
			Jabari Jalahuru Jarado Jaramogo Jelani Jibola Jiradhar
			Kayin Kayode Kolawole Konde Koroxhar Kwasdo
			Lano Lusaxharo
			Makhan Manahuru Mermesbhar Molo Morounkola
			Nyadundo
			Odingaxho
			Oladelo Olanro Olatunji Oluhuru Olujondo Oluwak Oluwar Oluwat Omobo Omolayo Omowale Orengo Oyin
			Paro Punuxharo
			Qaro Qubhar
			Ratemo Rudo
			Sango Siboxhoan Simbhar Sunbol Sunkanmi
			Tapuhuru Tejumola Temidare Temilo Teniayo Thabo Tiwalade Tokunbo Tundo
			Udo Unayo
			Waju Wapos
			Xhakan Xhal Xhelimo Xhonodo
			Zuberi Zumco
		}
		female_names = {
			# Canon

			# Non-Canon
			Abi Abraya Adebi Aderi Adetola Akintoye Ani Anituke Anuwani Arjana Aya Ayatani
			Bapaya Bola Boli Boluwatife Bosedi Boyi Bujiba
			Chakoya Chijja
			Doshura Dubaya Dumisa
			Emejja Enika Esanya
			Feyisa Fiayosemi Fulataya
			Gbolahun Gebhuya Gwahura
			Ibola Ifeda Impe Ireti Isoba Itana Iyabi
			Jalahura Jalataya Jarada Jiboli
			Kejja Kobijja Kolapi Koniya Koraxha Kunle Kunumi Kwasya
			Lujja Lusala
			Makya Manahura Manyara Miula Mojisole Moni Moniade Morenike Moruna
			Numi
			Oba Olola Ololade Oluhura Oluwa Oluwafemi Oluwakana Oluwaya Omo Omota Omotanwa Oni Onyaya Oya
			Pami Paya Pojja
			Quanda Qubya Quhura
			Sejja Shanika Simbaya Sunbola
			Talyaya Tatenda Taya Temi Tenia Titilaya
			Uda Ujja Unmise Unumi
			Xajja Xara Xataya Xha Xhonda
			Yewande
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0

		seafarer = yes

		modifier = default_culture_modifier
	}

	brindlemen = {
		graphical_cultures = { brindlegfx }
		color = { 0.5 0.7 0.2 } #Green

		used_for_random = no
		allow_in_ruler_designer = no

		alternate_start = {
			has_alternate_start_parameter = { key = culture value = full_random }
			NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } }
		}

		male_names = {
			Ajousux
			Bagdud Baugh Borug Buggug Burghed Burub
			Chagoth Choqha Chux
			Derthag Durgat Durz Durzub
			Ekako
			Faghig Fidgug
			Gaakt Garekk Ghamonk Ghorz Gnabadug Gomatug Grung
			Hejaug Heszah Hibub
			Ignatz Ignorg
			Jhakesu Jikzo Juesesh
			Karrghed Kharzug Kilug
			Mazhug Mogak Mug Murkub
			Nagakh Najahn Naukhaq Neguh Neshuhgu Nuchoh Nunshu
			Oakgu Oggugat Oglub Otugbu
			Parfu Pehrakgu
			Rijush Roukru Rugas Ruxag
			Sagoc Saka Sauggu Shagar Shagdub Shamob Sheszeth Shugru Sikhas Sorgulg Sulgha
			Thaqese Thigik Thougras
			Ug Uhnuq Urbul Urgran Urim Uxus Uzul
			Vamugbu Vetorkag Vigdug Viggu Viguka Vulug
			Worthag
			Xaqhut Xeqeth Xothkug Xuga Xugarf Xujut Xutjja
			Yaukraq Yegeh Yeke Yoguk
			Zaaz Zarod Zehga Zezakua Zinsbog Zonagh Zorgulg Zulmthu Zunza Zurgha Zurpigig
		}
		female_names = {
			Agrobi Araobi
			Badbog Bolar Bor Borgakh Borgakha Bula Bulak Bulfim Bulfimi Bumph Buruba Burzobi
			Dulug Durgati Durza
			Gashnakh Ghak Ghaki Gharol Gharoli Ghob Ghorza Glasha Globi Gluronk Grati Gula Gulfim
			Homraz
			Kharzuga
			Lagakh Lagakhi Lambug Lazgar
			Mazoga Moga Mogak Mogaka Mor Morn Murzush Murzushi
			Nargoli
			Oghasa Oghash Orbula
			Ragash Ragasha Razobi Rogbati Rogmesh Rogmesha Rulfim
			Shadbaka Shagar Shagara Shagdubi Sharamph Sharn Sharog Shazgob Shel Shelura Snaki
			Ugak Ugor Ulumpha Urog Urogi Urzoth Ushat Ushug
			Yazgash Yotul Yotuli
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0

		modifier = backward_culture_modifier
	}
}

naath_group = {
	graphical_cultures = { africangfx } # buildings

	naathi = {
		graphical_cultures = { naathigfx africangfx } # portraits
		color = { 0.1373 0.8235 0.4118 } #blue

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			# Canon
			Marselen:500 Mossador:500

			# Non-Canon
			Arselen Artelen Artellar Athanis Atylen
			Barselen Bilsaran Bossador
			Carran Carselen Cossador
			Edelen Edellan Esselador
			Gallendor Garselen Gossador
			Harselen Hilsaran Hossador Hotellar
			Ibran Issand Issandor
			Janselen Jarselen Jartelen Jartellar Jilan Jossador
			Karselen Kartellar Kilsaran Kissador Kossador Kosstellar
			Lossador
			Mardelan Marselon Marssala Martelen Martellar Massador Massodar Mersalan Mertallar Messador Messudan Milsaran Mosstellar Mossudan Mostelen
			Narselen Nartellara Nossador
			Ossador
			Retelen Retellar Reyan Rissador Rissend
			Vilsaran Vissador
			Wissador Wistelar
		}
		female_names = {
			# Canon
			Missandei:500

			# Non-Canon
			Adelya Aleriara Alyrelle Amradei Assadai Athanissai Athessei Atylrra
			Bassadorei Bersendei
			Carrai Charissai
			Darrisana
			Edellana Essadorei Esselana Evelendai Eysania
			Fissana
			Gallenai Gisellara
			Hilania Hyssanda
			Ibranei Isoronei Issacharis Issandei
			Janalaena Jilana Jismedei Jissendai
			Khissandei
			Laisendai Lissandei
			Macharissei Mahtarnei Maidelena Maisinia Manyllei Mariheria Marivyna Marselenei Massadei Melicia Meliryn Merelendra Miasellenai Millansia Missendai Mossadorai Mysalana
			Nannalyn Neritine Nissandei Nyssa
			Orissanda Orrisei
			Reyana Rissanda Rissendai Ryssa
			Unalaeria Urilei
			Yssendai
		}
		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0

		modifier = default_culture_modifier
	}
}