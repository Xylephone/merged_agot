castle = {
	ca_tsp_thousand_islands_shipyard_1 = {
		desc = ca_tsp_thousand_islands_shipyard_1_desc	
		potential = {
			port = yes
			OR = {
				province_id = 834
				FROM = { 
					OR = {
						culture = islander
						religion = fish_gods
					}	
				}
				FROMFROM = {
					OR = {
						culture = islander
						religion = fish_gods
					}
				}
			}
		}

		is_active_trigger = {
			port = yes
			OR = {
				province_id = 834 # The Thousand Islands will always have a basic shipyard
				FROM = { 
					OR = {
						culture = islander
						religion = fish_gods
						religion = pirate
						religion = corsair
						government = pirate_government
						is_lady_of_the_waves_pirate_religion_trigger = yes
					}	
				}
				FROMFROM = {
					OR = {
						culture = islander
						religion = fish_gods
					}
				}
			}
		}
		gold_cost = 75
		build_time = 150
		galleys = 20
		archers = 100
		light_infantry = 200
		heavy_infantry = 60
		ai_creation_factor = 110
		tax_income = 0.5
		fort_level = 1
		trigger = { TECH_CASTLE_CONSTRUCTION = 0 }
		add_number_to_name = no
		extra_tech_building_start = 10 # Spawns only on the Thousand Islands
	}

	ca_tsp_thousand_islands_shipyard_2 = {
		desc = ca_tsp_thousand_islands_shipyard_2_desc	
		potential = {
			port = yes
			OR = {
				FROM = { 
					OR = {
						culture = islander
						religion = fish_gods
					}	
				}
				FROMFROM = {
					OR = {
						culture = islander
						religion = fish_gods
					}
				}
			}
		}

		is_active_trigger = {
			port = yes
			OR = {
				FROM = { 
					OR = {
						culture = islander
						religion = fish_gods
						religion = pirate
						religion = corsair
						government = pirate_government
						is_lady_of_the_waves_pirate_religion_trigger = yes
					}	
				}
				FROMFROM = {
					OR = {
						culture = islander
						religion = fish_gods
					}
				}
			}
		}
		upgrades_from = ca_tsp_thousand_islands_shipyard_1
		gold_cost = 300
		build_time = 365
		galleys = 20
		archers = 150
		light_infantry = 360
		heavy_infantry = 120
		ai_creation_factor = 110
		tax_income = 0.85	
		fort_level = 1
		trigger = { TECH_CASTLE_CONSTRUCTION = 0 }
		add_number_to_name = no
		extra_tech_building_start = 10.0 # Spawns only on the Thousand Islands
	}

	ca_tsp_thousand_islands_shipyard_3 = {
		desc = ca_tsp_thousand_islands_shipyard_3_desc
		potential = {
			port = yes
			OR = {
				FROM = { 
					OR = {
						culture = islander
						religion = fish_gods
					}	
				}
				FROMFROM = {
					OR = {
						culture = islander
						religion = fish_gods
					}
				}
			}
		}

		is_active_trigger = {
			port = yes
			OR = {
				FROM = { 
					OR = {
						culture = islander
						religion = fish_gods
						religion = pirate
						religion = corsair
						government = pirate_government
						is_lady_of_the_waves_pirate_religion_trigger = yes
					}	
				}
				FROMFROM = {
					OR = {
						culture = islander
						religion = fish_gods
					}
				}
			}
		}
		upgrades_from = ca_tsp_thousand_islands_shipyard_2
		gold_cost = 400
		build_time = 365
		galleys = 25
		archers = 200
		light_infantry = 600
		heavy_infantry = 200
		ai_creation_factor = 110
		tax_income = 1.25
		fort_level = 1
		trigger = { TECH_CASTLE_CONSTRUCTION = 0 }
		add_number_to_name = no
		extra_tech_building_start = 10.0 # Spawns only on the Thousand Islands
	}

	ca_tsp_thousand_islands_shipyard_4 = {
		desc = ca_tsp_thousand_islands_shipyard_4_desc
		potential = {
			port = yes
			OR = {
				province_id = 834
				FROM = { 
					OR = {
						culture = islander
						religion = fish_gods
					}	
				}
				FROMFROM = {
					OR = {
						culture = islander
						religion = fish_gods
					}
				}
			}
		}

		is_active_trigger = {
			port = yes
			OR = {
				FROM = { 
					OR = {
						culture = islander
						religion = fish_gods
						religion = pirate
						religion = corsair
						government = pirate_government
						is_lady_of_the_waves_pirate_religion_trigger = yes
					}	
				}
				FROMFROM = {
					OR = {
						culture = islander
						religion = fish_gods
					}
				}
			}
		}
		upgrades_from = ca_tsp_thousand_islands_shipyard_3
		gold_cost = 650
		build_time = 365
		galleys = 30
		archers = 300
		light_infantry = 800
		heavy_infantry = 240
		ai_creation_factor = 110
		tax_income = 1.25
		fort_level = 2
		trigger = { TECH_CASTLE_CONSTRUCTION = 0 }
		add_number_to_name = no
		extra_tech_building_start = 10.0 # Spawns only on the Thousand Islands
	}

	ca_tsp_thousand_islands_shipyard_5 = {
		desc = ca_tsp_thousand_islands_shipyard_5_desc
		potential = {
			port = yes
			OR = {
				FROM = { 
					OR = {
						culture = islander
						religion = fish_gods
					}	
				}
				FROMFROM = {
					OR = {
						culture = islander
						religion = fish_gods
					}
				}
			}
		}

		is_active_trigger = {
			port = yes
			OR = {
				FROM = { 
					OR = {
						culture = islander
						religion = fish_gods
						religion = pirate
						religion = corsair
						government = pirate_government
						is_lady_of_the_waves_pirate_religion_trigger = yes
					}	
				}
				FROMFROM = {
					OR = {
						culture = islander
						religion = fish_gods
					}
				}
			}
		}

		upgrades_from = ca_tsp_thousand_islands_shipyard_4
		gold_cost = 900
		build_time = 365
		galleys = 30
		archers = 500
		light_infantry = 1000
		heavy_infantry = 500
		ai_creation_factor = 110
		tax_income = 2
		fort_level = 2
		trigger = { TECH_CASTLE_CONSTRUCTION = 0 }
		add_number_to_name = no
		extra_tech_building_start = 10.0 # Spawns only on the Thousand Islands
	}
}