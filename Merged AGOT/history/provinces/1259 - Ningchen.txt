# 1162  - Ningchen

# County Title
title = c_ningchen

# Settlements
max_settlements = 4

b_ningchen_castle = castle
b_ningchen_city1 = city
b_ningchen_temple = temple
b_ningchen_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_ningchen_castle = ca_asoiaf_yiti_basevalue_1
	b_ningchen_castle = ca_asoiaf_yiti_basevalue_2

	b_ningchen_city1 = ct_asoiaf_yiti_basevalue_1

}
	