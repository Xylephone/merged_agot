# 1216  - Duyun

# County Title
title = c_duyun

# Settlements
max_settlements = 4

b_duyun_castle = castle
b_duyun_city1 = city
b_duyun_temple = temple
b_duyun_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_duyun_castle = ca_asoiaf_yiti_basevalue_1
	b_duyun_castle = ca_asoiaf_yiti_basevalue_2

	b_duyun_city1 = ct_asoiaf_yiti_basevalue_1
	b_duyun_city1 = ct_asoiaf_yiti_basevalue_2

}
	