# 1196 - Narrowshade

# County Title
title = c_narrowshade

# Settlements
max_settlements = 3
b_dutton = castle

# Misc
culture = old_first_man
religion = old_gods

#History

1.1.1 = {
	b_dutton = ca_asoiaf_vale_basevalue_1
	b_dutton = ca_asoiaf_vale_basevalue_2
	b_dutton = ca_asoiaf_vale_basevalue_3
}
6700.1.1 = {
	culture = valeman
	religion = the_seven
}
