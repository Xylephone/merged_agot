# 777  - Linhan

# County Title
title = c_linhan

# Settlements
max_settlements = 7

b_wangtian_castle = castle
b_linhan_city1 = city
b_linhan_temple = temple
b_linhan_city2 = city
b_linhan_castle1 = city
b_linhan_castle2 = city
b_linhan_castle3 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_wangtian_castle = ca_asoiaf_yiti_basevalue_1
	b_wangtian_castle = ca_asoiaf_yiti_basevalue_2
	b_wangtian_castle = ca_asoiaf_yiti_basevalue_3
	b_wangtian_castle = ca_asoiaf_yiti_basevalue_4
	b_wangtian_castle = ca_asoiaf_yiti_basevalue_5
	b_wangtian_castle = ca_wangtian_castle

	b_linhan_city1 = ct_asoiaf_yiti_basevalue_1
	b_linhan_city1 = ct_asoiaf_yiti_basevalue_2
	b_linhan_city1 = ct_asoiaf_yiti_basevalue_3
	b_linhan_city1 = ct_asoiaf_yiti_basevalue_4

	b_linhan_city2 = ct_asoiaf_yiti_basevalue_1
	b_linhan_city2 = ct_asoiaf_yiti_basevalue_2
	b_linhan_city2 = ct_asoiaf_yiti_basevalue_3
	b_linhan_city2 = ct_asoiaf_yiti_basevalue_4

	b_linhan_castle1 = ct_asoiaf_yiti_basevalue_1

	b_linhan_castle2 = ct_asoiaf_yiti_basevalue_1

	b_linhan_castle3 = ct_asoiaf_yiti_basevalue_1

}
	