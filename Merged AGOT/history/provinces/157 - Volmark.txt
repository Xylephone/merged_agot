# 157 - Volmark

# County Title
title = c_volmark

# Settlements
max_settlements = 2
b_volmark = castle

#b_great_harbour



# Misc
culture = old_ironborn
religion = drowned_god

#History

1.1.1 = {b_volmark = ca_asoiaf_ironislands_basevalue_1}
1.1.1 = {b_volmark = ca_asoiaf_ironislands_basevalue_2}
1.1.1 = {b_volmark = ca_asoiaf_ironislands_basevalue_3}
1.1.1 = {b_volmark = ca_asoiaf_ironislands_basevalue_4}

6700.1.1 = {
	culture = ironborn
}
