# 1203  - Hengying

# County Title
title = c_hengying

# Settlements
max_settlements = 4

b_hengying_castle = castle
b_hengying_city1 = city
b_hengying_temple = temple
b_hengying_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_hengying_castle = ca_asoiaf_yiti_basevalue_1
	b_hengying_castle = ca_asoiaf_yiti_basevalue_2

	b_hengying_city1 = ct_asoiaf_yiti_basevalue_1
	b_hengying_city1 = ct_asoiaf_yiti_basevalue_2

}
	