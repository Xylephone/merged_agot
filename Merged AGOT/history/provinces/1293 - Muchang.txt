# 1196  - Muchang

# County Title
title = c_muchang

# Settlements
max_settlements = 4

b_muchang_castle = castle
b_muchang_city1 = city
b_muchang_temple = temple
b_muchang_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_muchang_castle = ca_asoiaf_yiti_basevalue_1
	b_muchang_castle = ca_asoiaf_yiti_basevalue_2

	b_muchang_city1 = ct_asoiaf_yiti_basevalue_1
	b_muchang_city1 = ct_asoiaf_yiti_basevalue_2

}
	