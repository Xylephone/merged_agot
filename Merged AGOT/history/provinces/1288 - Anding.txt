# 1191  - Anding

# County Title
title = c_anding

# Settlements
max_settlements = 4

b_anding_castle = castle
b_anding_city1 = city
b_anding_temple = temple
b_anding_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_anding_castle = ca_asoiaf_yiti_basevalue_1
	b_anding_castle = ca_asoiaf_yiti_basevalue_2

	b_anding_city1 = ct_asoiaf_yiti_basevalue_1
	b_anding_city1 = ct_asoiaf_yiti_basevalue_2

}
	