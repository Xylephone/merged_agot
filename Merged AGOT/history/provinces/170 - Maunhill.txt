# 170 - Maunhill

# County Title
title = c_maunhill

# Settlements
max_settlements = 5
b_maunhill = castle
b_maunhilltown = city
#b_maunhillsept = temple
b_maunhilltowntwo = castle
b_maunhilltownthree = city

# Misc
culture = old_first_man
religion = old_gods

#History

1.1.1 = {
	b_maunhill = ca_asoiaf_westerland_basevalue_1
	b_maunhill = ca_asoiaf_westerland_basevalue_2
	b_maunhill = ca_asoiaf_westerland_basevalue_3
	b_maunhill = ca_silvermine

	b_maunhilltown = ct_asoiaf_westerland_basevalue_1
	b_maunhilltown = ct_asoiaf_westerland_basevalue_2
	b_maunhilltown = ct_asoiaf_westerland_basevalue_3

	b_maunhilltowntwo = ca_asoiaf_westerland_basevalue_1
	b_maunhilltowntwo = ca_asoiaf_westerland_basevalue_2
	b_maunhilltowntwo = ca_asoiaf_westerland_basevalue_3

	b_maunhilltownthree = ct_asoiaf_westerland_basevalue_1
	b_maunhilltownthree = ct_asoiaf_westerland_basevalue_2
}

6700.1.1 = {
	culture = westerman
	religion = the_seven
}