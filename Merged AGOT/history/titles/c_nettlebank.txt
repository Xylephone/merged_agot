1.1.1 = {
	liege="d_orkmont"
	law = agnatic_succession
	law = succ_primogeniture
}

#House Hoare
6584.1.1 = { holder = 359066 } #Harras  Hoare
6605.1.1 = { holder = 360066 } #Wulfgar  Hoare
6639.1.1 = { holder = 361066 } #Harrag Hoare (NC)
6648.1.1 = { holder = 362066 } #Euron II Hoare (NC)
6671.1.1 = { holder = 363066 } #Horgan  Hoare
6692.1.1 = { holder = 3163066 } #Balon VIII Hoare (NC)
6734.1.1 = { holder = 0 }
7458.1.1 = { holder = 364066 } #Fergon  Hoare
7470.1.1 = { holder = 365066 } #Othgar I Hoare
7518.1.1 = { holder = 366066 } #Othgar II Hoare
7539.1.1 = { holder = 367066 } #Craghorn  Hoare
7547.1.1 = { holder = 368066 } #Harmund I Hoare
7598.1.1 = { holder = 369066 } #Harmund II Hoare
7612.1.1 = { holder = 370066 } #Harmund III Hoare
7615.1.1 = { holder = 371066 } #Hagon  Hoare
7622.1.1 = { holder = 0 }
7784.1.1 = { holder = 193066 } # Harrald Half-Drowned
7815.1.1 = { holder = 191066 } # Harrald II
7861.1.1 = { holder = 344123 } # Waldon (nc)
7867.1.1 = { holder = 343123 } # Gelmarr (nc)
7880.1.1 = { holder = 11066 } # Qhorwyn
7901.1.1 = { holder = 342123 } # Greydon (nc)
7912.1.1 = { holder = 341123 } # Torgon (nc)
7923.1.1 = { holder = 11366 } # Harwyn
7956.1.1 = { holder = 11466 } # Harrock
7948.1.1 = { holder = 340123 } # Torwold (nc)
7960.1.1 = { holder = 11566 } # Harren (C)
7973.1.1 = { holder = 6005123 } # Toron (nc)

#House Tawney
7998.2.1 = {
	liege = "k_iron_isles"
	holder = 6005123 # Toron (nc)
}
8006.1.1 = { holder = 6000123 } # Urragon (nc)
8020.1.1 = { holder = 6002123 } # Dykk (nc)
8035.1.1 = { holder = 6007123 } # Gyles (nc)
8080.1.1 = { holder = 6009123 } # Cragorn (nc)
8121.1.1 = { holder = 6011123 } # Tarle (nc)
8137.1.1 = { holder = 6016123 } # Eldiss (nc)
8149.1.1 = { holder = 6020123 } # Gevin (nc)
8194.1.1 = { holder = 6021123 } # Rook (nc)
8207.1.1 = { holder = 6027123 } # Gynir (nc)
8236.1.1 = { holder = 3000123 }
8289.1.1 = { liege = "e_iron_isles" }
8289.6.1 = { liege = "k_iron_isles" }
8299.3.4 = { liege = "e_iron_isles" }