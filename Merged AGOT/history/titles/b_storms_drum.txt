7831.1.1 = { 
	holder=100311 # Durran (nc)
	effect = {
		holder_scope = {
			set_dynasty_flag = is_cadet_house
			c_3021544 = { #durrandon father
				set_dynasty_flag = has_cadet_house
				set_dynasty_flag = owned_cadet_house_@PREV 
				ROOT = { holder_scope = { set_dynasty_flag = cadet_house_@PREVPREV } }
			}
		}
	}
} 
7882.1.1={
	holder=132332
}
7910.1.1={
	holder=131332
}
7931.1.1={
	holder=130332
}
7954.1.1={
	holder=129332
}
7970.1.1={
	holder=100332
}
7986.1.1 = { holder=101332 } # Jon (nc)
8020.1.1 = { holder=103332 } # Arlan (nc)
8036.1.1 = { holder=106332 } # Theo (C)
8047.1.1 = { holder=123332 } # Renly (nc)
8104.1.1 = { holder=112332 } # Orys (nc)
8141.1.1 = { holder=114332 } # Cortnay (nc)
8147.1.1 = { holder=119332 } # Lomas (nc)
8167.1.1 = { holder=121332 } # Robert (nc)
8192.1.1 = { holder=2000332 } # Florian (nc)
8230.1.1 = { holder=3001332 } # Ormund Bolling (nc)
8264.1.1 = { holder=3004332 } # Harwood Bolling (nc)
8278.1.1 = { holder=3005332 } # Joffrey Bolling (nc)
8292.1.1 = { holder=3008332 } # Harlan Bolling (nc)