islands_rel = {
	has_coa_on_barony_only = yes
	graphical_culture = drownedgodgfx
	playable = yes
#	interface_skin = { pagan_interface }
	ai_peaceful = no
	
	color = { 0.0 0.6 0.0 } #Green

	male_names = { Deep Howard Phillips Guh Lum Kraff Squam Dag Fish }
	female_names = { Luv Vog Nyalhotep Nyarlahotep Fish }

	fish_gods = {
		graphical_culture = drownedgodgfx
		interface_skin = { pagan_interface }

		icon = 35
		heresy_icon = 35
		color = { 150 255 150 } #Green
		
		high_god_name = GOD_OLD_ONES
		
		god_names = {
			GOD_FISH_GODS THE_DEEP_ONES GOD_THE_SQUAMOS GOD_Nyarlathotep GOD_OLD_ONES GOD_DARK_SPIRITS
		}
		evil_god_names = {
			GOD_LION_NIGHT GOD_THE_OTHERS
		}

		character_modifier = {
			general_opinion = -20 # Canon says the Thousand Islander worshippers of the Fish Gods were xenophobic
			same_religion_opinion = 35
		}

		crusade_name = FISH_RECKONING
		scripture_name = TALES_OF_THE_DEEP
		priest_title = DAGONOS

		max_wives = 3
		independence_war_score_bonus = 60

		can_excommunicate = yes
		can_grant_divorce = yes
		can_grant_claim = yes

		priests_can_marry = no
		priests_can_inherit = no
		can_call_crusade = no
		has_heir_designation = yes
		raised_vassal_opinion_loss = no
		rel_head_defense = yes
		seafarer = yes
		allow_looting = yes
		allow_rivermovement = no
		autocephaly = yes
		defensive_attrition = yes
#		male_temple_holders = yes
		female_temple_holders = no

		aggression = 2.5
		
		hard_to_convert = yes

		religious_clothing_head = 11
		religious_clothing_priest = 13

		intermarry = sothoryos_rel
		intermarry = starry_wisdom
		intermarry = old_ones
		intermarry = lady_of_the_waves_reformed
		intermarry = lady_of_the_waves

#		Religious head and empire flag courtesty of: https://society6.com/product/queen-of-the-deep-ones_print
	}	
}