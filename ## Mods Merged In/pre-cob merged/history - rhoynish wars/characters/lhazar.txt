###Izven of Lhazosh###
10055916 = {
	name="Jommo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	7065.1.1 = {birth="7065.1.1"}
	7081.1.1 = {add_trait=poor_warrior}
	7083.1.1 = {add_spouse=40055916}
	7118.1.1 = {death="7118.1.1"}
}
40055916 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7065.1.1 = {birth="7065.1.1"}
	7118.1.1 = {death="7118.1.1"}
}
10155916 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	7084.1.1 = {birth="7084.1.1"}
	7148.1.1 = {death="7148.1.1"}
}
10255916 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	7086.1.1 = {birth="7086.1.1"}
	7129.1.1 = {death="7129.1.1"}
}
10355916 = {
	name="Fogo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	7087.1.1 = {birth="7087.1.1"}
	7103.1.1 = {add_trait=poor_warrior}
	7105.1.1 = {add_spouse=40355916}
	7135.1.1 = {death="7135.1.1"}
}
40355916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7087.1.1 = {birth="7087.1.1"}
	7135.1.1 = {death="7135.1.1"}
}
10455916 = {
	name="Rommo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	7088.1.1 = {birth="7088.1.1"}
	7104.1.1 = {add_trait=poor_warrior}
	7128.1.1 = {death="7128.1.1"}
}
10555916 = {
	name="Rommo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	7090.1.1 = {birth="7090.1.1"}
	7106.1.1 = {add_trait=poor_warrior}
	7166.1.1 = {death="7166.1.1"}
}
10655916 = {
	name="Zollo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10355916
	mother=40355916

	7116.1.1 = {birth="7116.1.1"}
	7132.1.1 = {add_trait=trained_warrior}
	7134.1.1 = {add_spouse=40655916}
	7187.1.1 = {death="7187.1.1"}
}
40655916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7116.1.1 = {birth="7116.1.1"}
	7187.1.1 = {death="7187.1.1"}
}
10755916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10355916
	mother=40355916

	7117.1.1 = {birth="7117.1.1"}
	7154.1.1 = {death="7154.1.1"}
}
10855916 = {
	name="Aggo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10655916
	mother=40655916

	7137.1.1 = {birth="7137.1.1"}
	7153.1.1 = {add_trait=poor_warrior}
	7155.1.1 = {add_spouse=40855916}
	7194.1.1 = {death="7194.1.1"}
}
40855916 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7137.1.1 = {birth="7137.1.1"}
	7194.1.1 = {death="7194.1.1"}
}
10955916 = {
	name="Zolli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10655916
	mother=40655916

	7140.1.1 = {birth="7140.1.1"}
	7209.1.1 = {death="7209.1.1"}
}
11055916 = {
	name="Rhogoro"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10655916
	mother=40655916

	7141.1.1 = {birth="7141.1.1"}
	7157.1.1 = {add_trait=trained_warrior}
	7198.1.1 = {death="7198.1.1"}
}
11155916 = {
	name="Iggo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10855916
	mother=40855916

	7162.1.1 = {birth="7162.1.1"}
	7178.1.1 = {add_trait=trained_warrior}
	7180.1.1 = {add_spouse=41155916}
	7238.1.1 = {death="7238.1.1"}
}
41155916 = {
	name="Rimmi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7162.1.1 = {birth="7162.1.1"}
	7238.1.1 = {death="7238.1.1"}
}
11255916 = {
	name="Arakh"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10855916
	mother=40855916

	7164.1.1 = {birth="7164.1.1"}
	7180.1.1 = {add_trait=poor_warrior}
	7182.1.1 = {add_spouse=41255916}
	7225.1.1 = {death="7225.1.1"}
}
41255916 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7164.1.1 = {birth="7164.1.1"}
	7225.1.1 = {death="7225.1.1"}
}
11355916 = {
	name="Quiri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11155916
	mother=41155916

	7184.1.1 = {birth="7184.1.1"}
	7263.1.1 = {death="7263.1.1"}
}
11455916 = {
	name="Mago"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11255916
	mother=41255916

	7189.1.1 = {birth="7189.1.1"}
	7205.1.1 = {add_trait=trained_warrior}
	7207.1.1 = {add_spouse=41455916}
	7231.1.1 = {death="7231.1.1"}
}
41455916 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7189.1.1 = {birth="7189.1.1"}
	7231.1.1 = {death="7231.1.1"}
}
11555916 = {
	name="Fogo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11255916
	mother=41255916

	7191.1.1 = {birth="7191.1.1"}
	7207.1.1 = {add_trait=poor_warrior}
	7244.1.1 = {death="7244.1.1"}
}
11655916 = {
	name="Zolli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11255916
	mother=41255916

	7192.1.1 = {birth="7192.1.1"}
	7257.1.1 = {death="7257.1.1"}
}
11755916 = {
	name="Ogo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	7208.1.1 = {birth="7208.1.1"}
	7224.1.1 = {add_trait=poor_warrior}
	7226.1.1 = {add_spouse=41755916}
	7276.1.1 = {death="7276.1.1"}
}
41755916 = {
	name="Mirri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7208.1.1 = {birth="7208.1.1"}
	7276.1.1 = {death="7276.1.1"}
}
11855916 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	7209.1.1 = {birth="7209.1.1"}
	7267.1.1 = {death="7267.1.1"}
}
11955916 = {
	name="Mago"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	7211.1.1 = {birth="7211.1.1"}
	7227.1.1 = {add_trait=poor_warrior}
	7258.1.1 = {death="7258.1.1"}
}
12055916 = {
	name="Qothi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	7213.1.1 = {birth="7213.1.1"}
	7242.1.1 = {death = {death_reason = death_murder}}
}
12155916 = {
	name="Zollo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	7227.1.1 = {birth="7227.1.1"}
	7243.1.1 = {add_trait=trained_warrior}
	7245.1.1 = {add_spouse=42155916}
	7285.1.1 = {death="7285.1.1"}
}
42155916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7227.1.1 = {birth="7227.1.1"}
	7285.1.1 = {death="7285.1.1"}
}
12255916 = {
	name="Rakharo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	7229.1.1 = {birth="7229.1.1"}
	7245.1.1 = {add_trait=trained_warrior}
	7260.1.1 = {death="7260.1.1"}
}
12355916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	7230.1.1 = {birth="7230.1.1"}
	7278.1.1 = {death="7278.1.1"}
}
12455916 = {
	name="Mago"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	7232.1.1 = {birth="7232.1.1"}
	7248.1.1 = {add_trait=poor_warrior}
	7304.1.1 = {death="7304.1.1"}
}
12555916 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	7233.1.1 = {birth="7233.1.1"}
	7277.1.1 = {death="7277.1.1"}
}
12655916 = {
	name="Bharbo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12155916
	mother=42155916

	7253.1.1 = {birth="7253.1.1"}
	7269.1.1 = {add_trait=poor_warrior}
	7271.1.1 = {add_spouse=42655916}
	7298.1.1 = {death="7298.1.1"}
}
42655916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7253.1.1 = {birth="7253.1.1"}
	7298.1.1 = {death="7298.1.1"}
}
12755916 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12155916
	mother=42155916

	7254.1.1 = {birth="7254.1.1"}
	7303.1.1 = {death="7303.1.1"}
}
12855916 = {
	name="Zolli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12155916
	mother=42155916

	7255.1.1 = {birth="7255.1.1"}
	7303.1.1 = {death = {death_reason = death_accident}}
}
12955916 = {
	name="Haggo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12655916
	mother=42655916

	7274.1.1 = {birth="7274.1.1"}
	7290.1.1 = {add_trait=trained_warrior}
	7292.1.1 = {add_spouse=42955916}
	7339.1.1 = {death="7339.1.1"}
}
42955916 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7274.1.1 = {birth="7274.1.1"}
	7339.1.1 = {death="7339.1.1"}
}
13055916 = {
	name="Rakharo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12655916
	mother=42655916

	7275.1.1 = {birth="7275.1.1"}
	7291.1.1 = {add_trait=trained_warrior}
	7293.1.1 = {add_spouse=43055916}
	7325.1.1 = {death="7325.1.1"}
}
43055916 = {
	name="Irri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7275.1.1 = {birth="7275.1.1"}
	7325.1.1 = {death="7325.1.1"}
}
13155916 = {
	name="Mothi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12655916
	mother=42655916

	7277.1.1 = {birth="7277.1.1"}
	7327.1.1 = {death="7327.1.1"}
}
13255916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12955916
	mother=42955916

	7301.1.1 = {birth="7301.1.1"}
	7345.1.1 = {death="7345.1.1"}
}
13355916 = {
	name="Rakhiri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13055916
	mother=43055916

	7300.1.1 = {birth="7300.1.1"}
	7376.1.1 = {death="7376.1.1"}
}
13455916 = {
	name="Cohollo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13055916
	mother=43055916

	7301.1.1 = {birth="7301.1.1"}
	7317.1.1 = {add_trait=poor_warrior}
	7319.1.1 = {add_spouse=43455916}
	7366.1.1 = {death="7366.1.1"}
}
43455916 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7301.1.1 = {birth="7301.1.1"}
	7366.1.1 = {death="7366.1.1"}
}
13555916 = {
	name="Rakharo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13055916
	mother=43055916

	7302.1.1 = {birth="7302.1.1"}
	7318.1.1 = {add_trait=poor_warrior}
	7355.1.1 = {death="7355.1.1"}
}
13655916 = {
	name="Pono"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13455916
	mother=43455916

	7334.1.1 = {birth="7334.1.1"}
	7350.1.1 = {add_trait=trained_warrior}
	7352.1.1 = {add_spouse=43655916}
	7387.1.1 = {death="7387.1.1"}
}
43655916 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7334.1.1 = {birth="7334.1.1"}
	7387.1.1 = {death="7387.1.1"}
}
13755916 = {
	name="Rhogoro"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13455916
	mother=43455916

	7337.1.1 = {birth="7337.1.1"}
	7353.1.1 = {add_trait=trained_warrior}
	7355.1.1 = {add_spouse=43755916}
	7369.1.1 = {death = {death_reason = death_murder}}
}
43755916 = {
	name="Kovarri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7337.1.1 = {birth="7337.1.1"}
	7369.1.1 = {death="7369.1.1"}
}
13855916 = {
	name="Qotho"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13455916
	mother=43455916

	7340.1.1 = {birth="7340.1.1"}
	7356.1.1 = {add_trait=poor_warrior}
	7390.1.1 = {death="7390.1.1"}
}
13955916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13655916
	mother=43655916

	7358.1.1 = {birth="7358.1.1"}
}
14055916 = {
	name="Rakharo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13755916
	mother=43755916

	7357.1.1 = {birth="7357.1.1"}
	7373.1.1 = {add_trait=poor_warrior}
	7386.1.1 = {add_spouse=44055916}
}
44055916 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7368.1.1 = {birth="7368.1.1"}
}
14155916 = {
	name="Coholli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13755916
	mother=43755916

	7360.1.1 = {birth="7360.1.1"}
}
14255916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	7394.1.1 = {birth="7394.1.1"}
}
14355916 = {
	name="Motho"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	7397.1.1 = {birth="7397.1.1"}
	7413.1.1 = {add_trait=poor_warrior}
}
14455916 = {
	name="Quiri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	7398.1.1 = {birth="7398.1.1"}
}
14555916 = {
	name="Ogo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	7399.1.1 = {birth="7399.1.1"}
	7415.1.1 = {add_trait=poor_warrior}
}
###Sheqethi of Hesh###
10055923 = {
	name="Pono"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	7053.1.1 = {birth="7053.1.1"}
	7069.1.1 = {add_trait=poor_warrior}
	7071.1.1 = {add_spouse=40055923}
	7110.1.1 = {death="7110.1.1"}
}
40055923 = {
	name="Rimmi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7053.1.1 = {birth="7053.1.1"}
	7110.1.1 = {death="7110.1.1"}
}
10155923 = {
	name="Rhogoro"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10055923
	mother=40055923

	7082.1.1 = {birth="7082.1.1"}
	7098.1.1 = {add_trait=trained_warrior}
	7153.1.1 = {death="7153.1.1"}
}
10255923 = {
	name="Rhogiri"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10055923
	mother=40055923

	7084.1.1 = {birth="7084.1.1"}
	7138.1.1 = {death="7138.1.1"}
}
10355923 = {
	name="Motho"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10055923
	mother=40055923

	7085.1.1 = {birth="7085.1.1"}
	7101.1.1 = {add_trait=trained_warrior}
	7103.1.1 = {add_spouse=40355923}
	7157.1.1 = {death="7157.1.1"}
}
40355923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7085.1.1 = {birth="7085.1.1"}
	7157.1.1 = {death="7157.1.1"}
}
10455923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10355923
	mother=40355923

	7103.1.1 = {birth="7103.1.1"}
	7119.1.1 = {add_trait=poor_warrior}
	7180.1.1 = {death="7180.1.1"}
}
10555923 = {
	name="Jhogo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10355923
	mother=40355923

	7104.1.1 = {birth="7104.1.1"}
	7120.1.1 = {add_trait=trained_warrior}
	7122.1.1 = {add_spouse=40555923}
	7164.1.1 = {death = {death_reason = death_accident}}
}
40555923 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7104.1.1 = {birth="7104.1.1"}
	7164.1.1 = {death="7164.1.1"}
}
10655923 = {
	name="Rhogiri"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	7123.1.1 = {birth="7123.1.1"}
	7166.1.1 = {death="7166.1.1"}
}
10755923 = {
	name="Pono"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	7126.1.1 = {birth="7126.1.1"}
	7142.1.1 = {add_trait=poor_warrior}
	7144.1.1 = {add_spouse=40755923}
	7182.1.1 = {death="7182.1.1"}
}
40755923 = {
	name="Rhogiri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7126.1.1 = {birth="7126.1.1"}
	7182.1.1 = {death="7182.1.1"}
}
10855923 = {
	name="Coholli"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	7127.1.1 = {birth="7127.1.1"}
	7173.1.1 = {death="7173.1.1"}
}
10955923 = {
	name="Malakho"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	7129.1.1 = {birth="7129.1.1"}
	7145.1.1 = {add_trait=poor_warrior}
	7147.1.1 = {add_spouse=40955923}
	7176.1.1 = {death="7176.1.1"}
}
40955923 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7129.1.1 = {birth="7129.1.1"}
	7176.1.1 = {death="7176.1.1"}
}
11055923 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	7132.1.1 = {birth="7132.1.1"}
	7170.1.1 = {death="7170.1.1"}
}
11155923 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10955923
	mother=40955923

	7149.1.1 = {birth="7149.1.1"}
	7215.1.1 = {death="7215.1.1"}
}
11255923 = {
	name="Aggi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10955923
	mother=40955923

	7151.1.1 = {birth="7151.1.1"}
	7196.1.1 = {death="7196.1.1"}
}
11355923 = {
	name="Zollo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	7147.1.1 = {birth="7147.1.1"}
	7163.1.1 = {add_trait=trained_warrior}
	7205.1.1 = {death="7205.1.1"}
}
11455923 = {
	name="Fogo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	7148.1.1 = {birth="7148.1.1"}
	7164.1.1 = {add_trait=trained_warrior}
	7215.1.1 = {death="7215.1.1"}
}
11555923 = {
	name="Jommo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	7149.1.1 = {birth="7149.1.1"}
	7165.1.1 = {add_trait=poor_warrior}
	7167.1.1 = {add_spouse=41555923}
	7224.1.1 = {death = {death_reason = death_accident}}
}
41555923 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7149.1.1 = {birth="7149.1.1"}
	7224.1.1 = {death="7224.1.1"}
}
11655923 = {
	name="Kovarri"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	7152.1.1 = {birth="7152.1.1"}
	7201.1.1 = {death="7201.1.1"}
}
11755923 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11555923
	mother=41555923

	7174.1.1 = {birth="7174.1.1"}
	7243.1.1 = {death="7243.1.1"}
}
11855923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11555923
	mother=41555923

	7175.1.1 = {birth="7175.1.1"}
	7191.1.1 = {add_trait=poor_warrior}
	7193.1.1 = {add_spouse=41855923}
	7224.1.1 = {death="7224.1.1"}
}
41855923 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7175.1.1 = {birth="7175.1.1"}
	7224.1.1 = {death="7224.1.1"}
}
11955923 = {
	name="Pono"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	7196.1.1 = {birth="7196.1.1"}
	7206.1.1 = {death = {death_reason = death_accident}}
}
12055923 = {
	name="Motho"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	7198.1.1 = {birth="7198.1.1"}
	7214.1.1 = {add_trait=trained_warrior}
	7272.1.1 = {death="7272.1.1"}
}
12155923 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	7199.1.1 = {birth="7199.1.1"}
	7244.1.1 = {death="7244.1.1"}
}
12255923 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	7201.1.1 = {birth="7201.1.1"}
	7210.1.1 = {death = {death_reason = death_accident}}
}
12355923 = {
	name="Drogo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	7204.1.1 = {birth="7204.1.1"}
	7220.1.1 = {add_trait=trained_warrior}
	7222.1.1 = {add_spouse=42355923}
	7260.1.1 = {death="7260.1.1"}
}
42355923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7204.1.1 = {birth="7204.1.1"}
	7260.1.1 = {death="7260.1.1"}
}
12455923 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12355923
	mother=42355923

	7237.1.1 = {birth="7237.1.1"}
	7290.1.1 = {death="7290.1.1"}
}
12555923 = {
	name="Moro"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12355923
	mother=42355923

	7240.1.1 = {birth="7240.1.1"}
	7256.1.1 = {add_trait=trained_warrior}
	7258.1.1 = {add_spouse=42555923}
	7273.1.1 = {death="7273.1.1"}
}
42555923 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7240.1.1 = {birth="7240.1.1"}
	7273.1.1 = {death="7273.1.1"}
}
12655923 = {
	name="Rommo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12555923
	mother=42555923

	7264.1.1 = {birth="7264.1.1"}
	7280.1.1 = {add_trait=trained_warrior}
	7298.1.1 = {death="7298.1.1"}
}
12755923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12555923
	mother=42555923

	7265.1.1 = {birth="7265.1.1"}
	7281.1.1 = {add_trait=trained_warrior}
	7308.1.1 = {death="7308.1.1"}
}
12855923 = {
	name="Zekko"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12555923
	mother=42555923

	7266.1.1 = {birth="7266.1.1"}
	7282.1.1 = {add_trait=poor_warrior}
	7284.1.1 = {add_spouse=42855923}
	7320.1.1 = {death="7320.1.1"}
}
42855923 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7266.1.1 = {birth="7266.1.1"}
	7320.1.1 = {death="7320.1.1"}
}
12955923 = {
	name="Jommo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12855923
	mother=42855923

	7291.1.1 = {birth="7291.1.1"}
	7307.1.1 = {add_trait=trained_warrior}
	7309.1.1 = {add_spouse=42955923}
	7332.1.1 = {death="7332.1.1"}
}
42955923 = {
	name="Qothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7291.1.1 = {birth="7291.1.1"}
	7332.1.1 = {death="7332.1.1"}
}
13055923 = {
	name="Rommo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12955923
	mother=42955923

	7315.1.1 = {birth="7315.1.1"}
	7331.1.1 = {add_trait=trained_warrior}
	7362.1.1 = {death = {death_reason = death_accident}}
}
13155923 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12955923
	mother=42955923

	7316.1.1 = {birth="7316.1.1"}
	7369.1.1 = {death="7369.1.1"}
}
13255923 = {
	name="Qotho"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12955923
	mother=42955923

	7318.1.1 = {birth="7318.1.1"}
	7334.1.1 = {add_trait=trained_warrior}
	7336.1.1 = {add_spouse=43255923}
	7370.1.1 = {death="7370.1.1"}
}
43255923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7318.1.1 = {birth="7318.1.1"}
	7370.1.1 = {death="7370.1.1"}
}
13355923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13255923
	mother=43255923

	7342.1.1 = {birth="7342.1.1"}
	7358.1.1 = {add_trait=poor_warrior}
}
13455923 = {
	name="Iggo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13255923
	mother=43255923

	7345.1.1 = {birth="7345.1.1"}
	7361.1.1 = {add_trait=poor_warrior}
}
13555923 = {
	name="Mago"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13255923
	mother=43255923

	7346.1.1 = {birth="7346.1.1"}
	7362.1.1 = {add_trait=trained_warrior}
	7364.1.1 = {add_spouse=43555923}
}
43555923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7346.1.1 = {birth="7346.1.1"}
}
13655923 = {
	name="Malakho"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13555923
	mother=43555923

	7379.1.1 = {birth="7379.1.1"}
	7395.1.1 = {add_trait=poor_warrior}
}
13755923 = {
	name="Jhaquo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13555923
	mother=43555923

	7381.1.1 = {birth="7381.1.1"}
	7397.1.1 = {add_trait=poor_warrior}
}
13855923 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13555923
	mother=43555923

	7383.1.1 = {birth="7383.1.1"}
}
###Drogikh of Kosrak###
10055901 = {
	name="Moro"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	7032.1.1 = {birth="7032.1.1"}
	7048.1.1 = {add_trait=trained_warrior}
	7050.1.1 = {add_spouse=40055901}
	7104.1.1 = {death="7104.1.1"}
}
40055901 = {
	name="Irri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7032.1.1 = {birth="7032.1.1"}
	7104.1.1 = {death="7104.1.1"}
}
10155901 = {
	name="Drogo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	7061.1.1 = {birth="7061.1.1"}
	7077.1.1 = {add_trait=poor_warrior}
	7079.1.1 = {add_spouse=40155901}
	7125.1.1 = {death="7125.1.1"}
}
40155901 = {
	name="Mirri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7061.1.1 = {birth="7061.1.1"}
	7125.1.1 = {death="7125.1.1"}
}
10255901 = {
	name="Jhaquo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	7063.1.1 = {birth="7063.1.1"}
	7079.1.1 = {add_trait=poor_warrior}
	7107.1.1 = {death="7107.1.1"}
}
10355901 = {
	name="Rakhiri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	7064.1.1 = {birth="7064.1.1"}
	7130.1.1 = {death="7130.1.1"}
}
10455901 = {
	name="Mago"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	7067.1.1 = {birth="7067.1.1"}
	7083.1.1 = {add_trait=poor_warrior}
	7085.1.1 = {add_spouse=40455901}
	7146.1.1 = {death="7146.1.1"}
}
40455901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7067.1.1 = {birth="7067.1.1"}
	7146.1.1 = {death="7146.1.1"}
}
10555901 = {
	name="Pono"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10155901
	mother=40155901

	7089.1.1 = {birth="7089.1.1"}
	7105.1.1 = {add_trait=poor_warrior}
	7150.1.1 = {death="7150.1.1"}
}
10655901 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10155901
	mother=40155901

	7090.1.1 = {birth="7090.1.1"}
	7123.1.1 = {death="7123.1.1"}
}
10755901 = {
	name="Aggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10455901
	mother=40455901

	7095.1.1 = {birth="7095.1.1"}
	7151.1.1 = {death="7151.1.1"}
}
10855901 = {
	name="Ogo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10455901
	mother=40455901

	7096.1.1 = {birth="7096.1.1"}
	7112.1.1 = {add_trait=poor_warrior}
	7114.1.1 = {add_spouse=40855901}
	7147.1.1 = {death="7147.1.1"}
}
40855901 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7096.1.1 = {birth="7096.1.1"}
	7147.1.1 = {death="7147.1.1"}
}
10955901 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10855901
	mother=40855901

	7128.1.1 = {birth="7128.1.1"}
	7186.1.1 = {death = {death_reason = death_murder}}
}
11055901 = {
	name="Rakharo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10855901
	mother=40855901

	7129.1.1 = {birth="7129.1.1"}
	7145.1.1 = {add_trait=poor_warrior}
	7187.1.1 = {death="7187.1.1"}
}
11155901 = {
	name="Cohollo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10855901
	mother=40855901

	7131.1.1 = {birth="7131.1.1"}
	7147.1.1 = {add_trait=trained_warrior}
	7149.1.1 = {add_spouse=41155901}
	7198.1.1 = {death="7198.1.1"}
}
41155901 = {
	name="Irri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7131.1.1 = {birth="7131.1.1"}
	7198.1.1 = {death="7198.1.1"}
}
11255901 = {
	name="Jhaquo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11155901
	mother=41155901

	7151.1.1 = {birth="7151.1.1"}
	7167.1.1 = {add_trait=poor_warrior}
	7169.1.1 = {add_spouse=41255901}
	7183.1.1 = {death = {death_reason = death_battle}}
}
41255901 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7151.1.1 = {birth="7151.1.1"}
	7183.1.1 = {death="7183.1.1"}
}
11355901 = {
	name="Haggo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11155901
	mother=41155901

	7154.1.1 = {birth="7154.1.1"}
	7170.1.1 = {add_trait=poor_warrior}
	7198.1.1 = {death="7198.1.1"}
}
11455901 = {
	name="Cohollo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	7174.1.1 = {birth="7174.1.1"}
	7190.1.1 = {add_trait=poor_warrior}
	7192.1.1 = {add_spouse=41455901}
	7228.1.1 = {death="7228.1.1"}
}
41455901 = {
	name="Kovarri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7174.1.1 = {birth="7174.1.1"}
	7228.1.1 = {death="7228.1.1"}
}
11555901 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	7176.1.1 = {birth="7176.1.1"}
	7198.1.1 = {death="7198.1.1"}
}
11655901 = {
	name="Jommo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	7179.1.1 = {birth="7179.1.1"}
	7195.1.1 = {add_trait=poor_warrior}
	7197.1.1 = {add_spouse=41655901}
	7210.1.1 = {death="7210.1.1"}
}
41655901 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7179.1.1 = {birth="7179.1.1"}
	7210.1.1 = {death="7210.1.1"}
}
11755901 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	7182.1.1 = {birth="7182.1.1"}
	7246.1.1 = {death="7246.1.1"}
}
11855901 = {
	name="Qothi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11655901
	mother=41655901

	7200.1.1 = {birth="7200.1.1"}
	7240.1.1 = {death="7240.1.1"}
}
11955901 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	7196.1.1 = {birth="7196.1.1"}
	7253.1.1 = {death="7253.1.1"}
}
12055901 = {
	name="Kovarro"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	7198.1.1 = {birth="7198.1.1"}
	7214.1.1 = {add_trait=poor_warrior}
	7261.1.1 = {death="7261.1.1"}
}
12155901 = {
	name="Qothi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	7200.1.1 = {birth="7200.1.1"}
	7271.1.1 = {death="7271.1.1"}
}
12255901 = {
	name="Rakharo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	7203.1.1 = {birth="7203.1.1"}
	7219.1.1 = {add_trait=poor_warrior}
	7221.1.1 = {add_spouse=42255901}
	7244.1.1 = {death="7244.1.1"}
}
42255901 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7203.1.1 = {birth="7203.1.1"}
	7244.1.1 = {death="7244.1.1"}
}
12355901 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12255901
	mother=42255901

	7224.1.1 = {birth="7224.1.1"}
	7273.1.1 = {death="7273.1.1"}
}
12455901 = {
	name="Fogo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12255901
	mother=42255901

	7227.1.1 = {birth="7227.1.1"}
	7243.1.1 = {add_trait=trained_warrior}
	7301.1.1 = {death="7301.1.1"}
}
12555901 = {
	name="Jhaquo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12255901
	mother=42255901

	7229.1.1 = {birth="7229.1.1"}
	7245.1.1 = {add_trait=poor_warrior}
	7247.1.1 = {add_spouse=42555901}
	7283.1.1 = {death="7283.1.1"}
}
42555901 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7229.1.1 = {birth="7229.1.1"}
	7283.1.1 = {death="7283.1.1"}
}
12655901 = {
	name="Rommo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12555901
	mother=42555901

	7255.1.1 = {birth="7255.1.1"}
	7271.1.1 = {add_trait=poor_warrior}
	7321.1.1 = {death="7321.1.1"}
}
12755901 = {
	name="Zollo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12555901
	mother=42555901

	7258.1.1 = {birth="7258.1.1"}
	7274.1.1 = {add_trait=poor_warrior}
	7276.1.1 = {add_spouse=42755901}
	7317.1.1 = {death="7317.1.1"}
}
42755901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7258.1.1 = {birth="7258.1.1"}
	7317.1.1 = {death="7317.1.1"}
}
12855901 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12555901
	mother=42555901

	7259.1.1 = {birth="7259.1.1"}
	7308.1.1 = {death="7308.1.1"}
}
12955901 = {
	name="Motho"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	7291.1.1 = {birth="7291.1.1"}
	7307.1.1 = {add_trait=poor_warrior}
	7348.1.1 = {death = {death_reason = death_murder}}
}
13055901 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	7294.1.1 = {birth="7294.1.1"}
	7347.1.1 = {death="7347.1.1"}
}
13155901 = {
	name="Pono"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	7295.1.1 = {birth="7295.1.1"}
	7311.1.1 = {add_trait=trained_warrior}
	7313.1.1 = {add_spouse=43155901}
	7350.1.1 = {death="7350.1.1"}
}
43155901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7295.1.1 = {birth="7295.1.1"}
	7350.1.1 = {death="7350.1.1"}
}
13255901 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	7296.1.1 = {birth="7296.1.1"}
	7336.1.1 = {death="7336.1.1"}
}
13355901 = {
	name="Qotho"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	7328.1.1 = {birth="7328.1.1"}
	7344.1.1 = {add_trait=trained_warrior}
	7382.1.1 = {death="7382.1.1"}
}
13455901 = {
	name="Rakharo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	7329.1.1 = {birth="7329.1.1"}
	7345.1.1 = {add_trait=trained_warrior}
	7399.1.1 = {death="7399.1.1"}
}
13555901 = {
	name="Jhaquo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	7331.1.1 = {birth="7331.1.1"}
	7347.1.1 = {add_trait=trained_warrior}
	7349.1.1 = {add_spouse=43555901}
	7379.1.1 = {death="7379.1.1"}
}
43555901 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7331.1.1 = {birth="7331.1.1"}
	7379.1.1 = {death="7379.1.1"}
}
13655901 = {
	name="Rhogiri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	7334.1.1 = {birth="7334.1.1"}
	7408.1.1 = {death="7408.1.1"}
}
13755901 = {
	name="Rakhiri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13555901
	mother=43555901

	7359.1.1 = {birth="7359.1.1"}
}
13855901 = {
	name="Moro"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13555901
	mother=43555901

	7362.1.1 = {birth="7362.1.1"}
	7378.1.1 = {add_trait=poor_warrior}
	7380.1.1 = {add_spouse=43855901}
}
43855901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7362.1.1 = {birth="7362.1.1"}
}
13955901 = {
	name="Drogo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13555901
	mother=43555901

	7364.1.1 = {birth="7364.1.1"}
	7380.1.1 = {add_trait=poor_warrior}
	7382.1.1 = {add_spouse=43955901}
}
43955901 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	7364.1.1 = {birth="7364.1.1"}
}
14055901 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13855901
	mother=43855901

	7381.1.1 = {birth="7381.1.1"}
}
14155901 = {
	name="Fogo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	7392.1.1 = {birth="7392.1.1"}
	7408.1.1 = {add_trait=poor_warrior}
}
14255901 = {
	name="Haggo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	7395.1.1 = {birth="7395.1.1"}
	7411.1.1 = {add_trait=trained_warrior}
}
14355901 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	7396.1.1 = {birth="7396.1.1"}
}
14455901 = {
	name="Temmo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	7399.1.1 = {birth="7399.1.1"}
	7415.1.1 = {add_trait=poor_warrior}
}
