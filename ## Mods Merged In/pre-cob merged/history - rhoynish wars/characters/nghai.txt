####  House Owyang
10069208 = {
	name="Jalil"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	7065.1.1 = {birth="7065.1.1"}
	7081.1.1 = {add_trait=poor_warrior}
	7083.1.1 = {add_spouse=40069208}
	7123.1.1 = {death="7123.1.1"}
}
40069208 = {
	name="Shogofa"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7065.1.1 = {birth="7065.1.1"}
	7123.1.1 = {death="7123.1.1"}
}
10169208 = {
	name="Sajida"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10069208
	mother=40069208

	7087.1.1 = {birth="7087.1.1"}
	7138.1.1 = {death="7138.1.1"}
}
10269208 = {
	name="Habiba"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10069208
	mother=40069208

	7089.1.1 = {birth="7089.1.1"}
	7144.1.1 = {death="7144.1.1"}
}
10369208 = {
	name="Jalil"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10069208
	mother=40069208

	7090.1.1 = {birth="7090.1.1"}
	7106.1.1 = {add_trait=trained_warrior}
	7108.1.1 = {add_spouse=40369208}
	7128.1.1 = {death="7128.1.1"}
}
40369208 = {
	name="Habiba"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7090.1.1 = {birth="7090.1.1"}
	7128.1.1 = {death="7128.1.1"}
}
10469208 = {
	name="Fadil"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10369208
	mother=40369208

	7110.1.1 = {birth="7110.1.1"}
	7126.1.1 = {add_trait=trained_warrior}
	7128.1.1 = {add_spouse=40469208}
	7168.1.1 = {death="7168.1.1"}
}
40469208 = {
	name="Taliba"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7110.1.1 = {birth="7110.1.1"}
	7168.1.1 = {death="7168.1.1"}
}
10569208 = {
	name="Husam"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10369208
	mother=40369208

	7112.1.1 = {birth="7112.1.1"}
	7128.1.1 = {add_trait=poor_warrior}
	7158.1.1 = {death="7158.1.1"}
}
10669208 = {
	name="Saghar"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10369208
	mother=40369208

	7115.1.1 = {birth="7115.1.1"}
	7192.1.1 = {death="7192.1.1"}
}
10769208 = {
	name="Mansur"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10469208
	mother=40469208

	7133.1.1 = {birth="7133.1.1"}
	7149.1.1 = {add_trait=trained_warrior}
	7151.1.1 = {add_spouse=40769208}
	7190.1.1 = {death="7190.1.1"}
}
40769208 = {
	name="Asiya"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7133.1.1 = {birth="7133.1.1"}
	7190.1.1 = {death="7190.1.1"}
}
10869208 = {
	name="Zeyd"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10769208
	mother=40769208

	7167.1.1 = {birth="7167.1.1"}
	7183.1.1 = {add_trait=skilled_warrior}
	7185.1.1 = {add_spouse=40869208}
	7217.1.1 = {death="7217.1.1"}
}
40869208 = {
	name="Taliba"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7167.1.1 = {birth="7167.1.1"}
	7217.1.1 = {death="7217.1.1"}
}
10969208 = {
	name="Khalil"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10769208
	mother=40769208

	7169.1.1 = {birth="7169.1.1"}
	7185.1.1 = {add_trait=poor_warrior}
	7230.1.1 = {death="7230.1.1"}
}
11069208 = {
	name="Mansur"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=10869208
	mother=40869208

	7185.1.1 = {birth="7185.1.1"}
	7201.1.1 = {add_trait=poor_warrior}
	7203.1.1 = {add_spouse=41069208}
	7219.1.1 = {death="7219.1.1"}
}
41069208 = {
	name="Habiba"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7185.1.1 = {birth="7185.1.1"}
	7219.1.1 = {death="7219.1.1"}
}
11169208 = {
	name="Gafur"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11069208
	mother=41069208

	7208.1.1 = {birth="7208.1.1"}
	7224.1.1 = {add_trait=poor_warrior}
	7226.1.1 = {add_spouse=41169208}
	7251.1.1 = {death="7251.1.1"}
}
41169208 = {
	name="Layla"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7208.1.1 = {birth="7208.1.1"}
	7251.1.1 = {death="7251.1.1"}
}
11269208 = {
	name="Shola"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11069208
	mother=41069208

	7211.1.1 = {birth="7211.1.1"}
	7260.1.1 = {death="7260.1.1"}
}
11369208 = {
	name="Mahdi"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11069208
	mother=41069208

	7212.1.1 = {birth="7212.1.1"}
	7228.1.1 = {add_trait=poor_warrior}
	7271.1.1 = {death="7271.1.1"}
}
11469208 = {
	name="Qamara"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11069208
	mother=41069208

	7213.1.1 = {birth="7213.1.1"}
	7270.1.1 = {death="7270.1.1"}
}
11569208 = {
	name="Mahdi"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11169208
	mother=41169208

	7233.1.1 = {birth="7233.1.1"}
	7249.1.1 = {add_trait=trained_warrior}
	7251.1.1 = {add_spouse=41569208}
	7285.1.1 = {death="7285.1.1"}
}
41569208 = {
	name="Adila"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7233.1.1 = {birth="7233.1.1"}
	7285.1.1 = {death="7285.1.1"}
}
11669208 = {
	name="Aarif"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11169208
	mother=41169208

	7235.1.1 = {birth="7235.1.1"}
	7251.1.1 = {add_trait=trained_warrior}
	7276.1.1 = {death="7276.1.1"}
}
11769208 = {
	name="Ali"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11169208
	mother=41169208

	7236.1.1 = {birth="7236.1.1"}
	7252.1.1 = {add_trait=trained_warrior}
	7298.1.1 = {death="7298.1.1"}
}
11869208 = {
	name="Azam"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11569208
	mother=41569208

	7260.1.1 = {birth="7260.1.1"}
	7276.1.1 = {add_trait=poor_warrior}
	7278.1.1 = {add_spouse=41869208}
	7308.1.1 = {death="7308.1.1"}
}
41869208 = {
	name="Sabba"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7260.1.1 = {birth="7260.1.1"}
	7308.1.1 = {death="7308.1.1"}
}
11969208 = {
	name="Setara"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11569208
	mother=41569208

	7261.1.1 = {birth="7261.1.1"}
	7325.1.1 = {death="7325.1.1"}
}
12069208 = {
	name="Ramadan"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=11869208
	mother=41869208

	7287.1.1 = {birth="7287.1.1"}
	7303.1.1 = {add_trait=skilled_warrior}
	7305.1.1 = {add_spouse=42069208}
	7329.1.1 = {death="7329.1.1"}
}
42069208 = {
	name="Setara"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7287.1.1 = {birth="7287.1.1"}
	7329.1.1 = {death="7329.1.1"}
}
12169208 = {
	name="Rafiqa"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12069208
	mother=42069208

	7307.1.1 = {birth="7307.1.1"}
	7345.1.1 = {death="7345.1.1"}
}
12269208 = {
	name="Husam"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12069208
	mother=42069208

	7308.1.1 = {birth="7308.1.1"}
	7324.1.1 = {add_trait=master_warrior}
	7326.1.1 = {add_spouse=42269208}
	7362.1.1 = {death="7362.1.1"}
}
42269208 = {
	name="Shameem"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7308.1.1 = {birth="7308.1.1"}
	7362.1.1 = {death="7362.1.1"}
}
12369208 = {
	name="Maryam"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12069208
	mother=42069208

	7311.1.1 = {birth="7311.1.1"}
	7361.1.1 = {death="7361.1.1"}
}
12469208 = {
	name="Uways"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12269208
	mother=42269208

	7340.1.1 = {birth="7340.1.1"}
	7356.1.1 = {add_trait=skilled_warrior}
	7358.1.1 = {add_spouse=42469208}
	7392.1.1 = {death="7392.1.1"}
}
42469208 = {
	name="Layla"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7340.1.1 = {birth="7340.1.1"}
	7392.1.1 = {death="7392.1.1"}
}
12569208 = {
	name="Talib"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12269208
	mother=42269208

	7341.1.1 = {birth="7341.1.1"}
	7357.1.1 = {add_trait=poor_warrior}
	7393.1.1 = {death="7393.1.1"}
}
12669208 = {
	name="Aghlab"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12269208
	mother=42269208

	7343.1.1 = {birth="7343.1.1"}
	7359.1.1 = {add_trait=poor_warrior}
	7382.1.1 = {death="7382.1.1"}
}
12769208 = {
	name="Halil"	# a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12469208
	mother=42469208

	7369.1.1 = {birth="7369.1.1"}
	7385.1.1 = {add_trait=trained_warrior}
	7387.1.1 = {add_spouse=42769208}
}
42769208 = {
	name="Sheeva"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7369.1.1 = {birth="7369.1.1"}
}
12869208 = {
	name="Rafiqa"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12469208
	mother=42469208

	7371.1.1 = {birth="7371.1.1"}
}
12969208 = {
	name="Yahya"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12469208
	mother=42469208

	7374.1.1 = {birth="7374.1.1"}
	7390.1.1 = {add_trait=poor_warrior}
}
13069208 = {
	name="Saghar"	# not a lord
	female=yes
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12769208
	mother=42769208

	7392.1.1 = {birth="7392.1.1"}
}
13169208 = {
	name="Ubayd"	# not a lord
	dynasty=69208

	religion="gods_nghai"
	culture="nghai"

	father=12769208
	mother=42769208

	7393.1.1 = {birth="7393.1.1"}
	7409.1.1 = {add_trait=trained_warrior}
}


####  House Jeung
10069206 = {
	name="Qawurd"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	7063.1.1 = {birth="7063.1.1"}
	7079.1.1 = {add_trait=poor_warrior}
	7081.1.1 = {add_spouse=40069206}
	7121.1.1 = {death="7121.1.1"}
}
40069206 = {
	name="Nyawela"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7063.1.1 = {birth="7063.1.1"}
	7121.1.1 = {death="7121.1.1"}
}
10169206 = {
	name="Sholah"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10069206
	mother=40069206

	7091.1.1 = {birth="7091.1.1"}
	7119.1.1 = {death="7119.1.1"}
}
10269206 = {
	name="Amsha"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10069206
	mother=40069206

	7093.1.1 = {birth="7093.1.1"}
	7163.1.1 = {death = {death_reason = death_murder}}
}
10369206 = {
	name="Jahaira"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10069206
	mother=40069206

	7094.1.1 = {birth="7094.1.1"}
	7104.1.1 = {death="7104.1.1"}
}
10469206 = {
	name="Mahdi"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10069206
	mother=40069206

	7096.1.1 = {birth="7096.1.1"}
	7112.1.1 = {add_trait=skilled_warrior}
	7114.1.1 = {add_spouse=40469206}
	7154.1.1 = {death="7154.1.1"}
}
40469206 = {
	name="Shameem"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7096.1.1 = {birth="7096.1.1"}
	7154.1.1 = {death="7154.1.1"}
}
10569206 = {
	name="Nasr"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10469206
	mother=40469206

	7124.1.1 = {birth="7124.1.1"}
	7140.1.1 = {add_trait=skilled_warrior}
	7142.1.1 = {add_spouse=40569206}
	7179.1.1 = {death="7179.1.1"}
}
40569206 = {
	name="Nyawela"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7124.1.1 = {birth="7124.1.1"}
	7179.1.1 = {death="7179.1.1"}
}
10669206 = {
	name="Wahab"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10469206
	mother=40469206

	7125.1.1 = {birth="7125.1.1"}
	7141.1.1 = {add_trait=poor_warrior}
	7194.1.1 = {death="7194.1.1"}
}
10769206 = {
	name="Semeah"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10569206
	mother=40569206

	7142.1.1 = {birth="7142.1.1"}
	7178.1.1 = {death = {death_reason = death_accident}}
}
10869206 = {
	name="Paymaneh"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10569206
	mother=40569206

	7144.1.1 = {birth="7144.1.1"}
	7186.1.1 = {death="7186.1.1"}
}
10969206 = {
	name="Paymaneh"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10569206
	mother=40569206

	7145.1.1 = {birth="7145.1.1"}
	7179.1.1 = {death="7179.1.1"}
}
11069206 = {
	name="Is'mail"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=10569206
	mother=40569206

	7148.1.1 = {birth="7148.1.1"}
	7164.1.1 = {add_trait=trained_warrior}
	7166.1.1 = {add_spouse=41069206}
	7203.1.1 = {death="7203.1.1"}
}
41069206 = {
	name="Kamala"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7148.1.1 = {birth="7148.1.1"}
	7203.1.1 = {death="7203.1.1"}
}
11169206 = {
	name="Nizam"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11069206
	mother=41069206

	7181.1.1 = {birth="7181.1.1"}
	7197.1.1 = {add_trait=poor_warrior}
	7199.1.1 = {add_spouse=41169206}
	7226.1.1 = {death="7226.1.1"}
}
41169206 = {
	name="Amsha"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7181.1.1 = {birth="7181.1.1"}
	7226.1.1 = {death="7226.1.1"}
}
11269206 = {
	name="Parween"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11069206
	mother=41069206

	7182.1.1 = {birth="7182.1.1"}
	7258.1.1 = {death="7258.1.1"}
}
11369206 = {
	name="Bakr"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11169206
	mother=41169206

	7205.1.1 = {birth="7205.1.1"}
	7221.1.1 = {add_trait=poor_warrior}
	7223.1.1 = {add_spouse=41369206}
	7252.1.1 = {death="7252.1.1"}
}
41369206 = {
	name="Taneen"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7205.1.1 = {birth="7205.1.1"}
	7252.1.1 = {death="7252.1.1"}
}
11469206 = {
	name="Jalil"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11169206
	mother=41169206

	7206.1.1 = {birth="7206.1.1"}
	7222.1.1 = {add_trait=skilled_warrior}
	7239.1.1 = {death = {death_reason = death_battle}}
}
11569206 = {
	name="Nizam"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11169206
	mother=41169206

	7207.1.1 = {birth="7207.1.1"}
	7223.1.1 = {add_trait=trained_warrior}
	7285.1.1 = {death="7285.1.1"}
}
11669206 = {
	name="Ghalib"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11369206
	mother=41369206

	7226.1.1 = {birth="7226.1.1"}
	7242.1.1 = {add_trait=trained_warrior}
	7244.1.1 = {add_spouse=41669206}
	7293.1.1 = {death="7293.1.1"}
}
41669206 = {
	name="Sheeva"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7226.1.1 = {birth="7226.1.1"}
	7293.1.1 = {death="7293.1.1"}
}
11769206 = {
	name="Samira"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11369206
	mother=41369206

	7228.1.1 = {birth="7228.1.1"}
	7284.1.1 = {death="7284.1.1"}
}
11869206 = {
	name="Sajida"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11369206
	mother=41369206

	7231.1.1 = {birth="7231.1.1"}
	7278.1.1 = {death="7278.1.1"}
}
11969206 = {
	name="Sadiq"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11669206
	mother=41669206

	7245.1.1 = {birth="7245.1.1"}
	7261.1.1 = {add_trait=trained_warrior}
	7267.1.1 = {death="7267.1.1"}
}
12069206 = {
	name="Aram"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=11669206
	mother=41669206

	7246.1.1 = {birth="7246.1.1"}
	7262.1.1 = {add_trait=skilled_warrior}
	7264.1.1 = {add_spouse=42069206}
	7297.1.1 = {death = {death_reason = death_accident}}
}
42069206 = {
	name="Sholah"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7246.1.1 = {birth="7246.1.1"}
	7297.1.1 = {death="7297.1.1"}
}
12169206 = {
	name="Aram"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12069206
	mother=42069206

	7272.1.1 = {birth="7272.1.1"}
	7288.1.1 = {add_trait=poor_warrior}
	7290.1.1 = {add_spouse=42169206}
	7335.1.1 = {death="7335.1.1"}
}
42169206 = {
	name="Rasa"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7272.1.1 = {birth="7272.1.1"}
	7335.1.1 = {death="7335.1.1"}
}
12269206 = {
	name="Sajida"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12069206
	mother=42069206

	7274.1.1 = {birth="7274.1.1"}
	7317.1.1 = {death="7317.1.1"}
}
12369206 = {
	name="Mubarak"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12069206
	mother=42069206

	7277.1.1 = {birth="7277.1.1"}
	7293.1.1 = {add_trait=trained_warrior}
	7336.1.1 = {death="7336.1.1"}
}
12469206 = {
	name="Yusuf"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12169206
	mother=42169206

	7290.1.1 = {birth="7290.1.1"}
	7306.1.1 = {add_trait=poor_warrior}
	7308.1.1 = {add_spouse=42469206}
	7359.1.1 = {death="7359.1.1"}
}
42469206 = {
	name="Sabba"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7290.1.1 = {birth="7290.1.1"}
	7359.1.1 = {death="7359.1.1"}
}
12569206 = {
	name="Semeah"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12169206
	mother=42169206

	7292.1.1 = {birth="7292.1.1"}
	7367.1.1 = {death="7367.1.1"}
}
12669206 = {
	name="Adila"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12169206
	mother=42169206

	7295.1.1 = {birth="7295.1.1"}
	7319.1.1 = {death="7319.1.1"}
}
12769206 = {
	name="Nasr"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12469206
	mother=42469206

	7316.1.1 = {birth="7316.1.1"}
	7332.1.1 = {add_trait=poor_warrior}
	7334.1.1 = {add_spouse=42769206}
	7394.1.1 = {death = {death_reason = death_murder}}
}
42769206 = {
	name="Shogofa"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7316.1.1 = {birth="7316.1.1"}
	7394.1.1 = {death="7394.1.1"}
}
12869206 = {
	name="Yasmin"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12469206
	mother=42469206

	7319.1.1 = {birth="7319.1.1"}
	7390.1.1 = {death="7390.1.1"}
}
12969206 = {
	name="Qawurd"	# a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12769206
	mother=42769206

	7347.1.1 = {birth="7347.1.1"}
	7363.1.1 = {add_trait=poor_warrior}
	7365.1.1 = {add_spouse=42969206}
}
42969206 = {
	name="Semeah"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7347.1.1 = {birth="7347.1.1"}
}
13069206 = {
	name="Fadl"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12769206
	mother=42769206

	7349.1.1 = {birth="7349.1.1"}
	7365.1.1 = {add_trait=skilled_warrior}
}
13169206 = {
	name="Reshawna"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12769206
	mother=42769206

	7352.1.1 = {birth="7352.1.1"}
}
13269206 = {
	name="Mirza"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12769206
	mother=42769206

	7355.1.1 = {birth="7355.1.1"}
	7371.1.1 = {add_trait=skilled_warrior}
	7392.1.1 = {death="7392.1.1"}
}
13369206 = {
	name="Taneen"	# not a lord
	female=yes
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12969206
	mother=42969206

	7366.1.1 = {birth="7366.1.1"}
}
13469206 = {
	name="Azam"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=12969206
	mother=42969206

	7367.1.1 = {birth="7367.1.1"}
	7383.1.1 = {add_trait=poor_warrior}
	7385.1.1 = {add_spouse=43469206}
}
43469206 = {
	name="Parand"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7367.1.1 = {birth="7367.1.1"}
}
13569206 = {
	name="Fadil"	# not a lord
	dynasty=69206

	religion="gods_nghai"
	culture="nghai"

	father=13469206
	mother=43469206

	7385.1.1 = {birth="7385.1.1"}
	7401.1.1 = {add_trait=poor_warrior}
}


####  House Hawang
10069207 = {
	name="Akin"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	7069.1.1 = {birth="7069.1.1"}
	7085.1.1 = {add_trait=poor_warrior}
	7087.1.1 = {add_spouse=40069207}
	7137.1.1 = {death="7137.1.1"}
}
40069207 = {
	name="Layla"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7069.1.1 = {birth="7069.1.1"}
	7137.1.1 = {death="7137.1.1"}
}
10169207 = {
	name="Habiba"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10069207
	mother=40069207

	7091.1.1 = {birth="7091.1.1"}
	7144.1.1 = {death="7144.1.1"}
}
10269207 = {
	name="Khalil"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10069207
	mother=40069207

	7092.1.1 = {birth="7092.1.1"}
	7108.1.1 = {add_trait=skilled_warrior}
	7110.1.1 = {add_spouse=40269207}
	7151.1.1 = {death="7151.1.1"}
}
40269207 = {
	name="Paymaneh"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7092.1.1 = {birth="7092.1.1"}
	7151.1.1 = {death="7151.1.1"}
}
10369207 = {
	name="Saaman"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10069207
	mother=40069207

	7094.1.1 = {birth="7094.1.1"}
	7116.1.1 = {death="7116.1.1"}
}
10469207 = {
	name="Parween"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10069207
	mother=40069207

	7095.1.1 = {birth="7095.1.1"}
	7134.1.1 = {death="7134.1.1"}
}
10569207 = {
	name="Yagana"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10269207
	mother=40269207

	7125.1.1 = {birth="7125.1.1"}
	7198.1.1 = {death="7198.1.1"}
}
10669207 = {
	name="Yakta"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10269207
	mother=40269207

	7128.1.1 = {birth="7128.1.1"}
	7205.1.1 = {death="7205.1.1"}
}
10769207 = {
	name="Idris"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10269207
	mother=40269207

	7131.1.1 = {birth="7131.1.1"}
	7147.1.1 = {add_trait=poor_warrior}
	7149.1.1 = {add_spouse=40769207}
	7167.1.1 = {death="7167.1.1"}
}
40769207 = {
	name="Qamara"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7131.1.1 = {birth="7131.1.1"}
	7167.1.1 = {death="7167.1.1"}
}
10869207 = {
	name="Shogofa"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10269207
	mother=40269207

	7132.1.1 = {birth="7132.1.1"}
	7168.1.1 = {death="7168.1.1"}
}
10969207 = {
	name="Zeyd"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10769207
	mother=40769207

	7152.1.1 = {birth="7152.1.1"}
	7168.1.1 = {add_trait=skilled_warrior}
	7170.1.1 = {add_spouse=40969207}
	7208.1.1 = {death="7208.1.1"}
}
40969207 = {
	name="Sajida"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7152.1.1 = {birth="7152.1.1"}
	7208.1.1 = {death="7208.1.1"}
}
11069207 = {
	name="Asiya"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10969207
	mother=40969207

	7172.1.1 = {birth="7172.1.1"}
	7213.1.1 = {death="7213.1.1"}
}
11169207 = {
	name="Sholah"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10969207
	mother=40969207

	7175.1.1 = {birth="7175.1.1"}
	7219.1.1 = {death="7219.1.1"}
}
11269207 = {
	name="Rashida"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10969207
	mother=40969207

	7176.1.1 = {birth="7176.1.1"}
	7234.1.1 = {death="7234.1.1"}
}
11369207 = {
	name="Akin"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=10969207
	mother=40969207

	7177.1.1 = {birth="7177.1.1"}
	7193.1.1 = {add_trait=skilled_warrior}
	7195.1.1 = {add_spouse=41369207}
	7234.1.1 = {death="7234.1.1"}
}
41369207 = {
	name="Parand"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7177.1.1 = {birth="7177.1.1"}
	7234.1.1 = {death="7234.1.1"}
}
11469207 = {
	name="Fadl"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=11369207
	mother=41369207

	7201.1.1 = {birth="7201.1.1"}
	7217.1.1 = {add_trait=skilled_warrior}
	7219.1.1 = {add_spouse=41469207}
	7255.1.1 = {death="7255.1.1"}
}
41469207 = {
	name="Taneen"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7201.1.1 = {birth="7201.1.1"}
	7255.1.1 = {death="7255.1.1"}
}
11569207 = {
	name="Abdul"	# not a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=11369207
	mother=41369207

	7202.1.1 = {birth="7202.1.1"}
	7218.1.1 = {add_trait=skilled_warrior}
	7277.1.1 = {death = {death_reason = death_accident}}
}
11669207 = {
	name="Youkhanna"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=11469207
	mother=41469207

	7219.1.1 = {birth="7219.1.1"}
	7235.1.1 = {add_trait=poor_warrior}
	7237.1.1 = {add_spouse=41669207}
	7268.1.1 = {death="7268.1.1"}
}
41669207 = {
	name="Habiba"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7219.1.1 = {birth="7219.1.1"}
	7268.1.1 = {death="7268.1.1"}
}
11769207 = {
	name="Sheeftah"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=11669207
	mother=41669207

	7241.1.1 = {birth="7241.1.1"}
	7306.1.1 = {death="7306.1.1"}
}
11869207 = {
	name="Saaman"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=11669207
	mother=41669207

	7242.1.1 = {birth="7242.1.1"}
	7243.1.1 = {death="7243.1.1"}
}
11969207 = {
	name="Mirza"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=11669207
	mother=41669207

	7243.1.1 = {birth="7243.1.1"}
	7259.1.1 = {add_trait=poor_warrior}
	7261.1.1 = {add_spouse=41969207}
	7289.1.1 = {death="7289.1.1"}
}
41969207 = {
	name="Qamara"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7243.1.1 = {birth="7243.1.1"}
	7289.1.1 = {death="7289.1.1"}
}
12069207 = {
	name="Zeyd"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=11969207
	mother=41969207

	7261.1.1 = {birth="7261.1.1"}
	7277.1.1 = {add_trait=master_warrior}
	7279.1.1 = {add_spouse=42069207}
	7320.1.1 = {death="7320.1.1"}
}
42069207 = {
	name="Adila"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7261.1.1 = {birth="7261.1.1"}
	7320.1.1 = {death="7320.1.1"}
}
12169207 = {
	name="Ghalib"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12069207
	mother=42069207

	7282.1.1 = {birth="7282.1.1"}
	7298.1.1 = {add_trait=skilled_warrior}
	7300.1.1 = {add_spouse=42169207}
	7344.1.1 = {death="7344.1.1"}
}
42169207 = {
	name="Semeah"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7282.1.1 = {birth="7282.1.1"}
	7344.1.1 = {death="7344.1.1"}
}
12269207 = {
	name="Nafisa"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12069207
	mother=42069207

	7284.1.1 = {birth="7284.1.1"}
	7324.1.1 = {death="7324.1.1"}
}
12369207 = {
	name="Nyawela"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12069207
	mother=42069207

	7285.1.1 = {birth="7285.1.1"}
	7314.1.1 = {death="7314.1.1"}
}
12469207 = {
	name="Is'mail"	# not a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12069207
	mother=42069207

	7288.1.1 = {birth="7288.1.1"}
	7304.1.1 = {add_trait=trained_warrior}
	7327.1.1 = {death = {death_reason = death_battle}}
}
12569207 = {
	name="Shamir"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12169207
	mother=42169207

	7305.1.1 = {birth="7305.1.1"}
	7321.1.1 = {add_trait=poor_warrior}
	7323.1.1 = {add_spouse=42569207}
	7361.1.1 = {death="7361.1.1"}
}
42569207 = {
	name="Kamala"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7305.1.1 = {birth="7305.1.1"}
	7361.1.1 = {death="7361.1.1"}
}
12669207 = {
	name="Saaman"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12569207
	mother=42569207

	7327.1.1 = {birth="7327.1.1"}
	7375.1.1 = {death="7375.1.1"}
}
12769207 = {
	name="Taliba"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12569207
	mother=42569207

	7329.1.1 = {birth="7329.1.1"}
	7368.1.1 = {death="7368.1.1"}
}
12869207 = {
	name="Semeah"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12569207
	mother=42569207

	7332.1.1 = {birth="7332.1.1"}
	7337.1.1 = {death="7337.1.1"}
}
12969207 = {
	name="Yahya"	# a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12569207
	mother=42569207

	7334.1.1 = {birth="7334.1.1"}
	7350.1.1 = {add_trait=poor_warrior}
	7352.1.1 = {add_spouse=42969207}
}
42969207 = {
	name="Rafiqa"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7334.1.1 = {birth="7334.1.1"}
}
13069207 = {
	name="Ali"	# not a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12969207
	mother=42969207

	7367.1.1 = {birth="7367.1.1"}
	7383.1.1 = {add_trait=poor_warrior}
	7385.1.1 = {add_spouse=43069207}
}
43069207 = {
	name="Shararah"
	female=yes

	religion="gods_nghai"
	culture="nghai"

	7367.1.1 = {birth="7367.1.1"}
}
13169207 = {
	name="Semeah"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12969207
	mother=42969207

	7369.1.1 = {birth="7369.1.1"}
}
13269207 = {
	name="Azam"	# not a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=12969207
	mother=42969207

	7372.1.1 = {birth="7372.1.1"}
	7388.1.1 = {add_trait=poor_warrior}
}
13369207 = {
	name="Sami"	# not a lord
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=13069207
	mother=43069207

	7395.1.1 = {birth="7395.1.1"}
	7411.1.1 = {add_trait=poor_warrior}
}
13469207 = {
	name="Tanaz"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=13069207
	mother=43069207

	7397.1.1 = {birth="7397.1.1"}
}
13569207 = {
	name="Nyawela"	# not a lord
	female=yes
	dynasty=69207

	religion="gods_nghai"
	culture="nghai"

	father=13069207
	mother=43069207

	7398.1.1 = {birth="7398.1.1"}
}
