900.1.1={
	law = succ_primogeniture
	law = cognatic_succession
	law = investiture_law_2
	law = first_night_1
}

###House Fisher
3306.1.1 = { holder=600174430 } # Ryman (nc) # House Fisher
3323.1.1 = { holder=601174430 } # Patrek (nc)
3331.1.1 = { holder=605174430 } # Addan (nc)
3390.1.1 = { holder=612174430 } # Tristan (nc)
3408.1.1 = { holder=613174430 } # Eyman (nc)
3442.1.1 = { holder=614174430 } # Edmure (nc)
3456.1.1 = { holder=0 } 

###House Mudd
5917.1.1 = { holder=600174399 } # Tristan (nc) # House Mudd
5981.1.1 = { holder=601174399 } # Addam (nc)
6000.1.1 = { holder=604174399 } # Tristifer (C)
6034.1.1 = { holder=605174399 } # Tristan (nc)
6072.1.1 = { holder=610174399 } # Tristifer (C)
6122.1.1 = { holder=611174399 } # Roslin (nc)
6141.1.1 = { holder=613174399 } # Tristan (nc)
6150.1.1 = { holder=614174399 } # Jon (nc)
6165.1.1 = { holder=617174399 } # Hosteen (nc)
6226.1.1 = { holder=618174399 } # Addan (nc)
6261.1.1 = { holder=619174399 } # Tristan (nc)
6266.1.1 = { holder=621174399 } # Addan (nc)
6291.1.1 = { holder=622174399 } # Marq (nc)
6318.1.1 = { holder=624174399 } # Tristifer (C)
6344.1.1 = { holder=625174399 } # Tyston (nc)
6349.1.1 = { holder=628174399 } # Brynden (nc)
6390.1.1 = { holder=629174399 } # Tristan (nc)
6402.1.1 = { holder=630174399 } # Roslin (nc)
6453.1.1 = { holder=631174399 } # Brynden (nc)
6475.1.1 = { holder=633174399 } # Addan (nc)
6497.1.1 = { holder=634174399 } # Tristan (nc)
6535.1.1 = { holder=635174399 } # Arson (nc)
6558.1.1 = { holder=637174399 } # Brynden (nc)
6572.1.1 = { holder=640174399 } # Tristan (nc)
6620.1.1 = { holder=642174399 } # Elmar (nc)
6642.1.1 = { holder=643174399 } # Tristifer (C)
6651.1.1 = { holder=644174399 } # Tristifer (C)

6670.1.1 = { holder=0} # Mudd royal line extinct

###House Justman
6939.1.1 = { holder=600174432 } # Benedict (C) # House Jusman
6962.1.1 = { holder=602174432 } # Benedict (C) 
7022.1.1 = { holder=606174432 } # Bernarr (C)
7029.1.1 = { holder=607174432 } # Edwyn (nc)
7038.1.1 = { holder=609174432 } # Brynden (nc)
7086.1.1 = { holder=615174432 } # Bernarr (C)

7101.1.1 = { holder=0 } # Justman royal line extinct

###House Teague
7200.1.1 = { holder=600174435 } # Torrence (C) # House Teague
7250.1.1 = { holder=601174435 } # Brynden (nc)
7251.1.1 = { holder=602174435 } # Petyr (nc)
7275.1.1 = { holder=604174435 } # Theo (C)
7290.1.1 = { holder=607174435 } # Willem (nC)
7326.1.1 = { holder=608174435 } # Harold (nC)
7354.1.1 = { holder=609174435 } # Randyl (nC)

