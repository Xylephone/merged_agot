#c_casterly_rock
3.1.1={
	liege="d_casterly_rock"
}

40.1.1 = { holder=20174406 } # Corlos, son of Caster # Line of Casterlys
80.1.1 = { holder=21174406 } # Carlon, son of Corlos
120.1.1 = { holder=22174406 } # Caston, son of Carlon
150.1.1 = { holder=23174406 } # Criston, son of Caston
156.1.1 = { holder=24174406 } # Corlos, son of Criston
186.1.1 = { holder=25174406 } # Caster, son of Corlos
198.1.1 = { holder=26174406 } # Castella, daughter of Caster, bride of Lann the Clever

210.1.1 = { holder=80260190 } # Lanna Lannister # Line of Lannisters
251.1.1 = { holder=80270190 } # Loreon the Lion
280.1.1 = { holder=0 }

# Pre-Andal period
5320.1.1 = { holder=80310190 } # Gerold the Great (C)
5365.1.1 = { holder = 0 }

6609.1.1 = { holder=80370190 } # Tymeon I (nc)
6619.1.1 = { holder=80380190 } # Tybolt Tunderbolt (C)
6665.1.1 = { holder=80390190 } # Tyrion III (C)
6682.1.1 = { holder=80400190 } # Gerold II (C)
6695.1.1 = { holder=80401190 } # Gerold III (C)
6732.1.1 = { holder=80403190 } # Joffrey I Lydden (C)

# Post-Andal period
6751.1.1 = { holder=80280190 } # Cerion (C) 
6778.1.1 = { holder=8009190 } # Tommen (I C)
6799.1.1 = { holder=80290190 } # Loreon II (C)
6824.1.1 = { holder=80350190 } # Lancel I the lion (C)
6842.1.1 = { holder=80300190 } # Loreon III the Limp (C)
6859.1.1 = { holder = 0 }

7095.1.1 = { holder=1000190 } # Daven (nc)
7121.1.1 = { holder=1001190 } # Lorimer (nc)
7159.1.1 = { holder=1002190 } # Tymeon (nc)
7197.1.1 = { holder=1005190 } # Tywin (nc)
7207.1.1 = { holder=1006190 } # Loreon (nc)
7241.1.1 = { holder=1007190 } # Lorimar (nc)
7268.1.1 = { holder=1010190 } # Damon (nc)
7273.1.1 = { holder=1013190 } # Tytos (nc)
7311.1.1 = { holder=1014190 } # Tyland (nc)
7323.1.1 = { holder=1015190 } # Lucas (nc)
7381.1.1 = { holder=1020190 } # Tytos (nc)
7411.1.1 = { holder=1021190 } # Tyryan (nc)


