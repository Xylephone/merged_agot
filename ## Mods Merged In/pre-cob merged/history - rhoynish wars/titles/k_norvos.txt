7055.1.1 = { 
	law = slavery_2
	law = centralization_3
	law = succ_open_elective
	effect = {
		holder_scope = { 
			e_new_valyria = {
				holder_scope = {
					make_tributary = { who = PREVPREV tributary_type = valyrian_tributary }
				}
			}
		}	
	}
} 
7095.1.1 = { holder=10069116 }
7127.1.1 = { holder=100069118 }
7139.1.1 = { holder=10469116 } # Moredo (nc)
7153.1.1 = { holder=100669121 } # Gyleno (nc)
7173.1.1 = { holder=100869118 } # Qarro (nc)
7210.1.1 = { holder=100869156 } # Terro (nc)
7229.1.1 = { holder=101169118 } # Collio (nc)
7250.1.1 = { holder=101569156 } # Orbelo (nc)
7293.1.1 = { holder=101769122 } # Noho (nc)
7331.1.1 = { holder=101969122 } # Izembaro (nc)
7336.1.1 = { holder=102369156 } # Gyloro (nc)
7342.1.1 = { holder=102669119 } # Ordello (nc)
7366.1.1 = { holder=102069117 } # Groleo (nc)
7384.1.1 = { holder=102769121 } # Ezzelyno (nc)

