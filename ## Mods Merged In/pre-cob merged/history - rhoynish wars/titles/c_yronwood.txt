210.1.1 = { 
	holder=150012 #Yoren
	liege="d_stoneway"
	law = succ_primogeniture
	law = cognatic_succession
	effect = {
		holder_scope = { give_minor_title = title_the_bloodroyal }
	}
}
235.1.1 = { holder = 0 }
6400.1.1 = { holder = 152012 } #Yorick I
6440.1.1 = { holder = 153012 } #Yorick II
6468.1.1 = { holder = 154012 } #Olyvar
6481.1.1 = { holder = 0 }

7027.1.1 = { holder = 160012 }
7057.1.1 = { holder = 161012 }
7088.1.1 = { holder=30012 } # Yandry (nc)
7107.1.1 = { holder=30212 } # Cletus (nc)
7122.1.1 = { holder=30312 } # Ulrick (nc)
7148.1.1 = { holder=31012 } # Timoth (nc)
7175.1.1 = { holder=31112 } # Anders (nc)
7209.1.1 = { holder=31212 } # Castos (nc)
7210.1.1 = { holder=31312 } # Alaric (nc)

7241.1.1 = { holder = 156012 } #Yorick III
7268.1.1 = { holder = 157012 } #Yorick IV
7295.1.1 = { holder = 158012 } #Yorick V

7311.1.1 = { holder=32312 } # Trystane (nc)
7330.1.1 = { holder=32612 } # Edgar (c)
7374.1.1 = { holder=6969112} # Ormond (c)
7392.1.1 = {holder=12} # Anders (c)
