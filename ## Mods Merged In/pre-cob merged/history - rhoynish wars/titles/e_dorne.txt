7300.1.1={ # About 700 years before the events of the books Nymeria landed.
	law = succ_primogeniture
	law = true_cognatic_succession
	law = centralization_2
	law = investiture_law_2
	holder=2100001 #Mors
}

7309.1.1 = {
	holder = 2000001 #Nymeria
}
7336.1.1 = { holder=6055001} # Nymeria II (nc)
7358.1.1 = { holder=6056001} # Mors II (C)
7390.1.1 = { holder=60570001} # Malon (nc)
7418.1.1 = { holder=60840001} # Olyvar (nc)