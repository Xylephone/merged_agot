decisions = {
	expel_andals = {
		is_high_prio = yes
		only_playable = yes
		potential = {
			demesne_size = 1
			NOT = { culture_group = andal }
			NOT = { culture = ironborn }
			any_demesne_province = { has_province_modifier = andal_settlers }
		}
		allow = {
			conditional_tooltip = {
				trigger = { independent = no }
				NOT = { 
					any_liege = { culture_group = andal } 
				}	
			}	
			top_liege = {
				custom_tooltip = {
					text = TOOLTIPINDEPENDENTCONDITION
					hidden_tooltip = {
						independent = yes
						OR = {
							tier = EMPEROR
							primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
						}	
					}
				}
			}	
		}
		effect = {
			character_event = { id = andal.25 tooltip = TOOLTIPandal.25 }
			if = {
				limit = { religion = the_seven }
				piety = -250
			}
		}
		ai_check_interval = 6
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				religion = the_seven
			}
			modifier = {
				factor = 0
				OR = {
					trait = kind
					trait = just
					trait = humble
					trait = honorable
				}	
				NOT = { trait = ruthless }
				NOT = { trait = cruel }
				NOT = { trait = arbitrary }
			}
		}
	}
	start_andal_adventure = { #allows player to abandon andal lands when they like
		is_high_prio = yes
		ai = no
		potential = {
			culture = old_andal
			NOT = { #only andals located in canon abadoned lands do this
				any_demesne_province = { 
					NOR = {
						region = world_lorath
						region = world_braavos
						region = world_andalos
					}	
				} 
			}
		}
		allow = {
			religion = the_seven
			is_female = no
			martial = 8
			prestige = 300
			OR = {
				trait = ambitious
				trait = diligent
				trait = ruthless
				trait = zealous
			}
			NOT = { trait = content }	
			NOT = { tier = EMPEROR }
			NOT = { any_liege = { tier = EMPEROR } }				
			demesne_size = 1
			war = no		
			is_adult = yes
			prisoner = no
			is_incapable = no
			has_severe_disability_trigger = no
			has_medium_disability_trigger = no				
			
		}
		effect = {
			custom_tooltip = {
				text = TOOLTIP_start_andal_adventure
				hidden_tooltip = {
					any_realm_lord = {
						limit = { ai = no }
						character_event = { id = andal.9 }
					}
					any_vassal = {
						limit = { NOT = { tier = BARON } }
						set_defacto_liege = THIS
					}
					capital_scope = { save_event_target_as = andal_home_province }
					create_title = {
						tier = DUKE
						landless = yes
						#adventurer = yes
						culture = THIS
						name = "ANDAL_HOST"
						holder = THIS
					}
					any_demesne_title = {
						limit = {
							higher_tier_than = COUNT
							is_landless_type_title = no
						}
						destroy_landed_title = THIS
					}
					any_demesne_province = { 
						make_province_ruin_effect = yes	
					} 
					set_character_flag = canon_andal_dynasty
					set_character_flag = andal_adventurer
					
					spawn_unit = {
						province = event_target:andal_home_province
						home = event_target:andal_home_province		
						troops = {
							light_infantry = { 650 650 }
							heavy_infantry = { 1200 1200 }
							light_cavalry = { 325 325 }
							knights = { 125 125 }
							archers = { 200 200 }
						}
						attrition = 1.0
						earmark = andal_host
					}
					spawn_unit = {
						province = event_target:andal_home_province			
						troops = {
							light_infantry = { 650 650 }
							heavy_infantry = { 1200 1200 }
							light_cavalry = { 325 325 }
							knights = { 125 125 }
							archers = { 200 200 }
						}
						attrition = 1.0
						earmark = andal_host
					}
					spawn_unit = {
						province = event_target:andal_home_province
						home = event_target:andal_home_province			
						troops = {
							light_infantry = { 650 650 }
							heavy_infantry = { 1200 1200 }
							light_cavalry = { 325 325 }
							knights = { 125 125 }
							archers = { 200 200 }
						}
						attrition = 1.0
						earmark = andal_host
					}
					spawn_fleet = {
						province = closest # closest sea zone
						owner = THIS
						troops =
						{
							galleys = { 75 75 }
						}
						earmark = andal_fleet
						cannot_inherit = yes
						#disband_on_peace = yes
					}
					set_defacto_liege = THIS
				}
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	##Expand Red Keep
	expand_the_eyrie = {
		is_high_prio = yes
		only_independent = yes
		potential = {
			OR = {
				has_landed_title = e_iron_throne
				has_landed_title = e_vale
			}	
			has_landed_title = c_the_eyrie
			culture_group = andal
			NOR = {
				has_global_flag = c_the_eyrie_1
				has_global_flag = c_the_eyrie_2
				has_global_flag = c_the_eyrie_3
				has_global_flag = c_the_eyrie_4
			}
			b_the_eyrie = { NOT = { has_building = ca_asoiaf_vale_basevalue_5 } }
		}
		allow = {
			has_landed_title = c_the_eyrie
			NOT = { has_character_modifier = in_seclusion }
		}
		effect = {
			treasury = -250
			custom_tooltip = { text = TOOLTIPexpand_the_eyrie }
			if = {
				limit = { has_global_flag = c_the_eyrie_3_done }
				clr_global_flag = c_the_eyrie_3_done
				set_global_flag = c_the_eyrie_4
			}
			else_if = {
				limit = { has_global_flag = c_the_eyrie_2_done }
				clr_global_flag = c_the_eyrie_2_done
				set_global_flag = c_the_eyrie_3
			}
			else_if = {
				limit = { has_global_flag = c_the_eyrie_1_done }
				clr_global_flag = c_the_eyrie_1_done
				set_global_flag = c_the_eyrie_2
			}
			else = {
				136 = {
					build_holding = {
					   title = b_the_eyrie
					   type = castle
					   holder = ROOT
					}	
				}				
				hidden_tooltip = {
					c_the_eyrie = { 
						set_name = "" 
						hidden_tooltip = { location = { set_name = "The Eyrie" } }
					}
					b_the_gates_of_the_moon = { set_name = "" }
					b_the_eyrie = { 
						add_building = ca_asoiaf_vale_basevalue_1 
					}
				}	
				set_global_flag = c_the_eyrie_1
				add_artifact = falcon_crown
			}			
		}
		ai_check_interval = 12
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				war = yes
			}
			modifier = {
				factor = 0.2
				NOT = { treasury = 200 }
			}
		}
	}
	subdue_andals = {
		is_high_prio = yes
		only_playable = yes
		potential = {
			has_global_flag = andals_invasions
			capital_scope = { region = world_westeros }
			demesne_size = 1
			NOT = { culture_group = andal }
		}
		allow = {		
			custom_tooltip = {
				text = TOOLTIPINDEPENDENTCONDITION
				hidden_tooltip = {
					independent = yes
					OR = {
						tier = EMPEROR
						primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
					}	
				}
			}
			has_character_modifier = defeated_an_andal_invasion
			custom_tooltip = { #No Independent Andals
				text = TOOLTIPsubdue_andalsA
				hidden_tooltip = {
					NOT = {
						any_independent_ruler = {
							culture_group = andal
							demesne_size = 1
							NOT = { tier = BARON }
						}
					}					
				}
			}
			custom_tooltip = { #Controls Andalos
				text = TOOLTIPsubdue_andalsB
				hidden_tooltip = {
					e_andalos = {
						OR = {
							ROOT = { completely_controls = PREV }
							NOT = {
								any_direct_de_jure_vassal_title = {
									holder_scope = {
										NOT = { same_realm = ROOT }
										NOT = { culture_group = unoccupied_group }
									}
								}
							}	
						}	
					}	
					k_andalos = {
						OR = {
							ROOT = { completely_controls = PREV }
							NOT = {
								any_direct_de_jure_vassal_title = {
									holder_scope = {
										NOT = { same_realm = ROOT }
										NOT = { culture_group = unoccupied_group }
									}
								}
							}	
						}	
					}					
				}
			}
			capital_scope = { NOT = { has_province_modifier = andal_settlers } }
			NOT = { religion = the_seven }
			NOT = { any_spouse = { culture_group = andal } }
			war = no
			has_regent = no
			prestige = 2000
		}
		effect = {
			custom_tooltip = {
				text = TOOLTIPsubdue_andals
				clr_global_flag = andals_invasions #Invasions are ceased
				set_global_flag = andals_invasions_subdued
				if = {
					limit = { has_nickname = no }
					random_list = {
						1 = { give_nickname = nick_the_andal_bane }
						1 = { give_nickname = nick_the_scourge_of_the_andals }
						1 = { give_nickname = nick_the_butcher_of_the_andals }
						1 = { give_nickname = nick_the_andal_breaker }
						1 = { give_nickname = nick_the_vanquisher }
						1 = { give_nickname = nick_the_great }
						1 = { give_nickname = nick_the_victorious }
						1 = { give_nickname = nick_the_avenger }						
						1 = { give_nickname = nick_the_legendary }
						1 = { give_nickname = nick_the_magnificent }
						1 = { give_nickname = nick_the_illustrious }
						1 = { give_nickname = nick_the_glorious }
						1 = { give_nickname = nick_the_amazing }
						1 = { give_nickname = nick_the_neveryield }
						1 = { give_nickname = nick_greatheart }
						1 = { give_nickname = nick_the_breaker }
						1 = { give_nickname = nick_the_saviour }
						1 = { give_nickname = nick_the_triumphant }
					}
				}
				any_player = {
					character_event = { id = andal.91 }
				}
			}	
			prestige = 1000
			piety = 500
		}
		ai_check_interval = 12
		ai_will_do = {
			factor = 1
		}
	}
}

settlement_decisions = {
	expand_riverrun = {
		filter = owned
		ai_target_filter = owned
		only_playable = yes
		is_high_prio = yes
		
		from_potential = {
			culture_group = andal
		}
		potential = {
			title = b_riverrun				
			holder_scope = { character = FROM }	
			NOT = { has_holding_modifier = upgrade_bv }
			NOT = { has_building = ca_asoiaf_river_basevalue_6 }
		}
		allow = {
			FROM = { wealth = 250 }
		}
		effect = {
			hidden_tooltip = {
				FROM = { character_event = { id = 20951 days = 5 } }
			}
			FROM = { wealth = -250 }
			add_holding_modifier = {
				name = upgrade_bv
				duration = -1
			}
		}
		ai_check_interval = 12
		ai_will_do = {
			factor = 1
		}
	}
}



