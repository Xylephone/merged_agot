1.1.1 = { 
	law = slavery_2
	law = succ_primogeniture
	law = centralization_0
}
2000.1.1 = { 
	holder=100777308 #Bloodstone Emperor
}
2010.1.1 = { holder = 0 }

2100.1.1 = { 
	holder=10055956 #Har Loi, grey emperor
	effect = {
		dead_target = {	
			limit = { character = 10055956 }
			remove_trait = dead_target
			set_special_character_title = nick_grey_emperor
		}
	}	
}
2135.1.1 = { holder=0 }

2256.1.1 = { 
	holder=10055957 #Choq Choq, indigo emperor
	effect = {
		dead_target = {	
			limit = { character = 10055957 }
			remove_trait = dead_target
			set_special_character_title = nick_indigo_emperor
		}
	}
}
2293.1.1 = { holder=0 }

2360.1.1 = { 
	holder=10055955 #Mengo Quen, jade-green emperor
	effect = {
		dead_target = {	
			limit = { character = 10055955 }
			remove_trait = dead_target
			set_special_character_title = nick_jadegreen_emperor
		}
	}
}
2391.1.1 = { holder=0 }

#Nine Eunuchs
2800.1.1 = { 
	holder=10055961 
	effect = {
		dead_target = {	
			limit = { character = 10055961 }
			remove_trait = dead_target
			set_special_character_title = nick_pearlwhite_emperor
		}
	}
} # Qaun (nc)
2819.1.1 = { 
	holder=10155961 
	effect = {
		dead_target = {	
			limit = { character = 10155961 }
			remove_trait = dead_target
			set_special_character_title = nick_pearlwhite_emperor
		}
	}
} # An (nc)
2839.1.1 = {
	holder=10355961 
	effect = {
		dead_target = {	
			limit = { character = 10355961 }
			remove_trait = dead_target
			set_special_character_title = nick_pearlwhite_emperor
		}
	}
} # Gai (nc)
2858.1.1 = { 
	holder=10455961 
	effect = {
		dead_target = {	
			limit = { character = 10455961 }
			remove_trait = dead_target
			set_special_character_title = nick_pearlwhite_emperor
		}
	}
} # Mao (nc)
2866.1.1 = { 
	holder=10555961 
	effect = {
		dead_target = {	
			limit = { character = 10555961 }
			remove_trait = dead_target
			set_special_character_title = nick_pearlwhite_emperor
		}
	}
} # Tho (nc)
2881.1.1 = { 
	holder=10655961 
	effect = {
		dead_target = {	
			limit = { character = 10655961 }
			remove_trait = dead_target
			set_special_character_title = nick_pearlwhite_emperor
		}
	}
} # Kim (nc)
2905.1.1 = { 
	holder=10755961 
	effect = {
		dead_target = {	
			limit = { character = 10755961 }
			remove_trait = dead_target
			set_special_character_title = nick_pearlwhite_emperor
		}
	}
} # Dong (nc)
2931.1.1 = { 
	holder=10855961 
	effect = {
		dead_target = {	
			limit = { character = 10855961 }
			remove_trait = dead_target
			set_special_character_title = nick_pearlwhite_emperor
		}
	}
} # Quan (nc)
2940.1.1 = { 
	holder=10955961 
	effect = {
		dead_target = {	
			limit = { character = 10955961  }
			remove_trait = dead_target
			set_special_character_title = nick_pearlwhite_emperor
		}
	}
} # Huu (nc)
2945.1.1 = { holder=0 }
##


2950.1.1 = { 
	holder=10055958 #Jar Har, sixth sea-green emperor
	effect = {
		dead_target = {	
			limit = { character = 10055958  }
			remove_trait = dead_target
			set_special_character_title = nick_seagreen_emperor
		}
	}
}
2993.1.1 = { 
	holder=20055958 #Jar Joq, seventh sea-green emperor
	effect = {
		dead_target = {	
			limit = { character = 20055958  }
			remove_trait = dead_target
			set_special_character_title = nick_seagreen_emperor
		}
	}
}
3017.1.1 = { 
	holder=30055958 #Jar Han, eight sea-green emperor
	effect = {
		dead_target = {	
			limit = { character = 30055958  }
			remove_trait = dead_target
			set_special_character_title = nick_seagreen_emperor
		}
	}
}
3038.1.1 = { holder=0 }

6380.1.1 = { 
	holder=10065961 #Lo Tho, twenty-second of the scarlet emperors
	effect = {
		dead_target = {	
			limit = { character = 10065961 }
			remove_trait = dead_target
			set_special_character_title = nick_scarlet_emperor
		}
	}
}
6429.1.1 = { holder=0 }

6468.1.1 = { 
	holder=10005961  # Chao (nc)
	effect = {
		set_title_flag = scarlet_emperor
		set_global_flag = yellow_emperor_invasion #prevent yellow emperor invasion in this bookmark
		holder_scope = {
			set_special_character_title = nick_scarlet_emperor
			set_dynasty_flag = scarlet_emperor
			set_dynasty_flag = yi_ti_emperor_colour_assigned
			any_dynasty_member_even_if_dead = {
				limit = { primary_title = { title = e_yi_ti } }
				set_dynasty_flag = yi_ti_emperor_colour_assigned
			}
		}	
	}
}
6506.1.1 = { holder=10365961 } # Lo Doq canon 34th scarlet emperor
6520.1.1 = { holder=110055961 } # Huu (nc)
6550.1.1 = { holder=100065961 } # Tu (nc)
6560.1.1 = { holder=10085961 } # Phong (nc)
6580.1.1 = { holder=10105961 } # Quen (nc)
6619.1.1 = { holder=10135961 } # Tu (nc)
6661.1.1 = { holder=110155961 } # Binh (nc)
6666.1.1 = { holder=10185961 } # Gai (nc)
6670.1.1 = { holder=10065963 } # Lo Han canon 42nd scarlet emperor
6698.1.1 = { holder=10165963 } # Lo Bu canon 43rd scarlet emperor


