#NOTE: The holder of this title has the House Dondarrion dynasty tracker set in roberts_rebellion_events
1.1.1 = { liege="e_stormlands" }

6472.1.1 = { holder = 300303 } # Alaric Dondarrion (nc)

6484.1.1 = { holder = 301303 } # Donnel Dondarrion (nc)
6518.1.1 = { holder = 302303 } # Harbert Dondarrion (nc)
6547.1.1 = { holder = 303303 } # Galladon Dondarrion (nc)
6564.1.1 = { holder = 304303 } # Maric Dondarrion (nc)
6585.1.1 = { holder = 306303 } # Rickard Dondarrion (nc)
6638.1.1 = { holder = 307303 } # Devan Dondarrion (nc)
6654.1.1 = { holder = 200303 } # Lyonel Dondarrion (C)
6690.1.1 = { holder = 201303 } # Manfred Dondarrion (C)
6719.1.1 = { holder = 310303 } # Gulian Dondarrion (nc)
6749.1.1 = { holder = 303 } # Edric Dondarrion (nc)
