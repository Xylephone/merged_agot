2579.1.1={ 
	holder=174405 #The Griffin King
	name = c_griffins_peak
	effect = {
		if = {
			limit = { NOT = { year = 6538 } }
			location = { set_name = c_griffins_peak }
		}	
	}
 }	
2600.1.1 = { holder = 0 }

6538.1.1 = { 
	holder=900178 #Artys I Arryn
}	
6539.1.1 = { 
	reset_name = yes
	effect = {
		set_global_flag = c_the_eyrie_1 
		location = { set_name = c_the_eyrie }
	}
}	
6563.1.1 = { holder=5060178 }	#Osric I
6575.1.1 = { holder=5560178 }	#Alester I
6580.1.1 = {
	effect = { 
		clr_global_flag = c_the_eyrie_1
		set_global_flag = c_the_eyrie_1_done
	}
}
6586.1.1 = { holder=5061178 }	#Roland I
6602.1.1 = { holder=550178 }	#Mathos I
6616.1.1 = { holder=551178 }	#Osric II
6620.1.1 = {
	effect = { 
		clr_global_flag = c_the_eyrie_1_done
		set_global_flag = c_the_eyrie_2_done
	}
}
6634.1.1 = { holder=552178 }	#Roland II
6639.1.1 = { holder=553178 }	#Robin

6652.1.1 = { holder=557178 }	#Hugh
6660.1.1 = {
	effect = { 
		clr_global_flag = c_the_eyrie_2_done
		set_global_flag = c_the_eyrie_3_done
	}
}
6683.1.1 = { holder=559178 }	#Hugo
7000.1.1 = {
	effect = { 
		clr_global_flag = c_the_eyrie_3_done
	}
}
6710.1.1 = { holder=560178 }	#Alester II
6735.1.1 = { holder=561178 }	#Mathos II
6761.1.1 = { holder=5161178 }	#Ronnel
