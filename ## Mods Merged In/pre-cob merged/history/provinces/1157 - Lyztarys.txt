
# County Title
title = c_lyztarys

# Settlements
max_settlements = 3

b_lyztarys = castle
b_lyztarys2 = city
b_lyztarys3 = temple

# Misc
culture = high_valyrian
religion = valyrian_rel

# History
1.1.1 = {
	b_lyztarys = ca_asoiaf_valyria_basevalue_1
	b_lyztarys = ca_asoiaf_valyria_basevalue_2
		
	b_lyztarys2 = ct_asoiaf_valyria_basevalue_1
}
4000.1.1 = {
	b_lyztarys = ca_asoiaf_valyria_basevalue_3
	
	b_lyztarys2 = ct_asoiaf_valyria_basevalue_2
}

