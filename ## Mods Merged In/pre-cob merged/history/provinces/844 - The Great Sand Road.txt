# 680 - The Great Sand Road

# County Title
title = c_hyrkoon

# Settlements
max_settlements = 3

b_hyrkoon = castle
b_hyrkoon_city = city
b_hyrkoon_temple = temple


# Misc
culture = hyrkooni
religion = gods_bone_mountains

terrain = mountain

# History
1.1.1 = {

	b_hyrkoon = ca_asoiaf_eastern_basevalue_1
	b_hyrkoon = ca_asoiaf_eastern_basevalue_2
	b_hyrkoon = ca_asoiaf_eastern_basevalue_3
	b_hyrkoon = ca_asoiaf_eastern_basevalue_4
	b_hyrkoon = ca_asoiaf_eastern_basevalue_5
	b_hyrkoon = ca_hyrkooni_fortress
	
	b_hyrkoon_city = ct_asoiaf_eastern_basevalue_1
	b_hyrkoon_city = ct_asoiaf_eastern_basevalue_2
	b_hyrkoon_city = ct_asoiaf_eastern_basevalue_3
	b_hyrkoon_city = ct_asoiaf_eastern_basevalue_4


}


