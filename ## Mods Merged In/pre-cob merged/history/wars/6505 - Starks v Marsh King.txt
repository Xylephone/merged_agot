name = "Northern Invasion of the Neck"

casus_belli={
	casus_belli=invasion
	actor=814059 # Rickard Stark
	recipient=1050126 # Marsh King
	landed_title=k_theneckTK
	date=6505.1.1
}

6505.1.1 = {
	add_defender = 1050126 # Marsh King
	add_attacker = 814059 # Rickard Stark
}

6506.1.1 = {
	rem_defender = 1050126 # Marsh King
	rem_attacker = 814059 # Rickard Stark
}                                                                                                                                                                                                                                                                                                                