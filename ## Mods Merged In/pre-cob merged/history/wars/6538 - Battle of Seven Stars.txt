name = "The Battle of Seven Stars"

casus_belli={
	casus_belli=invasion
	actor=557187 # Robar Royce
	recipient=900178 	# Artys Arryn
	landed_title=c_the_eyrie
	date=6538.1.1
}

6538.1.1 = {
	add_defender = 900178 	# Artys Arryn
	add_attacker = 557187 # Robar Royce
	
	#add_attacker = 551176 	# Corbray
	#add_attacker = 3007182 	# Waynwood
	#add_attacker = 500172 	# Ninestars
}

6539.1.1 = {
	rem_defender = 900178 	# Artys Arryn
	rem_attacker = 557187 # Robar Royce	
	#rem_attacker = 3007182 	# Waynwood
	#rem_attacker = 500172 	# Ninestars
}                                                                                                                                                                                                                                                                                                                