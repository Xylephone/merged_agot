7200.1.1={
	liege="e_riverlands"
	law = succ_primogeniture
}
###House Teague
7200.1.1 = { holder=600174435 } # Torrence (C) # House Teague
7250.1.1 = { holder=601174435 } # Brynden (nc)
7251.1.1 = { holder=602174435 } # Petyr (nc)
7275.1.1 = { holder=604174435 } # Theo (C)
7290.1.1 = { holder=0 }
7595.1.1 = { holder=601074435 } # Jon (C)
7637.1.1 = { holder=600074435 } # Theomore (C)
7653.1.1 = { holder=607174435 } # Humfrey (C)
7680.1.1 = { holder=608174435 } # Humfrey  II( C)
7680.1.2 = { holder=609174435 } # Hollis  ( C)
7680.1.3 = { holder=611174435 } # Tyler C)
7680.1.4 = { holder=612174435 } # Damon( C)

#Durrandons of the Stormlands
7680.1.5 = {
     holder = 3501544 #Arlan III
}
7701.1.1={
     holder = 3511544 #Arlan IV
}
7719.1.1={
     holder = 3061544 #Anrec
}	 
7757.1.1 = {
     holder = 3051544 #Arcon
}	
7760.1.1 = {
     holder = 3041544 #Durran
}	
7803.1.1 = {
     holder = 3031544 #Alrec
}	
7840.1.1 = {
     holder = 3021544 #Durran
}	
7887.1.1 = {
     holder = 3011544 #Angron
}
