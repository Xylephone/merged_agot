2600.1.1={
	liege="d_the_fingers"
	law = succ_primogeniture
}

6070.1.1 = { holder = 550881800 } #Dywen Shell, King of The Fingers
6079.1.1={ holder = 0 }

6949.1.1 = { liege="e_vale" }

7510.1.1={ holder=214178 }	#Artys III
7565.1.1={ holder=213178 }	#Oswell II
7590.1.1={ holder=211178 }	#Robar
7595.1.1={ holder=210178 }  #Ronnel Arryn - The Arryn that bent the knee. Last person to wear the Falcon Crown.


7638.4.1 = { holder=251178 } # Jonos Arryn (brother usurper)
7639.1.1 = { holder=5085178}	#Hubert Arryn
7650.1.1 = { holder=208178 }	#Elbert Arryn
7660.1.1={ holder=207178 }	#Rodrik Arryn
7700.1.1 = { holder=5052178 }	#Jeyne Arryn
7734.6.1 = { holder=5050178 }	#Joffrey Arryn
7751.1.1={ holder=204178 }	#Jonothor Arryn
7780.1.1={ holder=202178 }	#Donnel Arryn
7818.1.1={ holder=200178 }	#Othor Arryn
7820.1.1={ holder=94001 }	#Jesper Arryn

7841.1.1={
	holder=94042
}
7878.1.1={
	holder=94043 #Littlefinger
}
7897.1.1={
	liege = "e_iron_throne"
}
# 7899.11.5 = {
	# holder=94043 #Littlefinger
# }
