#NOTE: The holder of this title has the House Caswell dynasty tracker set in roberts_rebellion_events
500.1.1={ law=succ_primogeniture law=cognatic_succession
	liege="e_reach"
	name = c_stonebridge
	effect = {
		if = {
			limit = { NOT = { year = 8050 } }
			location = { set_name = c_stonebridge }
		}	
	}
}

7556.1.1 = { holder=3000264 } # Robert (nc)

7599.1.1 = { holder=3001264 } # Mervyn (nc)
7613.1.1 = { holder=3004264 } # Orton (nc)
7642.1.1 = { holder=3008264 } # Lorent (nc)
7668.1.1 = { holder=3011264 } # Mark (nc)
7710.1.1 = { holder=3014264 } # Delena (nc)
7729.11.2={ holder=3016264 } # Dontos (nc)
7751.1.1 = { holder=3017264 } # Dickon (nc)

7788.1.1={
	holder=88294 #Armon (nc)
}
7807.1.1={
	holder=88293 #Joffrey 
}
7842.1.1={
	holder=88292 #Mathis (nc) 
}
7867.1.1={
	holder=88036 #Desmind (nc)
}
7893.1.1={
	holder=88037 #Lorent
}




