1.1.1={
	liege="e_yi_ti"
	law = succ_appointment
	law = agnatic_succession
	effect = {
		set_title_flag = military_command
		holder_scope = { 
			if = {
				limit = { primary_title = { title = PREVPREV } }
				set_government_type = military_command_government 
				PREV = { succession = appointment }
				recalc_succession = yes
			}	
		}	
	}	
}
2996.1.1 = { 
	holder=10055954 # Gafur (nc)
} 
3027.1.1 = { holder=10155954 } # Hasan_Hasan (nc)
3056.1.1 = { holder=10655954 } # Mukhtar (nc)
3079.1.1 = { holder=10755954 } # Azam (nc)
3081.1.1 = { holder=11155954 } # Fadil (nc)
3123.1.1 = { holder=11455954 } # Mukhtar (nc)
3156.1.1 = { holder=11755954 } # Ramadan (nc)
3165.1.1 = { holder=11955954 } # Jibril (nc)
3197.1.1 = { holder=12155954 } # Uways (nc)
3221.1.1 = { holder=12355954 } # Burhanaddin (nc)
3252.1.1 = { holder=13055954 } # Bakr (nc)



