3.1.1={
	law = succ_primogeniture
	law = cognatic_succession
	law = centralization_1
	law = investiture_law_2
	law = first_night_1
}
20.1.1={holder=88151} # Garth I
80.1.1={holder=65101524} # Garth II # Post Long Night
98.1.1={holder=65201524} # Gwayne I
110.1.1={holder=65301524} # Gareth I
123.1.1={holder=65401524} # Gordan I Grey-Eyes
167.1.1={holder=59501524} # Mervyn I
180.1.1={holder=0}

2550.1.1={holder=68901524} # Garth III the Great # Old-Gods era
2602.1.1={holder=65501524} # Gareth II
2604.1.1={holder=65601524} # Gyles I the Woe
2632.1.1={holder=65701524} # Garland I
2645.1.1={holder=59601524} # Mervyn II
2650.1.1={holder=65801524} # John I
2660.1.1={holder=65901524} # Garland II
2705.1.1={holder=67001524} # Gwayne II
2720.1.1={holder=67101524} # Gwayne III
2760.1.1={holder=67201524} # John II the Tall
2780.1.1={holder=67301524} # Garth IV
2790.1.1={holder=67401524} # Garth V the Hammer
2798.1.1={holder=67501524} # Mern I
2860.1.1={holder=65001524} # Garse I
2880.1.1={holder=64001524} # Garse II
2901.1.1={holder=67601524} # Garth VI the Morningstar

2985.1.1 = { holder=10001254 } # Colin (nc)
3027.1.1 = { holder=10011254 } # Gormon (nc)
3037.1.1 = { holder=10021254 } # Garland (nc)
3077.1.1 = { holder=10031254 } # Morm (nc)
3110.1.1 = { holder=10041254 } # Owain (nc)
3113.1.1 = { holder=10051254 } # Gareth (nc)
3140.1.1 = { holder=10061254 } # Gormon (nc)
3161.1.1 = { holder=10071254 } # Garrett (nc)
3209.1.1 = { holder=10081254 } # Cleyton (nc)
3230.1.1 = { holder=10091254 } # Brandon (nc)
3267.1.1 = { holder=10101254 } # Garland (nc)
3275.1.1 = { holder=10121254 } # Gareth (nc)
3298.1.1 = { holder=10131254 } # Elwood (nc)
3313.1.1 = { holder=10141254 } # Raymund (nc)