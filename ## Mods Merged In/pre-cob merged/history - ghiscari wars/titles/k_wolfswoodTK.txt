1.1.1={
	law = succ_primogeniture
	law = cognatic_succession
	effect = {
		holder_scope = {
			set_special_character_title = wolfking_male
		}	
	}
}

2979.1.1={
	holder=18062
}
3009.1.1={
	holder=17062
}
3025.1.1={
	holder=16062
}
3045.1.1={
	holder=15062
}
3077.1.1={
	holder=14062
}
3091.1.1={
	holder=13062
}
3108.1.1={
	holder=12062
}
3145.1.1={
	holder=10062
}
3178.1.1={
	holder=9062
}
3192.1.1={
	holder=8062
}
3222.1.1={
	holder=7062
}
3249.1.1={
	holder=5062
}
3281.1.1={
	holder=62
}

