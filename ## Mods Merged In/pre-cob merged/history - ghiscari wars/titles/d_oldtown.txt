1.1.1={ law=succ_primogeniture law=cognatic_succession
	liege="k_oldtownTK"
}
25.1.1={holder=6000285} # Uthor of the High tower (C)
94.1.1={holder=6100285} # Urrigon (C)
120.1.1={holder=6200285} # Otho I (C)
140.1.1={holder=6300285} # Uthor II
175.1.1={holder=6400285} # Otho II (C)
195.1.1={holder=0}
2670.1.1={holder=6700285} # Lymond the Sea Lion # Last king of the Oldtown (C)
2700.1.1={liege="e_reach"}
2723.1.1={holder=0}

2872.1.1={holder=6900285} # Jeremy (C)
2917.1.1={holder=61000285} # Jason (C)
2937.1.1={holder=61100285} # Jared
2967.1.1={holder=61200285} # Jarett
3012.1.1={holder=61300285} # Lymond II
3062.1.1={holder=61400285} # John
3077.1.1={holder=61500285} # Otto
3107.1.1={holder=61600285} # Gareth
3127.1.1={holder=611700285} # Dorian (C)
3160.1.1={holder=61800285} # Ormond 
3172.1.1={holder=61900285} # Damon the Devout (C) # First to convert to the seven
3187.1.1={holder=62000285} # Triston (C)
3237.1.1={holder=62100285} # Barris (C)
