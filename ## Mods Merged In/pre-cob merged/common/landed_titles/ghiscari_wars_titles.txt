k_hyrkoon = {
	color={ 160 91 50 }
	color2={ 0 64 0 }
	
	dignity=10
	culture = hyrkooni
	
	capital = 844 #Hyrkoon
	
	allow = { hidden_tooltip = { NOT = { culture_group = winter_group } } }

	d_hyrkoon = {
		color={ 244 172 74 }
		color2={ 244 172 74 }
		
		capital = 844 #Hyrkoon
		culture = hyrkoon
		
		allow = {
			OR = {				
				custom_tooltip = {
					text = TOOLTIPhl_creation_requirement
					hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
				}	
				has_game_rule = { name = hl_creation_requirement value = off }
			}	
		}

		c_hyrkoon = {
			color={ 162 132 114 }
			color2={ 244 172 74 }
			
			b_hyrkoon = {
			}	
			b_hyrkoon_city = {
			}		
			b_hyrkoon_temple = {
			}			
		}

		c_talina = {
			color={ 207 135 92 }
			color2={ 244 172 74 }
		
			b_talina = {
			}	
			b_talina_city = {
			}		
			b_talina_temple = {
			}			
		}

		c_basalam = {
			color={ 102 91 84 }
			color2={ 244 172 74 }
		
			b_basalam = {
			}	
			b_basalam_city = {
			}		
			b_basalam_temple = {
			}			
		}
	}
}	
c_great_sand_road = {
	color={ 207 135 92 }
	color2={ 244 172 74 }
	
	allow = {
		always = no
	}

	b_great_sand_road = {
	}	
}