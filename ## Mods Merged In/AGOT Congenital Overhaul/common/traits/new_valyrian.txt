#NEW Valyrian Swords
ashbringer = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
avarice = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
avenger = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
calibur = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
dusk = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
dustcloud = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
emancipate = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
fornicater = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
harbinger = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
misers_folly = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
red_menace = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
stormblade = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
unbound = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
vainglory = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
vendetta = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
whiplash = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}
white_menace = {
	monthly_character_prestige = 0.25
	martial = 1
	combat_rating = 20
	vassal_opinion = 5
	cached = yes # Keep a cache of all trait holders, to use with the corresponding event trigger 
	customizer = yes
}