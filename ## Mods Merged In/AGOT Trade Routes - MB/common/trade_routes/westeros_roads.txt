kingsroad_route = {
	wealth = 200			# Total base wealth of silk route

	modifier = {			# How the trade route modifies the provinces it passes
		castle_tax_modifier = 0.05
		city_tax_modifier = 0.05
		temple_tax_modifier = 0.05
		trade_route_value = 1
		icon = 4
	}
	
	trade_post_display_trigger = {
		kingsroad_route_post_trigger = yes
	}
	
	start = {				# Start provinces of trade route
		324 # Weeping Town
	}
	
	path = { #King's Road
		324 322 320 314 303 304			#WT > Storm's End
		299 296 232 1203 226			#SE > King's Landing
		224 209 205 116 107 97			#KL > Lord Harroway's Town
		108 84 83 78 70 66 61 57 49 47	#LHT > Winterfell
	}
	
	path = { #Winterfell to Wall
		47 37 356 24 20 19 17
	}
	
	path = {	#Weeping Town - L. Tyrosh
		324 925 924 366
	}
}

goldroad_route = {
	wealth = 100			# Total base wealth of silk route
	
	modifier = {			# How the trade route modifies the provinces it passes
		castle_tax_modifier = 0.05
		city_tax_modifier = 0.05
		temple_tax_modifier = 0.05
		trade_route_value = 1
		icon = 4
	}
	
	trade_post_display_trigger = {
		goldroad_route_post_trigger = yes
	}
	
	start = {				# Start provinces of trade route
		182 # Casterly Rock
	}
	
	path = { #Gold Road
		182 184 1132 191 192 194 202 222 1203 226
	}
	
	path = {	#King's Landing -  Dragonstone - Pentos
		226 932 931 930 220 221 933 968 967 406
	}
}

river_road_route = {
	wealth = 50			# Total base wealth of silk route
	
	modifier = {			# How the trade route modifies the provinces it passes
		castle_tax_modifier = 0.05
		city_tax_modifier = 0.05
		temple_tax_modifier = 0.05
		trade_route_value = 1
		icon = 4
	}
	
	trade_post_display_trigger = {
		river_road_route_post_trigger = yes
	}
	
	start = {				# Start provinces of trade route
		182 # Casterly Rock
	}
	
	path = { #River Road
		182 178 176 174 175 102 101 91 96 97 
	}
	
	path = {	#Harroway's Town - Gulltown
		97 936 935 149
	}
	
	#path = {	# Branstone > Stoney Sept
	#	102 110 112
	#	193 194 	# >Payne Hall
	#}
}

high_road_route = {
	wealth = 75			# Total base wealth of silk route
	
	modifier = {			# How the trade route modifies the provinces it passes
		castle_tax_modifier = 0.05
		city_tax_modifier = 0.05
		temple_tax_modifier = 0.05
		trade_route_value = 1
		icon = 4
	}
	
	trade_post_display_trigger = {
		high_road_route_post_trigger = yes
	}
	
	start = {				# Start provinces of trade route
		97 # Lord Harroway's Town
	}
	
	path = {	#High Road to Eyrie
		97 95 134 140 1201 136
	}
	
	path = {	#The Eyrie - Gulltown - Braavos
		136 1201 125 142 1196 149 937 971
	}
}

roseroad_route = {
	wealth = 75			# Total base wealth of silk route
	
	modifier = {			# How the trade route modifies the provinces it passes
		castle_tax_modifier = 0.05
		city_tax_modifier = 0.05
		temple_tax_modifier = 0.05
		trade_route_value = 1
		icon = 4
	}
	
	trade_post_display_trigger = {
		roseroad_route_post_trigger = yes
	}
	
	start = {				# Start provinces of trade route
		286 # Oldtown
	}
	
	path = { 	#Rose Road
		286 1202 283 284 275 276 267 	#OT > Highgarden
		256 251 253 255 227			#HG > Dalston's Keep
		295 1203 226 211 225 212	#Dalston's Keep > King's Landing > Duskendale
	}
	
	#path = { 	#HG > Chyttering Brook
		#228 229 232 
	#}
	
	path = {	#Oldtown - W. Sound
		286 895
	}
	
	path = {	# OT > The Ring
		286 1202 287 291
	}
}

oceanroad_route = {
	wealth = 25			# Total base wealth of silk route
	
	modifier = {			# How the trade route modifies the provinces it passes
		castle_tax_modifier = 0.05
		city_tax_modifier = 0.05
		temple_tax_modifier = 0.05
		trade_route_value = 1
		icon = 4
	}
	
	trade_post_display_trigger = {
		oceanroad_route_post_trigger = yes
	}
	
	start = {				# Start provinces of trade route
		267 # Highgarden
	}
	
	path = { #Ocean Road
		267 265 240 235 197 185 183 182
	}
}

reach_coast_route = {
	wealth = 25			# Total base wealth of silk route
	
	modifier = {			# How the trade route modifies the provinces it passes
		castle_tax_modifier = 0.05
		city_tax_modifier = 0.05
		temple_tax_modifier = 0.05
		trade_route_value = 1
		icon = 4
	}
	
	trade_post_display_trigger = {
		reach_coast_route_post_trigger = yes
	}
	
	start = {				# Start provinces of trade route
		292 # The Arbor
	}
	
	path = {	#Arbor - Lannisport
		292 893 895 896 897 262 899 900
	}
	
	path = {	#Lannisport - Lordsport - Seagard
		900 903 167 904 161 912 913 79
		81 82 92 97	#Seagard - LHT
	}
	
	path = {	#The Arbor - Dorne Coast
		292 892 889 922
	}
	
	path = {	#Coast > Lannisport
		900 183
	}
	
	#path = {	#Shield Isles > Middlebury
	#	262 264 261 271 276
	#}	
}

dorneroad_route = {
	wealth = 75			# Total base wealth of silk route
	
	modifier = {			# How the trade route modifies the provinces it passes
		castle_tax_modifier = 0.05
		city_tax_modifier = 0.05
		temple_tax_modifier = 0.05
		trade_route_value = 1
		icon = 4
	}
	
	trade_post_display_trigger = {
		dorneroad_route_post_trigger = yes
	}
	
	start = {				# Start provinces of trade route
		350 # Plankytown
	}
	
	path = { 	#Dorne Road to Mistwood
		350 349 344 334 330 311 315 316 318 322
	}
	
	#path = {	#Blackhaven > Summerhall
	#	311 312 306
	#}
	
	path = {	#PT > Lys
		350 922 1003 368
	}
}
princespass_route = {
	wealth = 50			# Total base wealth of silk route
	
	modifier = {			# How the trade route modifies the provinces it passes
		castle_tax_modifier = 0.05
		city_tax_modifier = 0.05
		temple_tax_modifier = 0.05
		trade_route_value = 1
		icon = 4
	}
	
	trade_post_display_trigger = {
		princespass_route_post_trigger = yes
	}
	
	start = {				# Start provinces of trade route
		335 # Starfall
		349 # Godsgrace
	}
	
	path = {	#Starfall > Middlebury
		335 332 333 331 328 307 280 276
	}
	
	path = {	#Middlebury > Shield Isles
		276 271 261 264 262
	}
	
	path = { 	#Godsgrace > Skyreach
		349 345 339 338 333
	}
	
	#path = {	#PP - Yronwood
	#	328 331 334
	#}
	
	path = {	#Starfall - Torrentine Gulf
		335 892
	}
	
	#path = {	# Nightsong > Blackhaven
	#	307 279 270 306 259 227
	#}
}

narrowsea_route = {
	wealth = 25			# Total base wealth of silk route

	modifier = {			# How the trade route modifies the provinces it passes
		castle_tax_modifier = 0.025
		city_tax_modifier = 0.025
		temple_tax_modifier = 0.025
		trade_route_value = 0.5
		icon = 4
	}
	
	trade_post_display_trigger = {
		narrowsea_route_post_trigger = yes
	}

	start = {				# Start provinces of trade route
		368 #Lys
		378 #Myr
		470 #Braavos
	}
	
	path = {	#Myr - Braavos
		378 1001 965 966 968
		969 970 971
	}
	
	path = {	#Lys - Tyrosh
		368 1003 359 923 358 366 367
	}
	
	path = {	#Tyrosh - Myr
		367 964 965
	}
	
	path = {	#Braavos - Barrowton
		470 971 972 945 878 942 1131 119 943 944 62 61 45 46
	}
}