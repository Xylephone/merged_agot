# 149 - Gulltown

# County Title
title = c_gulltown

# Settlements
max_settlements = 7
b_grafton_keep = castle
b_gull_tower = castle
b_gulltown = city
b_gulltown_sept = temple
b_valeresk = city
b_titans_town = castle
#b_swallowcliffe = city

# Misc
culture = old_first_man
religion = old_gods

#History

1.1.1 = {
	b_grafton_keep = ca_asoiaf_vale_basevalue_1
	b_grafton_keep = ca_asoiaf_vale_basevalue_2
	b_grafton_keep = ca_asoiaf_vale_basevalue_3
	b_grafton_keep = ca_asoiaf_vale_basevalue_4
	
	b_gull_tower = ca_asoiaf_vale_basevalue_1
	
	b_gulltown = ct_asoiaf_vale_basevalue_1
	b_gulltown = ct_asoiaf_vale_basevalue_2
	b_gulltown = ct_asoiaf_vale_basevalue_3
	b_gulltown = ct_asoiaf_vale_basevalue_4
	b_gulltown = ct_asoiaf_vale_basevalue_5
	b_gulltown = ct_asoiaf_vale_basevalue_6
	b_gulltown = ct_asoiaf_smallshipyard
	
	b_valeresk = ct_asoiaf_vale_basevalue_1
	b_valeresk = ct_asoiaf_vale_basevalue_2
	b_valeresk = ct_asoiaf_vale_basevalue_3
	b_valeresk = ct_asoiaf_vale_basevalue_4
	
	b_titans_town = ca_asoiaf_vale_basevalue_1
	b_titans_town = ca_asoiaf_vale_basevalue_2
}
6700.1.1 = {
	culture = valeman
	religion = the_seven
}
