# 1158 - Grey Garden

# County Title
title = c_grey_garden

# Settlements
max_settlements = 1

b_grey_garden_mbs = castle


# Misc
culture = old_ironborn
religion = drowned_god

#History


1.1.1 = {
	b_grey_garden_mbs = ca_asoiaf_ironislands_basevalue_1
	b_grey_garden_mbs = ca_asoiaf_ironislands_basevalue_2
}


6700.1.1 = {
	culture = ironborn
}

