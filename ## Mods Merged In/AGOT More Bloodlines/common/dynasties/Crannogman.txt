# crannogman
#non-canon
1453 = {
	name="Wett"
	culture = crannogman
}
1454 = {
	name="Slough"
	culture = crannogman
}
1455 = {
	name="Brownmyre"
	culture = crannogman
}
1456 = {
	name="Longreed"
	culture = crannogman
}
1457 = {
	name="Greenmyre"
	culture = crannogman
}
1458 = {
	name="Downpeat"
	culture = crannogman
}
1459 = {
	name="Neckman"
	culture = crannogman
}
1460 = {
	name="Mudder"
	culture = crannogman
}
1461 = {
	name="Frogg"
	culture = crannogman
}
1462 = {
	name="Marsh"
	culture = crannogman
}
1463 = {
	name="Thatcher"
	culture = crannogman
}
800900 = {
	name = Bogwell
	culture = crannogman
}
800901 = {
	name = Grue
	culture = crannogman
}
800902 = {
	name = Willot
	culture = crannogman
}
800903 = {
	name = Coster
	culture = crannogman
}
800904 = {
	name = Hayle
	culture = crannogman
}
800905 = {
	name = Cavett
	culture = crannogman
}
800906 = {
	name = Mossby
	culture = crannogman
}
800907 = {
	name = Frogpool
	culture = crannogman
}
800908 = {
	name = Gloombough
	culture = crannogman
}
800909 = {
	name = Hycks
	culture = crannogman
}
800910 = {
	name = Blackpeat
	culture = crannogman
}
800911 = {
	name = Fogg
	culture = crannogman
}
800912 = {
	name = Creepfoote
	culture = crannogman
}
800913 = {
	name = Frogby
	culture = crannogman
}
800914 = {
	name = Peatby
	culture = crannogman
}
800915 = {
	name = Mudfoote
	culture = crannogman
}
800916 = {
	name = Travett
	culture = crannogman
}
800917 = {
	name = Dankreeds
	culture = crannogman
}
800918 = {
	name = Fennels
	culture = crannogman
}
800919 = {
	name = Mossmyre
	culture = crannogman
}
800920 = {
	name = Mossfoote
	culture = crannogman
}
### Canon ###
1452 = {
	name="Boggs"
	culture = crannogman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 76
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
72 = {
	name="Moss"
	culture = crannogman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 25
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
73 = {
	name="Fenn"
	culture = crannogman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 7
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
74 = {
	name="Blackmyre"
	culture = crannogman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 49
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
78 = {
	name="Marsh"
	culture = crannogman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 22
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
126 = {
	name="Reed"
	culture = crannogman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 29
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1447 = {
	name="Tamm"
	culture = crannogman
}
1448 = {
	name="Cray"
	culture = crannogman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 50
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1449 = {
	name="Greengood"
	culture = crannogman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 51
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1450 = {
	name="Peat"
	culture = crannogman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 52
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1451 = {
	name="Quagg"
	culture = crannogman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 53
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
1066 = {
	name="Cailin"
	culture = crannogman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 29
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
