# 1182  - Caotang

# County Title
title = c_caotang

# Settlements
max_settlements = 4

b_caotang_castle = castle
b_caotang_city1 = city
b_caotang_temple = temple
b_caotang_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_caotang_castle = ca_asoiaf_yiti_basevalue_1
	b_caotang_castle = ca_asoiaf_yiti_basevalue_2

	b_caotang_city1 = ct_asoiaf_yiti_basevalue_1
	b_caotang_city1 = ct_asoiaf_yiti_basevalue_2

}
	