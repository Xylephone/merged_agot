# 1187  - Linxin

# County Title
title = c_linxin

# Settlements
max_settlements = 4

b_linxin_castle = castle
b_linxin_city1 = city
b_linxin_temple = temple
b_linxin_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_linxin_castle = ca_asoiaf_yiti_basevalue_1
	b_linxin_castle = ca_asoiaf_yiti_basevalue_2

	b_linxin_city1 = ct_asoiaf_yiti_basevalue_1
	b_linxin_city1 = ct_asoiaf_yiti_basevalue_2

}
	