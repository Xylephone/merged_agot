# 1188  - Gongyi

# County Title
title = c_gongyi

# Settlements
max_settlements = 4

b_gongyi_castle = castle
b_gongyi_city1 = city
b_gongyi_temple = temple
b_gongyi_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_gongyi_castle = ca_asoiaf_yiti_basevalue_1
	b_gongyi_castle = ca_asoiaf_yiti_basevalue_2

	b_gongyi_city1 = ct_asoiaf_yiti_basevalue_1
	b_gongyi_city1 = ct_asoiaf_yiti_basevalue_2

}
	