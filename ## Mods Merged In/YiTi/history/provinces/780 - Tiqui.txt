# 780  - Tiqui

# County Title
title = c_tiqui

# Settlements
max_settlements = 7

b_changlu_castle = castle
b_tiqui_city1 = city
b_tiqui_temple = temple
b_tiqui_city2 = city
b_tiqui_castle1 = city
b_tiqui_castle2 = castle
b_tiqui_castle3 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_changlu_castle = ca_asoiaf_yiti_basevalue_1
	b_changlu_castle = ca_asoiaf_yiti_basevalue_2
	b_changlu_castle = ca_asoiaf_yiti_basevalue_3
	b_changlu_castle = ca_asoiaf_yiti_basevalue_4
	b_changlu_castle = ca_asoiaf_yiti_basevalue_5
	b_changlu_castle = ca_changlu_castle

	b_tiqui_city1 = ct_asoiaf_yiti_basevalue_1
	b_tiqui_city1 = ct_asoiaf_yiti_basevalue_2
	b_tiqui_city1 = ct_asoiaf_yiti_basevalue_3
	b_tiqui_city1 = ct_asoiaf_yiti_basevalue_4

	b_tiqui_city2 = ct_asoiaf_yiti_basevalue_1
	b_tiqui_city2 = ct_asoiaf_yiti_basevalue_2
	b_tiqui_city2 = ct_asoiaf_yiti_basevalue_3
	b_tiqui_city2 = ct_asoiaf_yiti_basevalue_4

	b_tiqui_castle1 = ct_asoiaf_yiti_basevalue_1

	b_tiqui_castle2 = ca_asoiaf_yiti_basevalue_1

	b_tiqui_castle3 = ct_asoiaf_yiti_basevalue_1

}
	