# 771  - Yanyun

# County Title
title = c_yanyun

# Settlements
max_settlements = 6

b_yanyun_castle1 = castle
b_yanyun_city1 = city
b_yanyun_temple = temple
b_yanyun_city2 = city
b_yanyun_castle2 = city
b_yanyun_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_yanyun_castle1 = ca_asoiaf_yiti_basevalue_1
	b_yanyun_castle1 = ca_asoiaf_yiti_basevalue_2
	b_yanyun_castle1 = ca_asoiaf_yiti_basevalue_3

	b_yanyun_city1 = ct_asoiaf_yiti_basevalue_1
	b_yanyun_city1 = ct_asoiaf_yiti_basevalue_2
	b_yanyun_city1 = ct_asoiaf_yiti_basevalue_3

	b_yanyun_city2 = ct_asoiaf_yiti_basevalue_1
	b_yanyun_city2 = ct_asoiaf_yiti_basevalue_2
	b_yanyun_city2 = ct_asoiaf_yiti_basevalue_3

}
	