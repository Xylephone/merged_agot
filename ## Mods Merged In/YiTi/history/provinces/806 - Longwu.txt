# 806  - Longwu

# County Title
title = c_longwu

# Settlements
max_settlements = 6

b_longwu_castle1 = castle
b_longwu_city1 = city
b_longwu_temple = temple
b_longwu_city2 = city
b_longwu_castle2 = city
b_longwu_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_longwu_castle1 = ca_asoiaf_yiti_basevalue_1
	b_longwu_castle1 = ca_asoiaf_yiti_basevalue_2
	b_longwu_castle1 = ca_asoiaf_yiti_basevalue_3

	b_longwu_city1 = ct_asoiaf_yiti_basevalue_1
	b_longwu_city1 = ct_asoiaf_yiti_basevalue_2
	b_longwu_city1 = ct_asoiaf_yiti_basevalue_3

	b_longwu_city2 = ct_asoiaf_yiti_basevalue_1
	b_longwu_city2 = ct_asoiaf_yiti_basevalue_2
	b_longwu_city2 = ct_asoiaf_yiti_basevalue_3

}
	