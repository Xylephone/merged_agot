# 1205  - Pandao

# County Title
title = c_pandao

# Settlements
max_settlements = 4

b_pandao_castle = castle
b_pandao_city1 = city
b_pandao_temple = temple
b_pandao_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_pandao_castle = ca_asoiaf_yiti_basevalue_1
	b_pandao_castle = ca_asoiaf_yiti_basevalue_2

	b_pandao_city1 = ct_asoiaf_yiti_basevalue_1
	b_pandao_city1 = ct_asoiaf_yiti_basevalue_2

}
	