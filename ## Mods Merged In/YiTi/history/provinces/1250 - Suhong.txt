# 1153  - Suhong

# County Title
title = c_suhong

# Settlements
max_settlements = 4

b_suhong_castle = castle
b_suhong_city1 = city
b_suhong_temple = temple
b_suhong_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_suhong_castle = ca_asoiaf_yiti_basevalue_1
	b_suhong_castle = ca_asoiaf_yiti_basevalue_2

	b_suhong_city1 = ct_asoiaf_yiti_basevalue_1
	b_suhong_city1 = ct_asoiaf_yiti_basevalue_2

}
	