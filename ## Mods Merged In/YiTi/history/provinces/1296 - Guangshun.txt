# 1199  - Guangshun

# County Title
title = c_guangshun

# Settlements
max_settlements = 4

b_guangshun_castle = castle
b_guangshun_city1 = city
b_guangshun_temple = temple
b_guangshun_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_guangshun_castle = ca_asoiaf_yiti_basevalue_1
	b_guangshun_castle = ca_asoiaf_yiti_basevalue_2

	b_guangshun_city1 = ct_asoiaf_yiti_basevalue_1
	b_guangshun_city1 = ct_asoiaf_yiti_basevalue_2

}
	