# 776  - Yulin

# County Title
title = c_yulin

# Settlements
max_settlements = 6

b_yulin_castle1 = castle
b_yulin_city1 = city
b_yulin_temple = temple
b_yulin_city2 = city
b_yulin_castle2 = city
b_yulin_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_yulin_castle1 = ca_asoiaf_yiti_basevalue_1
	b_yulin_castle1 = ca_asoiaf_yiti_basevalue_2
	b_yulin_castle1 = ca_asoiaf_yiti_basevalue_3

	b_yulin_city1 = ct_asoiaf_yiti_basevalue_1
	b_yulin_city1 = ct_asoiaf_yiti_basevalue_2
	b_yulin_city1 = ct_asoiaf_yiti_basevalue_3

	b_yulin_city2 = ct_asoiaf_yiti_basevalue_1
	b_yulin_city2 = ct_asoiaf_yiti_basevalue_2
	b_yulin_city2 = ct_asoiaf_yiti_basevalue_3

}
	