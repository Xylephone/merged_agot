# 1151  - Nianshui

# County Title
title = c_nianshui

# Settlements
max_settlements = 4

b_nianshui_castle = castle
b_nianshui_city1 = city
b_nianshui_temple = temple
b_nianshui_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_nianshui_castle = ca_asoiaf_yiti_basevalue_1
	b_nianshui_castle = ca_asoiaf_yiti_basevalue_2

	b_nianshui_city1 = ct_asoiaf_yiti_basevalue_1
	b_nianshui_city1 = ct_asoiaf_yiti_basevalue_2

}
	