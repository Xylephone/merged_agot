# 847  - Trader Town

# County Title
title = c_trader_town

# Settlements
max_settlements = 7

b_furong_castle = castle
b_trader_town_city1 = city
b_trader_town_temple = temple
b_trader_town_city2 = city
b_trader_town_castle1 = city
b_trader_town_castle2 = castle
b_trader_town_castle3 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_furong_castle = ca_asoiaf_yiti_basevalue_1
	b_furong_castle = ca_asoiaf_yiti_basevalue_2
	b_furong_castle = ca_asoiaf_yiti_basevalue_3
	b_furong_castle = ca_asoiaf_yiti_basevalue_4
	b_furong_castle = ca_furong_castle

	b_trader_town_city1 = ct_asoiaf_yiti_basevalue_1
	b_trader_town_city1 = ct_asoiaf_yiti_basevalue_2
	b_trader_town_city1 = ct_asoiaf_yiti_basevalue_3
	b_trader_town_city1 = ct_asoiaf_yiti_basevalue_4
	b_trader_town_city1 = ct_asoiaf_yiti_basevalue_5

	b_trader_town_city2 = ct_asoiaf_yiti_basevalue_1
	b_trader_town_city2 = ct_asoiaf_yiti_basevalue_2
	b_trader_town_city2 = ct_asoiaf_yiti_basevalue_3
	b_trader_town_city2 = ct_asoiaf_yiti_basevalue_4
	b_trader_town_city2 = ct_asoiaf_yiti_basevalue_5

	b_trader_town_castle1 = ct_asoiaf_yiti_basevalue_1

	b_trader_town_castle2 = ca_asoiaf_yiti_basevalue_1

	b_trader_town_castle3 = ct_asoiaf_yiti_basevalue_1

}
	