# 1222  - Yongning

# County Title
title = c_yongning

# Settlements
max_settlements = 4

b_yongning_castle = castle
b_yongning_city1 = city
b_yongning_temple = temple
b_yongning_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_yongning_castle = ca_asoiaf_yiti_basevalue_1
	b_yongning_castle = ca_asoiaf_yiti_basevalue_2

	b_yongning_city1 = ct_asoiaf_yiti_basevalue_1
	b_yongning_city1 = ct_asoiaf_yiti_basevalue_2

}
	