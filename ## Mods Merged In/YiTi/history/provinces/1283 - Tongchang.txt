# 1186  - Tongchang

# County Title
title = c_tongchang

# Settlements
max_settlements = 4

b_tongchang_castle = castle
b_tongchang_city1 = city
b_tongchang_temple = temple
b_tongchang_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_tongchang_castle = ca_asoiaf_yiti_basevalue_1
	b_tongchang_castle = ca_asoiaf_yiti_basevalue_2

	b_tongchang_city1 = ct_asoiaf_yiti_basevalue_1
	b_tongchang_city1 = ct_asoiaf_yiti_basevalue_2

}
	