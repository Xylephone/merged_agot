# 1173  - Shijun

# County Title
title = c_shijun

# Settlements
max_settlements = 4

b_shijun_castle = castle
b_shijun_city1 = city
b_shijun_temple = temple
b_shijun_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_shijun_castle = ca_asoiaf_yiti_basevalue_1
	b_shijun_castle = ca_asoiaf_yiti_basevalue_2

	b_shijun_city1 = ct_asoiaf_yiti_basevalue_1

}
	