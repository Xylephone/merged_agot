# 1176  - Tenghua

# County Title
title = c_tenghua

# Settlements
max_settlements = 4

b_tenghua_castle = castle
b_tenghua_city1 = city
b_tenghua_temple = temple
b_tenghua_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_tenghua_castle = ca_asoiaf_yiti_basevalue_1
	b_tenghua_castle = ca_asoiaf_yiti_basevalue_2

	b_tenghua_city1 = ct_asoiaf_yiti_basevalue_1
	b_tenghua_city1 = ct_asoiaf_yiti_basevalue_2

}
	