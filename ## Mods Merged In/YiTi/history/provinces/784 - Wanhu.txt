# 784  - Wanhu

# County Title
title = c_wanhu

# Settlements
max_settlements = 6

b_wanhu_castle1 = castle
b_wanhu_city1 = city
b_wanhu_temple = temple
b_wanhu_city2 = city
b_wanhu_castle2 = city
b_wanhu_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_wanhu_castle1 = ca_asoiaf_yiti_basevalue_1
	b_wanhu_castle1 = ca_asoiaf_yiti_basevalue_2
	b_wanhu_castle1 = ca_asoiaf_yiti_basevalue_3

	b_wanhu_city1 = ct_asoiaf_yiti_basevalue_1
	b_wanhu_city1 = ct_asoiaf_yiti_basevalue_2
	b_wanhu_city1 = ct_asoiaf_yiti_basevalue_3

	b_wanhu_city2 = ct_asoiaf_yiti_basevalue_1
	b_wanhu_city2 = ct_asoiaf_yiti_basevalue_2
	b_wanhu_city2 = ct_asoiaf_yiti_basevalue_3

}
	