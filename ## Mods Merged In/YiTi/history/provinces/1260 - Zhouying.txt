# 1163  - Zhouying

# County Title
title = c_zhouying

# Settlements
max_settlements = 4

b_zhouying_castle = castle
b_zhouying_city1 = city
b_zhouying_temple = temple
b_zhouying_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_zhouying_castle = ca_asoiaf_yiti_basevalue_1
	b_zhouying_castle = ca_asoiaf_yiti_basevalue_2

	b_zhouying_city1 = ct_asoiaf_yiti_basevalue_1

}
	