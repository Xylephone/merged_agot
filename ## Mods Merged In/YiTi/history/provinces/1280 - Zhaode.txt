# 1183  - Zhaode

# County Title
title = c_zhaode

# Settlements
max_settlements = 4

b_zhaode_castle = castle
b_zhaode_city1 = city
b_zhaode_temple = temple
b_zhaode_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_zhaode_castle = ca_asoiaf_yiti_basevalue_1
	b_zhaode_castle = ca_asoiaf_yiti_basevalue_2

	b_zhaode_city1 = ct_asoiaf_yiti_basevalue_1
	b_zhaode_city1 = ct_asoiaf_yiti_basevalue_2

}
	