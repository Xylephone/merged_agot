1.1.1={
	liege="d_orkmont"
	de_jure_liege = d_orkmont
	law = agnatic_succession
	law = succ_primogeniture
}
2917.1.1 = { holder = 500174420 } #Urras  Greyiron
2942.1.1 = { holder = 501174420 } #Erich I Greyiron
2943.1.1 = { holder = 0 } 
2956.1.1 = { holder = 502174420 } #Theon I Greyiron (NC)
2993.1.1 = { holder = 354066 } #Sylas  
3009.1.1 = { holder = 503174420 } #Balon I Greyiron (NC)
3034.1.1 = { holder = 0 }

3487.1.1 = { holder = 355066 } #Harrag  Hoare
3519.1.1 = { holder = 357066 } #Erich II Hoare
3545.1.1 = { holder = 504174420 } #Urrathon I Greyiron (NC)
3556.1.1 = { holder = 505174420 } #Urrathon II Greyiron (NC)
3578.1.1 = { holder = 0 } 
3638.1.1 = { holder = 506174420 } #Urragon I Greyiron (NC)
3658.1.1 = { holder = 0 } 
3670.1.1 = { holder = 358066 } #Qhored  Hoare
3745.1.1 = { holder = 0 }

4159.1.1 = { holder = 507174420 } #Balon III Greyiron (NC)
4177.1.1 = { holder = 508174420 } #Erich III Greyiron (NC)
4216.1.1 = { holder = 509174420 } #Rognar I Greyiron (NC)
4230.1.1 = { holder = 0 } 
4238.1.1 = { holder = 510174420 } #Erich IV Greyiron (NC)
4278.1.1 = { holder = 511174420 } #Urrathon III Greyiron (NC)
4287.1.1 = { holder = 0 } 

5014.1.1 = { holder = 524174420 } #Urragon II Greyiron (NC)
5043.1.1 = { holder = 512174420 } #Urragon III Greyiron
5087.1.1 = { holder = 0 } 
5089.1.1 = { holder = 513174420 } #Torgon  Greyiron
5129.1.1 = { holder = 517174420 } #Urragon IV Greyiron

5181.1.1 = { 
	holder = 520174420  #Urron  Greyiron
	law = succ_primogeniture
} 
5203.1.1 = { holder = 0 }

6490.1.1 = { holder = 525174420 } #Joron II Greyiron (NC)
6521.1.1 = { holder = 521174420 } #Euron I Greyiron (NC)
6540.1.1 = { holder = 522174420 } #Balon VII Greyiron (NC)
6563.1.1 = { holder = 523174420 } #Rognar II Greyiron

6584.1.1 = { holder = 359066 } #Harras  Hoare
6605.1.1 = { holder = 360066 } #Wulfgar  Hoare
6639.1.1 = { holder = 361066 } #Harrag Hoare (NC)
6648.1.1 = { holder = 362066 } #Euron II Hoare (NC)
6671.1.1 = { holder = 363066 } #Horgan  Hoare
6692.1.1 = { holder = 3163066 } #Balon VIII Hoare (NC)

6734.1.1 = { holder = 0 }

7458.1.1 = { holder = 364066 } #Fergon  Hoare
7470.1.1 = { holder = 365066 } #Othgar I Hoare
7518.1.1 = { holder = 366066 } #Othgar II Hoare
7539.1.1 = { holder = 367066 } #Craghorn  Hoare
7547.1.1 = { holder = 368066 } #Harmund I Hoare
7598.1.1 = { holder = 369066 } #Harmund II Hoare
7612.1.1 = { holder = 370066 } #Harmund III Hoare
7615.1.1 = { holder = 371066 } #Hagon  Hoare
7622.1.1 = { holder = 0 }

7622.1.1 = { liege="d_orkwood" }

7971.1.1 = { holder=100116 } # Urzen (nc)
8013.1.1 = { holder=101116 } # Rolfe (nc)
8024.1.1 = { holder=103116 } # Sigfryd (nc)
8061.1.1 = { holder=111116 } # Burton (nc)
8090.1.1 = { holder=114116 } # Drennan (nc)
8148.1.1 = { holder=121116 } # Dagon (nc)
8156.1.1 = { holder=123116 } # Harrag (nc)
8161.1.1 = { holder=124116 } # Rolfe (nc)
8180.1.1 = { holder=128116 } # Werlag (nc)
8209.1.1 = { holder=129116 } # Aeron (nc)
8214.1.1 = { holder=135116 } # Lorren (nc)
8249.1.1 = { holder=138116 } # Urek (nc)
8285.1.1 = { holder=116 } # Alyn