# 1145 - Sweetport Sound

# County Title
title = c_sweetport_sound

# Settlements
max_settlements = 3

b_sweetport_sound = castle
b_rambton = castle

# Misc
culture = old_first_man
religion = old_gods

# History

1066.1.1 = {
	b_sweetport_sound = ca_asoiaf_crown_basevalue_1
	b_sweetport_sound = ca_asoiaf_smallshipyard
	
	b_rambton = ca_asoiaf_crown_basevalue_1
}
6700.1.1 = { 
	culture = crownlander
	religion = the_seven
}