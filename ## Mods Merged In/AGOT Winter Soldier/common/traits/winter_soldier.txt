northern_winter_soldier = {
	potential = {
		AND ={
			is_adult = yes
			martial = 8
			NOR = {
				trait = white_walker
				culture_group = winter_group
			}
			capital_scope = {
				OR = {
					region = world_beyond_the_wall
					region = world_the_wall
					region = world_north
				}
			}
		}
	}
	leader = yes
	
	command_modifier = {
		winter_supply = 2
		winter_combat = 0.1
	}	
}

white_walker_winter_soldier = {
	potential = {
		trait = white_walker
	}
	leader = yes
	
	command_modifier = {
		winter_supply = 4
		winter_combat = 0.2
	}
}