559101 = {
	name="Shieldbreaker"
	culture = wildling
}
559102 = {
	name="Boarhell"
	culture = wildling
}
559103 = {
	name="Warriormane"
	culture = wildling
}
559104 = {
	name="Lockblood"
	culture = wildling
}
559105 = {
	name="Warghunt"
	culture = wildling
}
559106 = {
	name="Bluerage"
	culture = wildling
}
559107 = {
	name="Axebar"
	culture = wildling
}
559108 = {
	name="Torgrand"
	culture = wildling
}
559109 = {
	name="Skullsmorn"
	culture = wildling
}
559110 = {
	name="Varawolf"
	culture = wildling
}
559111 = {
	name="Egelgrand"
	culture = wildling
}

559112 = {
	name="Thanskull"
	culture = wildling
}
559113 = {
	name="Manconn"
	culture = wildling
}
559114 = {
	name="Barthe"
	culture = wildling
}
559115 = {
	name="Marahorn"
	culture = wildling
}
559116 = {
	name="Granspear"
	culture = wildling
}
559117 = {
	name="Blackshield"
	culture = wildling
}
559118 = {
	name="Lordsbane"
	culture = wildling
}
559119 = {
	name="Drynboar"
	culture = wildling
}
559120 = {
	name="Sealskinner"
	culture = wildling
}
559121 = {
	name="Wanderer"
	culture = wildling
}
559122 = {
	name="Crowhunter"
	culture = wildling
}
559123 = {
	name="Crowkiller"
	culture = wildling
}
559124 = {
	name="Crowsbane"
	culture = wildling
}
559125 = {
	name="Icewarrior"
	culture = wildling
}
559126 = {
	name="White Mask"
	culture = wildling
}
559127 = {
	name="Bronzesword"
	culture = wildling
}
559128 = {
	name="Pikefrost"
	culture = wildling
}
559129 = {
	name="Bridgetamer"
	culture = wildling
}
559130 = {
	name="Kingsmember"
	culture = wildling
}
559131 = {
	name="Sygerrik"
	culture = wildling
}
174345 = {
	name="of Ruddy Hall"
	culture = wildling
}
174346 = {
	name="of Skirling Pass"
	culture = wildling
}
174347 = {
	name = "Rayder"
	culture = wildling
}
174348 = {
	name="Kingsblood"
	culture = wildling
}
174349 = {
	name="Mossfoot"
	culture = wildling
	}
174350 = {
	name="Whitewatch"
	culture = wildling
}
174351 = {
	name="Frostfang"
	culture = wildling
}
174352 = {
	name="Iceguard"
	culture = wildling
}
174353 = {
	name="Forester"
	culture = wildling
}
174402 = {
	name="Wooden Ear"
	culture = wildling
}
174403 = {
	name="Shivers"
	culture = wildling
}
174404 = {
	name="Frostbite"
	culture = wildling
}
174408 = {
	name="Blackantler"
	culture = wildling
}
4500016 = {
	name = "Palefist"
	culture = wildling
}
4500017 = {
	name = "Bloodfang"
	culture = wildling
}
559132 = {
	name = "Skullskinner"
	culture = wildling
}
559133 = {
	name = "Hallowbluff"
	culture = wildling
}
559134 = {
	name = "Whitehunt"
	culture = wildling
}
559135 = {
	name = "Bronzefrost"
	culture = wildling
}
559136 = {
	name = "Starwood"
	culture = wildling
}
559137 = {
	name = "Bluespear"
	culture = wildling
}
559138 = {
	name = "Warghorn"
	culture = wildling
}
559139 = {
	name = "Heartwhisper"
	culture = wildling
}
559140 = {
	name = "Burninghead"
	culture = wildling
}
559141 = {
	name = "Hardhunter"
	culture = wildling
}
559142 = {
	name = "Lockblade"
	culture = wildling
}
559143 = {
	name = "Stonemorn"
	culture = wildling
}
559144 = {
	name = "Greensky"
	culture = wildling
}
559145 = {
	name = "Boarshaper"
	culture = wildling
}
559146 = {
	name = "Bridgeshield"
	culture = wildling
}
559147 = {
	name = "Skullarm"
	culture = wildling
}
559148 = {
	name = "Sealcut"
	culture = wildling
}
559149 = {
	name = "Crowbane"
	culture = wildling
}
559150 = {
	name = "Freeskull"
	culture = wildling
}
559151 = {
	name = "Pikeshot"
	culture = wildling
}
559152 = {
	name = "Egelbone"
	culture = wildling
}
559153 = {
	name = "Shieldblood"
	culture = wildling
}
559154 = {
	name = "Varabane"
	culture = wildling
}
559155 = {
	name = "Sternhell"
	culture = wildling
}
559156 = {
	name = "Spearwarrior"
	culture = wildling
}
559157 = {
	name = "Skywillow"
	culture = wildling
}
559158 = {
	name = "Manwatcher"
	culture = wildling
}
559159 = {
	name = "Redtree"
	culture = wildling
}
559160 = {
	name = "Bloodmember"
	culture = wildling
}
559161 = {
	name = "Blackwolf"
	culture = wildling
}
559162 = {
	name = "Weirbar"
	culture = wildling
}
559163 = {
	name = "Wolfkiller"
	culture = wildling
}
559164 = {
	name = "Demonface"
	culture = wildling
}
559165 = {
	name = "Thanrage"
	culture = wildling
}
559166 = {
	name = "Songmask"
	culture = wildling
}
559167 = {
	name = "Torbow"
	culture = wildling
}
559168 = {
	name = "Godsword"
	culture = wildling
}
559169 = {
	name = "Woodsmane"
	culture = wildling
}
559170 = {
	name = "Goreshield"
	culture = wildling
}
559171 = {
	name = "Drydemon"
	culture = wildling
}
559172 = {
	name = "Warriorkin"
	culture = wildling
}
559173 = {
	name = "Woodskinner"
	culture = wildling
}
559174 = {
	name = "Songwarrior"
	culture = wildling
}
559175 = {
	name = "Burningboar"
	culture = wildling
}
559176 = {
	name = "Wolfmember"
	culture = wildling
}
559177 = {
	name = "Starwhisper"
	culture = wildling
}
559178 = {
	name = "Shadeface"
	culture = wildling
}
559179 = {
	name = "Sternsun"
	culture = wildling
}
559180 = {
	name = "Oldhorn"
	culture = wildling
}
559181 = {
	name = "Greenchief"
	culture = wildling
}
559182 = {
	name = "Bridgeshaper"
	culture = wildling
}
559183 = {
	name = "Redbone"
	culture = wildling
}
559184 = {
	name = "Sealtamer"
	culture = wildling
}
559185 = {
	name = "Pikehead"
	culture = wildling
}
559186 = {
	name = "Demonfrost"
	culture = wildling
}
559187 = {
	name = "Whitekiller"
	culture = wildling
}
559188 = {
	name = "Axehunt"
	culture = wildling
}
559189 = {
	name = "Bloodconn"
	culture = wildling
}
559190 = {
	name = "Skytree"
	culture = wildling
}
559191 = {
	name = "Boarhunter"
	culture = wildling
}
559192 = {
	name = "Icerage"
	culture = wildling
}
559193 = {
	name = "Heartcut"
	culture = wildling
}
559194 = {
	name = "Blackgaze"
	culture = wildling
}
559195 = {
	name = "Marawatcher"
	culture = wildling
}
559196 = {
	name = "Godserpent"
	culture = wildling
}
559197 = {
	name = "Kingsblood"
	culture = wildling
}
559198 = {
	name = "Hallowclaw"
	culture = wildling
}
559199 = {
	name = "Weirspear"
	culture = wildling
}
559200 = {
	name = "Bluebluff"
	culture = wildling
}
559201 = {
	name = "Lockmask"
	culture = wildling
}
559202 = {
	name = "Warriordemon"
	culture = wildling
}
559203 = {
	name = "Flintwood"
	culture = wildling
}
559204 = {
	name = "Egelskull"
	culture = wildling
}
559205 = {
	name = "Drynmane"
	culture = wildling
}
559206 = {
	name = "Shieldarm"
	culture = wildling
}
559207 = {
	name = "Torbow"
	culture = wildling
}
559208 = {
	name = "Highwolf"
	culture = wildling
}
559209 = {
	name = "Hardsheep"
	culture = wildling
}
559210 = {
	name = "Thannkin"
	culture = wildling
}
559211 = {
	name = "Bearhell"
	culture = wildling
}
559212 = {
	name = "Bronzegrand"
	culture = wildling
}
559213 = {
	name = "Mansword"
	culture = wildling
}
559214 = {
	name = "Crowguard"
	culture = wildling
}
559215 = {
	name = "Skullbane"
	culture = wildling
}
559216 = {
	name = "Spearbane"
	culture = wildling
}
559217 = {
	name = "Freemorn"
	culture = wildling
}
559218 = {
	name = "Granblade"
	culture = wildling
}
559219 = {
	name = "Nightstar"
	culture = wildling
}
559220 = {
	name = "Varashot"
	culture = wildling
}
559221 = {
	name = "Stonepeak"
	culture = wildling
}
559223 = {
	name = Hardblade
	culture = wildling
}
559224 = {
	name = Tallseeker
	culture = wildling
}
559225 = {
	name = Hellbelly
	culture = wildling
}
559226 = {
	name = Icebane
	culture = wildling
}
559227 = {
	name = Moonmourn
	culture = wildling
}
559228 = {
	name = Bluebeard
	culture = wildling
}
559229 = {
	name = Icecrest
	culture = wildling
}
559230 = {
	name = Steelwood
	culture = wildling
}
559231 = {
	name = Fasthand
	culture = wildling
}
559232 = {
	name = Amberscar
	culture = wildling
}
559233 = {
	name = Nightshield
	culture = wildling
}
559234 = {
	name = Threespark
	culture = wildling
}
559235 = {
	name = Stoneflame
	culture = wildling
}
559236 = {
	name = Fernwalker
	culture = wildling
}
559237 = {
	name = Steelcreek
	culture = wildling
}
559238 = {
	name = Iceaxe
	culture = wildling
}
559239 = {
	name = Yellowhair
	culture = wildling
}
559240 = {
	name = "of Craster's Keep"
	culture = wildling
	used_for_random = no
	can_appear = no
}
559241 = { #for Dirk
	name = "of the Keep"
	culture = wildling
	used_for_random = no
	can_appear = no
}
### Giants
6660010 = {
	name = "Mar Tun Doh Weg"
	culture = giant
}
6660011 = {
	name = "Weg Wun Dar Wun"
	culture = giant
}
6660012 = {
	name = "Doh War Wun Tug"
	culture = giant
}
6660013 = {
	name = "Mar Weg Doh Wun"
	culture = giant
}
6660014 = {
	name = "Doh Wun Dar Weg"
	culture = giant
}
6660015 = {
	name = "Weg Mar Bun Doh"
	culture = giant
}

###  Thenn catch-all

6660016 = {
	name = "of Thenn"
	culture = thenn
}
#for historical thenns
174344 = {
	name="of Thenn"
	culture = thenn
	used_for_random = no
	can_appear = no
}
1174344 = {
	name="Thenn"
	culture = thenn
	used_for_random = no
	can_appear = no
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 47
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}

## Frozen Shore
559250 = {
	name="Ibarg"
	culture = frozen_shore
}
559251 = {
	name="Latag"
	culture = frozen_shore
}
559252 = {
	name="Frebgi"
	culture = frozen_shore
}
559253 = {
	name="Mubroch"
	culture = frozen_shore
}
559254 = {
	name="Slumnir"
	culture = frozen_shore
}
559255 = {
	name="Lacrion"
	culture = frozen_shore
}
559256 = {
	name="Mibgan"
	culture = frozen_shore
}
559257 = {
	name="Deglor"
	culture = frozen_shore
}
559258 = {
	name="Nokrus"
	culture = frozen_shore
}
559259 = {
	name="Sribzak"
	culture = frozen_shore
}
559260 = {
	name="Fuzlog"
	culture = frozen_shore
}
559261 = {
	name="Variok"
	culture = frozen_shore
}
559262 = {
	name="Drufum"
	culture = frozen_shore
}
559263 = {
	name="Trasak"
	culture = frozen_shore
}
559264 = {
	name="Jalog"
	culture = frozen_shore
}
559265 = {
	name="Redsog"
	culture = frozen_shore
}
559266 = {
	name="Oskus"
	culture = frozen_shore
}
559267 = {
	name="Lemgar"
	culture = frozen_shore
}
559268 = {
	name="Crewdyr"
	culture = frozen_shore
}
559269 = {
	name="Hukal"
	culture = frozen_shore
}
559270 = {
	name="Vaom"
	culture = frozen_shore
}
559271 = {
	name="Gladwor"
	culture = frozen_shore
}
559272 = {
	name="Ukor"
	culture = frozen_shore
}
559273 = {
	name="Tringi"
	culture = frozen_shore
}
559274 = {
	name="Varok"
	culture = frozen_shore
}

## Hornfoot
559300 = {
	name="Hardsole"
	culture = hornfoot
}
559301 = {
	name="Hornfoot"
	culture = hornfoot
}
559302 = {
	name="Nightsbane"
	culture = hornfoot
}
559303 = {
	name="Hornsorrow"
	culture = hornfoot
}
559304 = {
	name="Redtoe"
	culture = hornfoot
}
559305 = {
	name="Largefoot"
	culture = hornfoot
}
559306 = {
	name="Greathike"
	culture = hornfoot
}
559307 = {
	name="Roamer"
	culture = hornfoot
}
559308 = {
	name="Wildclaw"
	culture = hornfoot
}
559309 = {
	name="Hornwalker"
	culture = hornfoot
}
559310 = {
	name="Farmane"
	culture = hornfoot
}
559311 = {
	name="Pinesong"
	culture = hornfoot
}
559312 = {
	name="Duskfire"
	culture = hornfoot
}
559313 = {
	name="Slatehorn"
	culture = hornfoot
}
559314 = {
	name="Shieldhorn"
	culture = hornfoot
}
559315 = {
	name="Silverfoot"
	culture = hornfoot
}
559316 = {
	name="Greatsole"
	culture = hornfoot
}
559317 = {
	name="Hornshield"
	culture = hornfoot
}
559318 = {
	name="Hardshaper"
	culture = hornfoot
}
559319 = {
	name="Crowhorn"
	culture = hornfoot
}
559320 = {
	name="Whitestalker"
	culture = hornfoot
}
559321 = {
	name="Threebringer"
	culture = hornfoot
}
559322 = {
	name="Bluehorn"
	culture = hornfoot
}
559323 = {
	name="Thunderfoot"
	culture = hornfoot
}
559324 = {
	name="Flintstrider"
	culture = hornfoot
}

## Cannibal Clan
559350 = {
	name = Icefist
	culture = cannibal_clan
}
559351 = {
	name = Keenbend
	culture = cannibal_clan
}
559352 = {
	name = Deadeater
	culture = cannibal_clan
}
559353 = {
	name = Fleshfeast
	culture = cannibal_clan
}
559354 = {
	name = Clanbreaker
	culture = cannibal_clan
}
559355 = {
	name = Redriver
	culture = cannibal_clan
}
559356 = {
	name = Winterschief
	culture = cannibal_clan
}
559357 = {
	name = Riverblood
	culture = cannibal_clan
}
559358 = {
	name = Icebrow
	culture = cannibal_clan
}
559359 = {
	name = Crowcleaver
	culture = cannibal_clan
}
559360 = {
	name = Bonegorger
	culture = cannibal_clan
}
559361 = {
	name = Runerun
	culture = cannibal_clan
}
559362 = {
	name = Strongtooth
	culture = cannibal_clan
}
559363 = {
	name = Cannibal
	culture = cannibal_clan
}
559364 = {
	name = Manhunter
	culture = cannibal_clan
}
559365 = {
	name = Iceblood
	culture = cannibal_clan
}
559366 = {
	name = Mournflayer
	culture = cannibal_clan
}
559367 = {
	name = Crowcloak
	culture = cannibal_clan
}
559368 = {
	name = Greensbane
	culture = cannibal_clan
}
559369 = {
	name = Battlesteel
	culture = cannibal_clan
}
559370 = {
	name = Lordeater
	culture = cannibal_clan
}
559371 = {
	name = Iceriver
	culture = cannibal_clan
}
559372 = {
	name = Drinkblood
	culture = cannibal_clan
}
559373 = {
	name = Bronzeforest
	culture = cannibal_clan
}
559374 = {
	name = Bones
	culture = cannibal_clan
}

#Nightrunners
560101 = {
	name="Shieldbreaker"
	culture = nightrunner
}
560102 = {
	name="Boarhell"
	culture = nightrunner
}
560103 = {
	name="Warriormane"
	culture = nightrunner
}
560104 = {
	name="Lockblood"
	culture = nightrunner
}
560105 = {
	name="Warghunt"
	culture = nightrunner
}
560106 = {
	name="Bluerage"
	culture = nightrunner
}
560107 = {
	name="Axebar"
	culture = nightrunner
}
560108 = {
	name="Torgrand"
	culture = nightrunner
}
560109 = {
	name="Skullsmorn"
	culture = nightrunner
}
560110 = {
	name="Varawolf"
	culture = nightrunner
}
560111 = {
	name="Egelgrand"
	culture = nightrunner
}

560112 = {
	name="Thanskull"
	culture = nightrunner
}
560113 = {
	name="Manconn"
	culture = nightrunner
}
560114 = {
	name="Barthe"
	culture = nightrunner
}
560115 = {
	name="Marahorn"
	culture = nightrunner
}
560116 = {
	name="Granspear"
	culture = nightrunner
}
560117 = {
	name="Blackshield"
	culture = nightrunner
}
560118 = {
	name="Lordsbane"
	culture = nightrunner
}
560119 = {
	name="Drynboar"
	culture = nightrunner
}
560120 = {
	name="Sealskinner"
	culture = nightrunner
}
560121 = {
	name="Wanderer"
	culture = nightrunner
}
560122 = {
	name="Crowhunter"
	culture = nightrunner
}
560123 = {
	name="Crowkiller"
	culture = nightrunner
}
560124 = {
	name="Crowsbane"
	culture = nightrunner
}
560125 = {
	name="Icewarrior"
	culture = nightrunner
}
560126 = {
	name="White Mask"
	culture = nightrunner
}
560127 = {
	name="Bronzesword"
	culture = nightrunner
}
560128 = {
	name="Pikefrost"
	culture = nightrunner
}
560129 = {
	name="Bridgetamer"
	culture = nightrunner
}
560130 = {
	name="Kingsmember"
	culture = nightrunner
}
560131 = {
	name="Sygerrik"
	culture = nightrunner
}
560345 = {
	name="Nightrunner"
	culture = nightrunner
}
560346 = {
	name="Nightblade"
	culture = nightrunner
}
560347 = {
	name = "Nightaxe"
	culture = nightrunner
}
560348 = {
	name="Longnight"
	culture = nightrunner
}
560349 = {
	name="Mossfoot"
	culture = nightrunner
	}
560350 = {
	name="Whitewatch"
	culture = nightrunner
}
560351 = {
	name="Frostfang"
	culture = nightrunner
}
560352 = {
	name="Iceguard"
	culture = nightrunner
}
560353 = {
	name="Forester"
	culture = nightrunner
}
560402 = {
	name="Wooden Ear"
	culture = nightrunner
}
560403 = {
	name="Shivers"
	culture = nightrunner
}
560404 = {
	name="Frostbite"
	culture = nightrunner
}
560408 = {
	name="Blackantler"
	culture = nightrunner
}
560132 = {
	name = "Skullskinner"
	culture = nightrunner
}
560133 = {
	name = "Hallowbluff"
	culture = nightrunner
}
560134 = {
	name = "Whitehunt"
	culture = nightrunner
}
560135 = {
	name = "Bronzefrost"
	culture = nightrunner
}
560136 = {
	name = "Starwood"
	culture = nightrunner
}
560137 = {
	name = "Bluespear"
	culture = nightrunner
}
560138 = {
	name = "Warghorn"
	culture = nightrunner
}
560139 = {
	name = "Heartwhisper"
	culture = nightrunner
}
560140 = {
	name = "Burninghead"
	culture = nightrunner
}
560141 = {
	name = "Hardhunter"
	culture = nightrunner
}
560142 = {
	name = "Lockblade"
	culture = nightrunner
}
560143 = {
	name = "Stonemorn"
	culture = nightrunner
}
560144 = {
	name = "Greensky"
	culture = nightrunner
}
560145 = {
	name = "Boarshaper"
	culture = nightrunner
}
560146 = {
	name = "Bridgeshield"
	culture = nightrunner
}
560147 = {
	name = "Skullarm"
	culture = nightrunner
}
560148 = {
	name = "Sealcut"
	culture = nightrunner
}
560149 = {
	name = "Crowbane"
	culture = nightrunner
}
560150 = {
	name = "Freeskull"
	culture = nightrunner
}
560151 = {
	name = "Pikeshot"
	culture = nightrunner
}
560152 = {
	name = "Egelbone"
	culture = nightrunner
}
560153 = {
	name = "Shieldblood"
	culture = nightrunner
}
560154 = {
	name = "Varabane"
	culture = nightrunner
}
560155 = {
	name = "Sternhell"
	culture = nightrunner
}
560156 = {
	name = "Spearwarrior"
	culture = nightrunner
}
560157 = {
	name = "Skywillow"
	culture = nightrunner
}
560158 = {
	name = "Manwatcher"
	culture = nightrunner
}
560159 = {
	name = "Redtree"
	culture = nightrunner
}
560160 = {
	name = "Bloodmember"
	culture = nightrunner
}
560161 = {
	name = "Blackwolf"
	culture = nightrunner
}
560162 = {
	name = "Weirbar"
	culture = nightrunner
}
560163 = {
	name = "Wolfkiller"
	culture = nightrunner
}
560164 = {
	name = "Demonface"
	culture = nightrunner
}
560165 = {
	name = "Thanrage"
	culture = nightrunner
}
560166 = {
	name = "Songmask"
	culture = nightrunner
}
560167 = {
	name = "Torbow"
	culture = nightrunner
}
560168 = {
	name = "Godsword"
	culture = nightrunner
}
560169 = {
	name = "Woodsmane"
	culture = nightrunner
}
560170 = {
	name = "Goreshield"
	culture = nightrunner
}
560171 = {
	name = "Drydemon"
	culture = nightrunner
}
560172 = {
	name = "Warriorkin"
	culture = nightrunner
}
560173 = {
	name = "Woodskinner"
	culture = nightrunner
}
560560 = {
	name = "Songwarrior"
	culture = nightrunner
}
560175 = {
	name = "Burningboar"
	culture = nightrunner
}
560176 = {
	name = "Wolfmember"
	culture = nightrunner
}
560177 = {
	name = "Starwhisper"
	culture = nightrunner
}
560178 = {
	name = "Shadeface"
	culture = nightrunner
}
560179 = {
	name = "Sternsun"
	culture = nightrunner
}
560180 = {
	name = "Oldhorn"
	culture = nightrunner
}
560181 = {
	name = "Greenchief"
	culture = nightrunner
}
560182 = {
	name = "Bridgeshaper"
	culture = nightrunner
}
560183 = {
	name = "Redbone"
	culture = nightrunner
}
560184 = {
	name = "Sealtamer"
	culture = nightrunner
}
560185 = {
	name = "Pikehead"
	culture = nightrunner
}
560186 = {
	name = "Demonfrost"
	culture = nightrunner
}
560187 = {
	name = "Whitekiller"
	culture = nightrunner
}
560188 = {
	name = "Axehunt"
	culture = nightrunner
}
560189 = {
	name = "Bloodconn"
	culture = nightrunner
}
560190 = {
	name = "Skytree"
	culture = nightrunner
}
560191 = {
	name = "Boarhunter"
	culture = nightrunner
}
560192 = {
	name = "Icerage"
	culture = nightrunner
}
560193 = {
	name = "Heartcut"
	culture = nightrunner
}
560194 = {
	name = "Blackgaze"
	culture = nightrunner
}
560195 = {
	name = "Marawatcher"
	culture = nightrunner
}
560196 = {
	name = "Godserpent"
	culture = nightrunner
}
560197 = {
	name = "Kingsblood"
	culture = nightrunner
}
560198 = {
	name = "Hallowclaw"
	culture = nightrunner
}
560199 = {
	name = "Weirspear"
	culture = nightrunner
}
560200 = {
	name = "Bluebluff"
	culture = nightrunner
}
560201 = {
	name = "Lockmask"
	culture = nightrunner
}
560202 = {
	name = "Warriordemon"
	culture = nightrunner
}
560203 = {
	name = "Flintwood"
	culture = nightrunner
}
560204 = {
	name = "Egelskull"
	culture = nightrunner
}
560205 = {
	name = "Drynmane"
	culture = nightrunner
}
560206 = {
	name = "Shieldarm"
	culture = nightrunner
}
560207 = {
	name = "Torbow"
	culture = nightrunner
}
560208 = {
	name = "Highwolf"
	culture = nightrunner
}
560209 = {
	name = "Hardsheep"
	culture = nightrunner
}
560210 = {
	name = "Thannkin"
	culture = nightrunner
}
560211 = {
	name = "Bearhell"
	culture = nightrunner
}
560212 = {
	name = "Bronzegrand"
	culture = nightrunner
}
560213 = {
	name = "Mansword"
	culture = nightrunner
}
560214 = {
	name = "Crowguard"
	culture = nightrunner
}
560215 = {
	name = "Skullbane"
	culture = nightrunner
}
560216 = {
	name = "Spearbane"
	culture = nightrunner
}
560217 = {
	name = "Freemorn"
	culture = nightrunner
}
560218 = {
	name = "Granblade"
	culture = nightrunner
}
560219 = {
	name = "Nightstar"
	culture = nightrunner
}
560220 = {
	name = "Varashot"
	culture = nightrunner
}
560221 = {
	name = "Stonepeak"
	culture = nightrunner
}
560223 = {
	name = Hardblade
	culture = nightrunner
}
560224 = {
	name = Tallseeker
	culture = nightrunner
}
560225 = {
	name = Hellbelly
	culture = nightrunner
}
560226 = {
	name = Icebane
	culture = nightrunner
}
560227 = {
	name = Moonmourn
	culture = nightrunner
}
560228 = {
	name = Bluebeard
	culture = nightrunner
}
560229 = {
	name = Icecrest
	culture = nightrunner
}
560230 = {
	name = Steelwood
	culture = nightrunner
}
560231 = {
	name = Fasthand
	culture = nightrunner
}
560232 = {
	name = Amberscar
	culture = nightrunner
}
560233 = {
	name = Nightshield
	culture = nightrunner
}
560234 = {
	name = Threespark
	culture = nightrunner
}
560235 = {
	name = Stoneflame
	culture = nightrunner
}
560236 = {
	name = Fernwalker
	culture = nightrunner
}
560237 = {
	name = Steelcreek
	culture = nightrunner
}
560238 = {
	name = Iceaxe
	culture = nightrunner
}
560239 = {
	name = Yellowhair
	culture = nightrunner
}
