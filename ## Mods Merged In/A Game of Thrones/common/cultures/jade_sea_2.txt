moraqi_group = {
	graphical_cultures = { mesoamericangfx } # portraits, units

	farosi = {
		graphical_cultures = { southerngfx sanddornishgfx }

		color = { 0.5 1 0 } 
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			#CANON
			
			#NON-CANON
			#DAMOC
			Abad Alib Amid Akub Alaq Annan Alam Ahaq Adem Aden Akif Akram Ahsan Azim Azhar Asem Asif Alim Anjem Amjad Asghar Ayad Ayman Ayub
			"Ahaq Anaf" "Ahaq Alam" "Ahaq Uza" "Ahaq Haffar" "Ahaq Basat" "Ahaq Basar" "Ahaq Ahad" "Ahaq Alaq" "Ahaq Baq" "Ahaq Ghan" "Ahaq Hai" "Ahaq Hak" "Ahaq Usein" "Ahaq Karim" "Ahaq Jal" "Ahaq Tif" "Ahaq Qif" "Ahaq Zah" "Ahaq Hira" "Ahaq Wahud" "Ahaq Qud" "Ahaq Alib" "Ahaq Annan" "Ahaq Haliq" "Ahaq Abbar" "Ahaq Amid" "Ahaq Akim" "Ahaq Rab" "Ahaq Ahim" "Ahaq Razzaq" "Ahaq Sabur" "Ahaq Shakur" "Ahaq Rah"
			Basat Basar Baq Bahad Bagher Baki Bakir Bashar Boulos Boutros Burhan Badeeb
			Chasat Chakir Chazhar Chaqi Chalam
			Dhaq Dhazim Dhayd Dhaffar Daffar Darfeen
			Ekram Eraq Ezim Eyad Elam Ehaq Ehsan
			Faris Faheem Farid Faizan Faraq Farhat Fathi Fazi Furkan
			Ghan Gaffar Galim Gamjad Gaki Gakir Gaq Gulam Gulzar
			Hakam Haliq Halaq Hakrem Hazhar Hakif Halam Haleem Harun Hassaq Hichem Hikmat Hossam
			Ihaq Idraq Ibraq Ilyaq Imran Iqbal Ishtaq Ismaq Izzaq
			Jaffar Jabal Jaber Jahaq Jarrah Jasam Jawdaq Jihaq Jubay
			Karim Kadin Kab Kadir Kadri Kafeel Kazeem Kullhum Kulwar
			Laraq Lazim Latif Lakir
			Malak Mansar Marwan Mehd Moeen Mahir Mhaq Muhsin Munib Munif Munir Muraq Motaseem Murtaz Muzaffar
			Nabhi Nabal Nadeem Nadaq Nagub Nahyan Naim Najm Nasar Nazaf Nawfat Nawaz Nasuh Nur Nuraq Naseeb
			Obay Osmaq Ochem Olam Orhad Ohaq
			Qaid Qamar Qusay Qalam Qamir Qasin Qadih Qajjad Qahim Qaed Qhad Qissem Qafar Qazif Qizzaq Qhsan Qalam Qaffar Qalaq Qeem
			Razaq Rahak Raed Raham Ramaz Rashid Rayan Raza Rifaq Ruhaq 
			Saad Saadeen Sadik Sahar Sajad Shamad Shalman Shaqib Sattar Shakur Shukri Shakeem 
			Tahir Tahaq Tashid Tarun Tossam Turhan Tadeeb Talam Tazi Turkan Taizan Tarrah Taffar Tullhum Takir Tarid Tasar
			Usein Ulzar Uraq Ussam Useem Uqbal 
			Wael Wahad Wahid Wasim Wazar Wossam Walaq Waid Wakir
			Yasin Yasaf Yahyi Yahaq Yarun 
			Zaffar Zafer Zaim Zaid Zaki Zayn Zahed Zadeem Zaraq Zahab 	
			
		}
		female_names = {
			#CANON
			
			
			#NON-CANON
			#DAMOC
			Abadi Aliba Amida Akubi Annani Alami Ahaqi Ademi Adena Akifa Akrami Ahsani Azhari Asema Asifa Alima Anjemi Amjadi Ayadi Aymani Ayubi Aliya Afaf Akilah Azra
			Bushra Basata Chakira Chalami Chaima Dhaqi Dhazima Deena Dhaydi Darfeena Dalal Dalia Dina Eraqi Ezima Esma Esgher Fetima Ghani Galima Gamjadi Gakira Gaqi Gulami  
			Habiba Hasfa Hajra Haleema Hamida Hana Hanifa Havva Haliqi Halaqa Hafiza Hakrema Hazhara Hakifa Halami Haleema Haruni Hichema Hikmata Hossami Ihaqi Imrani
			Jabera Kareena Karima Katiya Kabi Kadira Kafeela Lazima Latifa Layan Leyila Lulwa Mediha Mehmuna Melika Merwa Meya Melek Mira Mirna Munira Munifa Mhaqi 
			Nadia Nafisa Naila Nahla Najat Naima Nadine Nayla Nejla Nezha Nusha Nabali Nadaqi Nagubi Nuri Naseeba Obaya Olami Ohaqi Qamari Qalami Qamira Qaedi Qhadi
			Qeemi Rahiba Rahima Rana Reema Rimasi Sadia Sabhina Saira Sakina Salma Samiya Sania Shadi Shami Shatha Shumalla Shakura Suha Saadi Tahira Tahaqi Tashida
			Taruni Turhani Talami Tazira Tasari Tarida Wafa Waida Wakira Wazara Yamina Yara Yasmin Zahra Zakiya Zaimi Zaida Zakina Zadeema Zahabi

		}
		from_dynasty_prefix = "of "
		prefix = yes

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0

		modifier = default_culture_modifier
	}
	
	moraqi = {
		graphical_cultures = { southerngfx sanddornishgfx }
		
		color = { 0 0.5 0 } 
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			#CANON
			
			#NON-CANON
			#DAMOC
			Abad Alib Amid Akub Alaq Annan Alam Ahaq Adem Aden Akif Akram Ahsan Azim Azhar Asem Asif Alim Anjem Amjad Asghar Ayad Ayman Ayub
			"Ahaq Anaf" "Ahaq Alam" "Ahaq Uza" "Ahaq Haffar" "Ahaq Basat" "Ahaq Basar" "Ahaq Ahad" "Ahaq Alaq" "Ahaq Baq" "Ahaq Ghan" "Ahaq Hai" "Ahaq Hak" "Ahaq Usein" "Ahaq Karim" "Ahaq Jal" "Ahaq Tif" "Ahaq Qif" "Ahaq Zah" "Ahaq Hira" "Ahaq Wahud" "Ahaq Qud" "Ahaq Alib" "Ahaq Annan" "Ahaq Haliq" "Ahaq Abbar" "Ahaq Amid" "Ahaq Akim" "Ahaq Rab" "Ahaq Ahim" "Ahaq Razzaq" "Ahaq Sabur" "Ahaq Shakur" "Ahaq Rah"
			Basat Basar Baq Bahad Bagher Baki Bakir Bashar Boulos Boutros Burhan Badeeb
			Chasat Chakir Chazhar Chaqi Chalam
			Dhaq Dhazim Dhayd Dhaffar Daffar Darfeen
			Ekram Eraq Ezim Eyad Elam Ehaq Ehsan
			Faris Faheem Farid Faizan Faraq Farhat Fathi Fazi Furkan
			Ghan Gaffar Galim Gamjad Gaki Gakir Gaq Gulam Gulzar
			Hakam Haliq Halaq Hakrem Hazhar Hakif Halam Haleem Harun Hassaq Hichem Hikmat Hossam
			Ihaq Idraq Ibraq Ilyaq Imran Iqbal Ishtaq Ismaq Izzaq
			Jaffar Jabal Jaber Jahaq Jarrah Jasam Jawdaq Jihaq Jubay
			Karim Kadin Kab Kadir Kadri Kafeel Kazeem Kullhum Kulwar
			Laraq Lazim Latif Lakir
			Malak Mansar Marwan Mehd Moeen Mahir Mhaq Muhsin Munib Munif Munir Muraq Motaseem Murtaz Muzaffar
			Nabhi Nabal Nadeem Nadaq Nagub Nahyan Naim Najm Nasar Nazaf Nawfat Nawaz Nasuh Nur Nuraq Naseeb
			Obay Osmaq Ochem Olam Orhad Ohaq
			Qaid Qamar Qusay Qalam Qamir Qasin Qadih Qajjad Qahim Qaed Qhad Qissem Qafar Qazif Qizzaq Qhsan Qalam Qaffar Qalaq Qeem
			Razaq Rahak Raed Raham Ramaz Rashid Rayan Raza Rifaq Ruhaq 
			Saad Saadeen Sadik Sahar Sajad Shamad Shalman Shaqib Sattar Shakur Shukri Shakeem 
			Tahir Tahaq Tashid Tarun Tossam Turhan Tadeeb Talam Tazi Turkan Taizan Tarrah Taffar Tullhum Takir Tarid Tasar
			Usein Ulzar Uraq Ussam Useem Uqbal 
			Wael Wahad Wahid Wasim Wazar Wossam Walaq Waid Wakir
			Yasin Yasaf Yahyi Yahaq Yarun 
			Zaffar Zafer Zaim Zaid Zaki Zayn Zahed Zadeem Zaraq Zahab 	
			
		}
		female_names = {
			#CANON
			
			
			#NON-CANON
			#DAMOC
			Abadi Aliba Amida Akubi Annani Alami Ahaqi Ademi Adena Akifa Akrami Ahsani Azhari Asema Asifa Alima Anjemi Amjadi Ayadi Aymani Ayubi Aliya Afaf Akilah Azra
			Bushra Basata Chakira Chalami Chaima Dhaqi Dhazima Deena Dhaydi Darfeena Dalal Dalia Dina Eraqi Ezima Esma Esgher Fetima Ghani Galima Gamjadi Gakira Gaqi Gulami  
			Habiba Hasfa Hajra Haleema Hamida Hana Hanifa Havva Haliqi Halaqa Hafiza Hakrema Hazhara Hakifa Halami Haleema Haruni Hichema Hikmata Hossami Ihaqi Imrani
			Jabera Kareena Karima Katiya Kabi Kadira Kafeela Lazima Latifa Layan Leyila Lulwa Mediha Mehmuna Melika Merwa Meya Melek Mira Mirna Munira Munifa Mhaqi 
			Nadia Nafisa Naila Nahla Najat Naima Nadine Nayla Nejla Nezha Nusha Nabali Nadaqi Nagubi Nuri Naseeba Obaya Olami Ohaqi Qamari Qalami Qamira Qaedi Qhadi
			Qeemi Rahiba Rahima Rana Reema Rimasi Sadia Sabhina Saira Sakina Salma Samiya Sania Shadi Shami Shatha Shumalla Shakura Suha Saadi Tahira Tahaqi Tashida
			Taruni Turhani Talami Tazira Tasari Tarida Wafa Waida Wakira Wazara Yamina Yara Yasmin Zahra Zakiya Zaimi Zaida Zakina Zadeema Zahabi

		}
		from_dynasty_prefix = "of "
		prefix = yes
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
	
	zabhadi = {
		graphical_cultures = { southerngfx sanddornishgfx }
		
		color = { 0.82 0.83 0.4 } 
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			#CANON
			
			#NON-CANON
			#DAMOC
			Abad Alib Amid Akub Alaq Annan Alam Ahaq Adem Aden Akif Akram Ahsan Azim Azhar Asem Asif Alim Anjem Amjad Asghar Ayad Ayman Ayub
			"Ahaq Anaf" "Ahaq Alam" "Ahaq Uza" "Ahaq Haffar" "Ahaq Basat" "Ahaq Basar" "Ahaq Ahad" "Ahaq Alaq" "Ahaq Baq" "Ahaq Ghan" "Ahaq Hai" "Ahaq Hak" "Ahaq Usein" "Ahaq Karim" "Ahaq Jal" "Ahaq Tif" "Ahaq Qif" "Ahaq Zah" "Ahaq Hira" "Ahaq Wahud" "Ahaq Qud" "Ahaq Alib" "Ahaq Annan" "Ahaq Haliq" "Ahaq Abbar" "Ahaq Amid" "Ahaq Akim" "Ahaq Rab" "Ahaq Ahim" "Ahaq Razzaq" "Ahaq Sabur" "Ahaq Shakur" "Ahaq Rah"
			Basat Basar Baq Bahad Bagher Baki Bakir Bashar Boulos Boutros Burhan Badeeb
			Chasat Chakir Chazhar Chaqi Chalam
			Dhaq Dhazim Dhayd Dhaffar Daffar Darfeen
			Ekram Eraq Ezim Eyad Elam Ehaq Ehsan
			Faris Faheem Farid Faizan Faraq Farhat Fathi Fazi Furkan
			Ghan Gaffar Galim Gamjad Gaki Gakir Gaq Gulam Gulzar
			Hakam Haliq Halaq Hakrem Hazhar Hakif Halam Haleem Harun Hassaq Hichem Hikmat Hossam
			Ihaq Idraq Ibraq Ilyaq Imran Iqbal Ishtaq Ismaq Izzaq
			Jaffar Jabal Jaber Jahaq Jarrah Jasam Jawdaq Jihaq Jubay
			Karim Kadin Kab Kadir Kadri Kafeel Kazeem Kullhum Kulwar
			Laraq Lazim Latif Lakir
			Malak Mansar Marwan Mehd Moeen Mahir Mhaq Muhsin Munib Munif Munir Muraq Motaseem Murtaz Muzaffar
			Nabhi Nabal Nadeem Nadaq Nagub Nahyan Naim Najm Nasar Nazaf Nawfat Nawaz Nasuh Nur Nuraq Naseeb
			Obay Osmaq Ochem Olam Orhad Ohaq
			Qaid Qamar Qusay Qalam Qamir Qasin Qadih Qajjad Qahim Qaed Qhad Qissem Qafar Qazif Qizzaq Qhsan Qalam Qaffar Qalaq Qeem
			Razaq Rahak Raed Raham Ramaz Rashid Rayan Raza Rifaq Ruhaq 
			Saad Saadeen Sadik Sahar Sajad Shamad Shalman Shaqib Sattar Shakur Shukri Shakeem 
			Tahir Tahaq Tashid Tarun Tossam Turhan Tadeeb Talam Tazi Turkan Taizan Tarrah Taffar Tullhum Takir Tarid Tasar
			Usein Ulzar Uraq Ussam Useem Uqbal 
			Wael Wahad Wahid Wasim Wazar Wossam Walaq Waid Wakir
			Yasin Yasaf Yahyi Yahaq Yarun 
			Zaffar Zafer Zaim Zaid Zaki Zayn Zahed Zadeem Zaraq Zahab 	
			
		}
		female_names = {
			#CANON
			
			
			#NON-CANON
			#DAMOC
			Abadi Aliba Amida Akubi Annani Alami Ahaqi Ademi Adena Akifa Akrami Ahsani Azhari Asema Asifa Alima Anjemi Amjadi Ayadi Aymani Ayubi Aliya Afaf Akilah Azra
			Bushra Basata Chakira Chalami Chaima Dhaqi Dhazima Deena Dhaydi Darfeena Dalal Dalia Dina Eraqi Ezima Esma Esgher Fetima Ghani Galima Gamjadi Gakira Gaqi Gulami  
			Habiba Hasfa Hajra Haleema Hamida Hana Hanifa Havva Haliqi Halaqa Hafiza Hakrema Hazhara Hakifa Halami Haleema Haruni Hichema Hikmata Hossami Ihaqi Imrani
			Jabera Kareena Karima Katiya Kabi Kadira Kafeela Lazima Latifa Layan Leyila Lulwa Mediha Mehmuna Melika Merwa Meya Melek Mira Mirna Munira Munifa Mhaqi 
			Nadia Nafisa Naila Nahla Najat Naima Nadine Nayla Nejla Nezha Nusha Nabali Nadaqi Nagubi Nuri Naseeba Obaya Olami Ohaqi Qamari Qalami Qamira Qaedi Qhadi
			Qeemi Rahiba Rahima Rana Reema Rimasi Sadia Sabhina Saira Sakina Salma Samiya Sania Shadi Shami Shatha Shumalla Shakura Suha Saadi Tahira Tahaqi Tashida
			Taruni Turhani Talami Tazira Tasari Tarida Wafa Waida Wakira Wazara Yamina Yara Yasmin Zahra Zakiya Zaimi Zaida Zakina Zadeema Zahabi

		}
		from_dynasty_prefix = "of "
		prefix = yes
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
	
	vahari = {
		graphical_cultures = { southerngfx sanddornishgfx }
		
		color = { 0.5 0.9 0.1 } 
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			#CANON
			
			#NON-CANON
			#DAMOC
			Abad Alib Amid Akub Alaq Annan Alam Ahaq Adem Aden Akif Akram Ahsan Azim Azhar Asem Asif Alim Anjem Amjad Asghar Ayad Ayman Ayub
			"Ahaq Anaf" "Ahaq Alam" "Ahaq Uza" "Ahaq Haffar" "Ahaq Basat" "Ahaq Basar" "Ahaq Ahad" "Ahaq Alaq" "Ahaq Baq" "Ahaq Ghan" "Ahaq Hai" "Ahaq Hak" "Ahaq Usein" "Ahaq Karim" "Ahaq Jal" "Ahaq Tif" "Ahaq Qif" "Ahaq Zah" "Ahaq Hira" "Ahaq Wahud" "Ahaq Qud" "Ahaq Alib" "Ahaq Annan" "Ahaq Haliq" "Ahaq Abbar" "Ahaq Amid" "Ahaq Akim" "Ahaq Rab" "Ahaq Ahim" "Ahaq Razzaq" "Ahaq Sabur" "Ahaq Shakur" "Ahaq Rah"
			Basat Basar Baq Bahad Bagher Baki Bakir Bashar Boulos Boutros Burhan Badeeb
			Chasat Chakir Chazhar Chaqi Chalam
			Dhaq Dhazim Dhayd Dhaffar Daffar Darfeen
			Ekram Eraq Ezim Eyad Elam Ehaq Ehsan
			Faris Faheem Farid Faizan Faraq Farhat Fathi Fazi Furkan
			Ghan Gaffar Galim Gamjad Gaki Gakir Gaq Gulam Gulzar
			Hakam Haliq Halaq Hakrem Hazhar Hakif Halam Haleem Harun Hassaq Hichem Hikmat Hossam
			Ihaq Idraq Ibraq Ilyaq Imran Iqbal Ishtaq Ismaq Izzaq
			Jaffar Jabal Jaber Jahaq Jarrah Jasam Jawdaq Jihaq Jubay
			Karim Kadin Kab Kadir Kadri Kafeel Kazeem Kullhum Kulwar
			Laraq Lazim Latif Lakir
			Malak Mansar Marwan Mehd Moeen Mahir Mhaq Muhsin Munib Munif Munir Muraq Motaseem Murtaz Muzaffar
			Nabhi Nabal Nadeem Nadaq Nagub Nahyan Naim Najm Nasar Nazaf Nawfat Nawaz Nasuh Nur Nuraq Naseeb
			Obay Osmaq Ochem Olam Orhad Ohaq
			Qaid Qamar Qusay Qalam Qamir Qasin Qadih Qajjad Qahim Qaed Qhad Qissem Qafar Qazif Qizzaq Qhsan Qalam Qaffar Qalaq Qeem
			Razaq Rahak Raed Raham Ramaz Rashid Rayan Raza Rifaq Ruhaq 
			Saad Saadeen Sadik Sahar Sajad Shamad Shalman Shaqib Sattar Shakur Shukri Shakeem 
			Tahir Tahaq Tashid Tarun Tossam Turhan Tadeeb Talam Tazi Turkan Taizan Tarrah Taffar Tullhum Takir Tarid Tasar
			Usein Ulzar Uraq Ussam Useem Uqbal 
			Wael Wahad Wahid Wasim Wazar Wossam Walaq Waid Wakir
			Yasin Yasaf Yahyi Yahaq Yarun 
			Zaffar Zafer Zaim Zaid Zaki Zayn Zahed Zadeem Zaraq Zahab 	
			
		}
		female_names = {
			#CANON
			
			
			#NON-CANON
			#DAMOC
			Abadi Aliba Amida Akubi Annani Alami Ahaqi Ademi Adena Akifa Akrami Ahsani Azhari Asema Asifa Alima Anjemi Amjadi Ayadi Aymani Ayubi Aliya Afaf Akilah Azra
			Bushra Basata Chakira Chalami Chaima Dhaqi Dhazima Deena Dhaydi Darfeena Dalal Dalia Dina Eraqi Ezima Esma Esgher Fetima Ghani Galima Gamjadi Gakira Gaqi Gulami  
			Habiba Hasfa Hajra Haleema Hamida Hana Hanifa Havva Haliqi Halaqa Hafiza Hakrema Hazhara Hakifa Halami Haleema Haruni Hichema Hikmata Hossami Ihaqi Imrani
			Jabera Kareena Karima Katiya Kabi Kadira Kafeela Lazima Latifa Layan Leyila Lulwa Mediha Mehmuna Melika Merwa Meya Melek Mira Mirna Munira Munifa Mhaqi 
			Nadia Nafisa Naila Nahla Najat Naima Nadine Nayla Nejla Nezha Nusha Nabali Nadaqi Nagubi Nuri Naseeba Obaya Olami Ohaqi Qamari Qalami Qamira Qaedi Qhadi
			Qeemi Rahiba Rahima Rana Reema Rimasi Sadia Sabhina Saira Sakina Salma Samiya Sania Shadi Shami Shatha Shumalla Shakura Suha Saadi Tahira Tahaqi Tashida
			Taruni Turhani Talami Tazira Tasari Tarida Wafa Waida Wakira Wazara Yamina Yara Yasmin Zahra Zakiya Zaimi Zaida Zakina Zadeema Zahabi

		}
		from_dynasty_prefix = "of "
		prefix = yes
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
}

yi_ti_group = {
	graphical_cultures = { mesoamericangfx } # portraits, units

	yi_ti = {
		graphical_cultures = { chinesegfx mongolgfx yitigfx }
		
		color = { 0.5 0.8 0.05 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			#Canon
			Bu 
			Doq Duq 
			Choq
			Gai
			Han Har
			Joq
			Loi 
			Po  
			Quen Qo
			Tho
			
			#DAMOC + Non-Canon
			
			Ao An Aoq Auq Aoi Quen Aho
			Bo Bai Bao Boq Buq Bhoq Boi Buen Bho Bar Ban Bi
			Chan Cho Chu Chen Chao Chin Coq Chin Cho Choq Car Can Coi Cuen Co Ci
			Da Dai Du Dhoq Dan Dar Ding Doq Doi Do Duen Dho Di
			Fa Fai Fu Doq Fuq Fan Far Fong Foi Fo Fuen Fho Fi
			Gang Gu Goq Duq Ghoq Gan Gar Goi Go Guen Gho Gi
			Hai Hao Hu Huan Har Ho Hoi Hi
			Ji Ju Joq Juq Jhoq Jan Jar Joi Jo Juen Jho
			Lan Lao Luq Lo Li Lar Lhoq Luen Lho Li
			Nu Mo Nhu Noq Nuq Nai Nan Nar Noi No Nuen Nho Ni 
			Pao Ping Phoq Puq Pu Poq Phoq Phan Phar Pan Par Pong Pho Puen Pi
			Quan Quy Qu Qoq Quq Qai Qan Qar Qoi Qo Qho Qin
			Ru Roq Ruq Rai Ran Roi Ro Ruen Rho Ri
			San Shen Shinu Shun Su Soq Shoq Sai San Sar So Suen Shu Si Sun Sung
			Van Vuen Vu Voq Vai Van Var Voi Vo Vi
			Wo Who Wai Wan Wi Wu
			Xun Xuan Xhe Xai Xho Xar Xan Xo Xi
			Yu Yao Yo Yuen Yoq Yan Yi
			Zei Zu Zuq Zhoq Zai Zan Zar Zo Zhao Zho Zhao Zong Zuan Zi
			"Zong San" "Zong An" "Zong Co" "Zong Dan" "Har Li" "Har Lo" "Fho Go" "Fho So" "Ho Boi" "Xan Li" "Quen Lo" "Ji Bao" "Ji Qi" "Ji Sho"
			"Ji Sun" "Ye Sun" "Fa Sun" "Hi Sun" "Pu Sun" "Ji Sho" "Ji Wan" "Sho Wan" "Ju Wan" "Jhoq Vo" "Guen Qo" "Juen Ni" "Par Sun" "Wi Sun"
			"Jia Wu" "Yu Wu" "Yan Wu" "Wu Yu" "Nho Sho" "Xo Phu" "No Jho" "Ho Gho" "Goq Suen" "Wo Cai" "Wo Qan" "Wo Vi" "Vi Sun" "Vi Li" "Vi Nho"
		}
		female_names = {
			#Canon
			#Bathi is a surname
			
			#DAMOC + Non-Canon
			
			Ah Ai An Aoi Ae
			Bai Bao Bo Boq Buq Bha Be Ba Bi
			Cam Chan Chi Cu Cai Ciq Chin Cha Cuyen Ce Ca Ci
			Dai Di De Dha Duyen Da Di
			Han Ho Hu Hai Hoi Ha He Hi
			Jia Jyl Ju Jha Jar Jan Juyen Jo Jin Ja 
			Kim Ku Kai Ke Kei Kan Kar Koi Ko Kuyen Kha Kho Ka Ki Khis
			Lien Ling Lai Lu Lo Lha Le La Li
			Mai Mu Ming Mha Ma Muyen Me Ming
			Ning Nai Nha Na Ne Ni Noi Nuyen Niq Niq
			Ping Peng Pha Pu Pai Poi Puyen Pe Pa Piq Pi
			Quyen Qi Qha Qu Qai Qoi Qa Qe Qiq Qi Qan Qar
			Shu Shi Si Sha Se Sa Su Sai Song Suyen Siq Si 
			Thi Thu Thuy Tien Ting Tu Tha Tai Tuyen Te Ta Toi Tiq Ti
			Ve Va Viq Vi
			Wai Wan Wa We Wha Wo Wi Wiq 
			Xiu Xha Xia Xian Xai Xe Xa Xiq Xi
			Yen Ya Ye Yai Yoi Yuyen Yiq Ying
			Zu Zha Zi Za Ze Zuyen Zoi Zai
			"Ah Bai" "Ah Bao" "Mo Cu" "Mo Cha" "Mo Ce" "Hoi Ja" "Hoi Ke" "Hoi Niq" "Hoi Su" "Hoi Ne" "De Han" "De Ho" "De Hai" "Deh Hi" "Jia Ku" "Jia Cai" "Jai Bai"
			"Jia Li" "Jia Lu" "Ji Si" "Jia Te" "Jia Zai" "Jia Ling" "Jia Kha" "Mha Jai" "Mha Si" "Mha Ja" "Mha Lha" "Me Qa" "Qa Ping" "Qi Si" "Qi Bi" "Qa Ai" "Qi Chi" "Ve Cai"
			"Mu Peng" "Mu Cha" "Mu Khis" "Mo Khis" "De Khis" "Jia Khis" "Mha Khis" "Lo Khis" "Qai Lo" "Qai Mi" "Tha Si" "Tha Mi" "Tha Ci" "Yi Jan" "Yi Koi" "Yi Bai" "Yi Ai"
		}
		from_dynasty_prefix = "of "
		prefix = yes
		dynasty_name_first = yes
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
		
		disinherit_from_blinding = yes
	}
	asabhadi = {
		graphical_cultures = { easterngfx sanddornishgfx }
		
		color = { 0.87 0.75 0.38 } 
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = { #Mix of Hyrkoon and Yi Ti
			Hyrkoon
			
			Aaro Ahvo Armas Auvo Antto Aslak Aulis
			Dyrvid
			Eero Elmeri Esko
			Hyrkko Hyrno Hannu
			Ikka Isko Iyvari
			Jyri Jyrki Jarkko Juha Jouko
			Kalevoon Kain Kari Kyosti
			Lynnart
			Markku Mykoon Myikku
			Nyilo Nyyrikki
			Oiva Orvo
			Pyrkoon Pekko Pyrkko Panu
			Ryku Raimo Raitis
			Sakari Soini Saku Santtu
			Taito Tero Tuukka Touko Tarvo
			Usko Uljas
			Veikko Voitto Valto	
			
			Ah Ai An
			Boq Buq Bha
			Cai Ciq Chin Cha Cuyen Ce
			Dai Dha Duyen Da
			Han Hai Hoi
			Jha Jar Jan Juyen
			Kim Kai Kei Kan Kar Kuyen Kha Kho Khis
			Lien Ling Lai  Li
			Mai Ming Mha Muyen
			Ning Nai NhaNoi Nuyen
			Ping Peng Pha Puyen Pe Pa
			Quyen Qha Qai Qoi Qa Qan Qar
			Shu Shi Sha Sai Soi Suyen Siq
			Thi Thu Thuy Tien TingTha Tai Tuyen Toi Tiq
			Va Viq
			Wai Wan Wha Wiq 
			Xiu Xha Xian Xai Xiq
			Yen Yai Yuyen Yiq
			Zha Zuyen Zoi Zai
			
		}
		female_names = {
			Aava Aulikki Aura Annukka Arja Aynikki Ayri
			Elviira Esteri Eyni Eeva Eerika Erja
			Fynni
			Hylkka Hyrlli Hellevi Hyrnna Hyrkka
			Inka Iyta Inkeri
			Janika Jutta
			Kyllikki Kyrtta Kynerva Kyira Kylli
			Lyyti Lumikki Laimi Loviisa
			Myrva Marjukka Maija Maila Minea Myrka
			Nelli Naima Noora
			Orvokki Onyrva 
			Pyrkka Pilvi Pirja
			Rauna Rykka Rytva Raisa
			Sylvi Syrkka Syrpa Saima Sari Soila Suvi
			Tyrtta Taava Tekla Tari
			
			Ai Aoi
			Bai Bao Boq Buq Bha
			Cam Chan Chi Cai Chin Cha Cuyen
			Dai Dha Duyen
			Han Hai Hoi
			Jia Jyl Jha Jar Jan Juyen
			Kim Kai Kei Kan Kuyen Kha Kho Khis
			Lien Ling Lai Lha
			Mai Ming Mha Muyen
			Ning Nai Nha Noi Nuyen Niq
			Ping Peng Pha Poi Puyen iq
			Quyen  Qha Qai Qoi Qiq Qan Qar
			Shu Shi  Sha Sai Soi Suyen Siq
			Thi Thu Thuy Tien Ting Tai Tuyen Toi Tiq
			Ve Viq 
			Wai Wan Wha  Wiq 
			Xiu Xha Xia Xian Xai Xiq 
			Yen Yai Yoi Yuyen Yiq 
			Zha Zi Ze Zuyen Zoi Zai

		}
		from_dynasty_prefix = "of "
		prefix = yes
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
}
jade_islands_group = {
	graphical_cultures = { indiangfx southyrosgfx } # buildings

	lengi = {
		graphical_cultures = { turkishgfx  naathigfx } # portraits, units 
		#horde = yes
		
		color = { 0 0.65 0.4 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			Abdul Atal Alijah Abholos Alala Ammutseba Arwassa
			Barzai Baoht Basatan Bokrug Byatis
			Chaugnar Cthaat
			Dhumin
			Exior Exekiel Eihort "Ei'lor"
			Ghisguth
			Horvath Han Hastalyk Hastur Hnarqu
			Ibn Ithaqua
			Japhet "Janai'ngo"
			Kuranes Keziah Kaalut "Khal'kru" Kurpannga
			Lam
			Mormo Mortllgh
			Nephren "Ngirrth'lu" Northot Nycrama
			Obed Othuum
			Pharol
			Quyagen
			Raandaii "Rh'Thulla" Rokon Ruhtra
			Sunand Surama Sebek Shaklatal Shathak Shlithneth Summanus
			"T'yog" Thog
			Vibur
			Yakthoob Yhashtur Yorith
			Xirdneth
			Zanthu Zathog
		}
		
		female_names = {			
			Khiara:300 #canon
			
			Abigail "Ayi'ig" Aylith
			Coatlicue Cthaeghya Cthylla
			Dygra Dythalla Dzewa
			Gleetha "Gur'la" Gzxtyos
			Idh-yaa Inpesca Istasha
			Kassogtha Klosmiebhyx
			Lavinia Lythalia
			Mynoghra
			Nctosa Nctolhu Nyogtha
			Ragnalla
			"Saa'itii" Scathach Sthanee
			Thanaroa Turua
			Voltiyiga
			Xotli
			Yhagni "Y'lla"
		}
			
		pat_grf_name_chance = 25
		mat_grf_name_chance = 25
		father_name_chance = 25
		
		pat_grm_name_chance = 25
		mat_grm_name_chance = 25
		mother_name_chance = 25
		
		modifier = default_culture_modifier
		
		disinherit_from_blinding = yes
	}

}
asshai_group = {
	graphical_cultures = { indiangfx ghiscarigfx } # buildings
	
	asshai = {
		graphical_cultures = { romangfx ghiscarigfx } # portraits
	
		color = { 0.75 0.0 0.0 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		character_modifier = {
			dragon_hatching = 0.5
			dragon_taming = 1.5
		}
		
		#Sumerian
		male_names = {
			#NON CANON	
			Ammith Amurri Arganth Akiya Awil
			Belshun Bunuth
			Dagan Dadasig Damuzi Dadazig Dadanum
			Enlil Errath
			Gudeath
			Hunhal Humbab
			Irrara Ishtup Iddin Ishme Iluath Ibnatum Ipquath
			Kabid Kuda Kurum Kabkath Kugbau
			Limer Luenlila "La'um" Lugatum Lipith
			Mesanepada Mushtal Mutubisir Mamagal Munawirtum
			Nurkubi	Nawirum	Ninurta Naqir Namhu Nergal
			Puzur Palusum
			Quazur Quabum Qasmath
			Shusuen Shulgi Shepsin Shullat Sidu Sumalika Shulpae Shamash Shatath Samuqan
			Tehuth
			Utu Urnammu Ugazum Ubaruth
			Warad Wusum
			Yatum Yasmath		
		}
		
		female_names = {
			#CANON
			Quaithe
			
			#NON CANON
			Aea Anu Ahassuth Aya
			Belessa Bikku Bau
			Daqqartum Delondra
			Erethe Erethkiga Enheduana Eala
			Gemethega Gemithe
			Ishtare Ilutha Irkalla
			Kithar
			Murrithe Murrim
			Ninhursag Nin Ninbanda Nammu Ninsun Nintuda Nisaba Nindukugga Nanna
			Puabi
			Quaiduri
			Sapurtum Summirathe Shubad Silili
			Taram
			Urbau
			Waqrat
			Zimu
		}
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}

	shadowlander = {
		graphical_cultures = { dalmatiangfx shadowlandergfx } # portraits shadowgfx
	
		color = { 0.75 0.0 0.0 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		#Sumerian
		male_names = {
			#NON CANON	
			Ammith Amurri Arganth Akiya Awil
			Belshun Bunuth
			Dagan Dadasig Damuzi Dadazig Dadanum
			Enlil Errath
			Gudeath
			Hunhal Humbab
			Irrara Ishtup Iddin Ishme Iluath Ibnatum Ipquath
			Kabid Kuda Kurum Kabkath Kugbau
			Limer Luenlila "La'um" Lugatum Lipith
			Mesanepada Mushtal Mutubisir Mamagal Munawirtum
			Nurkubi	Nawirum	Ninurta Naqir Namhu Nergal
			Puzur Palusum
			Quazur Quabum Qasmath
			Shusuen Shulgi Shepsin Shullat Sidu Sumalika Shulpae Shamash Shatath Samuqan
			Tehuth
			Utu Urnammu Ugazum Ubaruth
			Warad Wusum
			Yatum Yasmath		
		}
		
		female_names = {
			#CANON
			Quaithe
			
			#NON CANON
			Aea Anu Ahassuth Aya
			Belessa Bikku Bau
			Daqqartum Delondra
			Erethe Erethkiga Enheduana Eala
			Gemethega Gemithe
			Ishtare Ilutha Irkalla
			Kithar
			Murrithe Murrim
			Ninhursag Nin Ninbanda Nammu Ninsun Nintuda Nisaba Nindukugga Nanna
			Puabi
			Quaiduri
			Sapurtum Summirathe Shubad Silili
			Taram
			Urbau
			Waqrat
			Zimu
		}
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = backward_culture_modifier
	}
}
further_east = {
	graphical_cultures = { freefolkgfx } # buildings
	
	grey_waste = {
		graphical_cultures = { freefolkgfx }
		
		color = { 0.3 0.3 0.3 } #Grey
		
		used_for_random = no
		alternate_start = { 
			has_alternate_start_parameter = { key = culture value = full_random } 
			NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } }
		}
		
		male_names = {
			Borug Buggug Burghed Bagdud
			Durzub
			Faghig Fidgug
			Garekk Gomatug Ghamonk Grung Gnabadug Gaakt
			Hibub
			Ignatz Ignorg
			Karrghed Kilug
			Mug Murkub
			Oggugat Otugbu Oglub Oakgu
			Pehrakgu Parfu
			Sorgulg Shamob Sulgha
			Urgran Uzul Urim Ug Urbul
			Vigdug Vulug Vetorkag Viguka Vamugbu
			Xothkug Xugarf	Xutjja	 		
			Zurgha Zarod Zurpigig Zorgulg Zulmthu
		}
		female_names = {
			Bumph Borgakh Bulfim Bula Bulak Bolar Bor Badbog
			Dulug
			Gashnakh Gluronk Gharol Ghob Gulfim Ghak Ghorza
			Homraz
			Lambug Lazgar Lagakh
			Mor Morn Murzush Mazoga Mogak
			Oghash
			Rogmesh Rulfim Ragash
			Sharn Shel Shazgob Shagar Sharamph Sharog
			Ulumpha Urog Ushat Ushug Urzoth Ugak Ugor
			Yazgash Yotul
		}

		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = backward_culture_modifier
	}
}
qarth_group = {
	graphical_cultures = { ghiscarigfx } # buildings
	
	qartheen = {
		graphical_cultures = { qartheengfx } # portraits
		
		color = { 0.25 0.5 0.75 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			#CANON
			Egon Mathos Pyat Sallor Sybassion Urrathon Wendello Saathos Xaro Xandarro
			
			#NON CANON
			Gathril
			Navathos
			Mundello
			Ognos
			Regon
			Surindos Semython
			Wullon
			Zathor
			
			#DAMOC
			
			"Athe Nades" "Harm Odios" "Astyn Naax" "Chilon Cedes" "Demo Ilon" Edellos Daathos Ayat Dyat Igon Nallor Mallor Wallor Nurrathon Arrathon Sathon Paathos Daathos Xallor Daro 
			Phalinos  Zendello Zagon Tebos Lydathos Tallor Tathos Apellos Lemnus Aabderos Tharybos Phanos Lyodamos Lyat Kyat Periso Chabrio Telemacho Dyathrambo Thersito
			Chigon Chathos Kathon Arrotho Podarro Naadaro Sydaaro Kallinos Timonax Chalcodon Damasto "Kae Xheno" "Pharno Odon" Tychido Proto Agropo Lathos Hallor Ochos "Clin Thoon"
			Kephalo Dunbello Kenbello Xathos Xallor Xaantho Qallos Qendello Qara Qyat Qaathos Qgon "Qgon Dhee" Kybassion Dybassion Tybassion Tython Ducethon Talathon Pharathon Xaathon
			Paathon Maathon Thon Tendello Agon "Agon Kree" "Thon Mhee"
		}
		
		female_names = {
		#NON CANON
		#DAMOC
		
		Damis Cedea Phalina Anygoni Chabris Thersitas Cupha Euphenas Oiclas Podalina Lycareta Kallinas Timona Pharna Tychida Astya Naxi Chilona Dema Edella Dathi Ayati Igona Nallora Mallora Wallora 
		Sathonis Paathis Daathis Xallora Daris Phalinas Zagona Teba Lydathi Tallora Allora Apellas Lemnas Abdera Tharyba Phani Lydami Lyati Kyati Perisa Telemacha Dyathramba Chigonis Damia Kypris
		Xantippes Omphalis Xara Megara Erenike Myrine Katina Kyadora Nemerte Iphis Issa Sybasteia "Ar Monia" Raisa Nysa Cratais Dianais Antheia Thyia Rhene Xanthe Evadne Cytheria Chiore Achradina
		Thalassi Maiandria Antehe Sotera Lanike Orythia Pylanta "Pyona Mey" Lathis Hathis	"Kala Ida" "Nal Ora" "Alo Isa" "Oda Xaen" "Sita Oan"
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		seafarer = yes
		
		modifier = default_culture_modifier
	}
}