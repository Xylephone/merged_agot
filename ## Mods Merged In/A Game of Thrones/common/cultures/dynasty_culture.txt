Dynasty = {
	graphical_cultures = { westerngfx } # buildings, portraits, units
	
	Baratheon_dyn = {
		graphical_cultures = { baratheongfx } # portraits
		
		used_for_random = no
		alternate_start = { always = no }
		color = { 0.15 0.15 0.15 }
		allow_in_ruler_designer = no
		male_names = {
			Aerys:400 Aegon:1100 Aegor Aelix Aemon:400 Aemond Aenar Aenys:200 Aerion:300 Aeryn Aelor
			Baelor:200 Baelon:300
			Daegar:50 Daemon:400 Daeron:600 Daemion
			Gaemon:200 Haegon Haerys:50
			Jaekar:50 Jaehaerys:400 Jacaerys
			Lucerys Laenor
			Maelys Maegon Maegor:200 Maekar Maerys:50 Matarys Maelor
			Orys:50 Otherys:50
			Qoherys:50
			Rhaegar Rhaegel Rhaekar:50
			Valarr Viserys:400 Vaeron Vaekar:50 Vaermon Vaemond:50 Vaegon Valerion
		}
		female_names = {
			Allyria:50 Alearys:50 Aelinor Alysanne Aerea Alyssa Aelora
			Baela
			Cyeana Cymella:50 Calla		
			Daella:200 Daena Daeoril Daenerys:200 Daenys Daenaera Daenora
			Elaena:200
			Gael
			Helaena Haera 
			Jaehaera
			Leaysa:50 Laena 
			Maera:50 Maega:50 Maegelle
			Naerys Rhae Rhaelle Rhaena:300 Rhaeys:50 Rhaenyra Rhaelinor:50 Rhaenys:300 Rhalla
			Saenrys:50 Syaella:50 Shaera Saera Shaena
			Visenya:200 Vaera:50 Vaella:200 Viserra
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 5
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 5
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
    Lannister_dyn = {
		graphical_cultures = { lannistergfx } # portraits
		
		used_for_random = no
		allow_in_ruler_designer = no
		alternate_start = { always = no }
		color = { 0.15 0.15 0.15 }
		
		male_names = {
			Aerys:400 Aegon:1100 Aegor Aelix Aemon:400 Aemond Aenar Aenys:200 Aerion:300 Aeryn Aelor
			Baelor:200 Baelon:300
			Daegar:50 Daemon:400 Daeron:600 Daemion
			Gaemon:200 Haegon Haerys:50
			Jaekar:50 Jaehaerys:400 Jacaerys
			Lucerys Laenor
			Maelys Maegon Maegor:200 Maekar Maerys:50 Matarys Maelor
			Orys:50 Otherys:50
			Qoherys:50
			Rhaegar Rhaegel Rhaekar:50
			Valarr Viserys:400 Vaeron Vaekar:50 Vaermon Vaemond:50 Vaegon Valerion
		}
		female_names = {
			Allyria:50 Alearys:50 Aelinor Alysanne Aerea Alyssa Aelora
			Baela
			Cyeana Cymella:50 Calla		
			Daella:200 Daena Daeoril Daenerys:200 Daenys Daenaera Daenora
			Elaena:200
			Gael
			Helaena Haera 
			Jaehaera
			Leaysa:50 Laena 
			Maera:50 Maega:50 Maegelle
			Naerys Rhae Rhaelle Rhaena:300 Rhaeys:50 Rhaenyra Rhaelinor:50 Rhaenys:300 Rhalla
			Saenrys:50 Syaella:50 Shaera Saera Shaena
			Visenya:200 Vaera:50 Vaella:200 Viserra
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 5
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 5
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
    Reyne_dyn = {
		graphical_cultures = { reynegfx } # portraits
		
		used_for_random = no
		alternate_start = { always = no }
		color = { 0.15 0.15 0.15 }
		allow_in_ruler_designer = no
		male_names = {
			Aerys:400 Aegon:1100 Aegor Aelix Aemon:400 Aemond Aenar Aenys:200 Aerion:300 Aeryn Aelor
			Baelor:200 Baelon:300
			Daegar:50 Daemon:400 Daeron:600 Daemion
			Gaemon:200 Haegon Haerys:50
			Jaekar:50 Jaehaerys:400 Jacaerys
			Lucerys Laenor
			Maelys Maegon Maegor:200 Maekar Maerys:50 Matarys Maelor
			Orys:50 Otherys:50
			Qoherys:50
			Rhaegar Rhaegel Rhaekar:50
			Valarr Viserys:400 Vaeron Vaekar:50 Vaermon Vaemond:50 Vaegon Valerion
		}
		female_names = {
			Allyria:50 Alearys:50 Aelinor Alysanne Aerea Alyssa Aelora
			Baela
			Cyeana Cymella:50 Calla		
			Daella:200 Daena Daeoril Daenerys:200 Daenys Daenaera Daenora
			Elaena:200
			Gael
			Helaena Haera 
			Jaehaera
			Leaysa:50 Laena 
			Maera:50 Maega:50 Maegelle
			Naerys Rhae Rhaelle Rhaena:300 Rhaeys:50 Rhaenyra Rhaelinor:50 Rhaenys:300 Rhalla
			Saenrys:50 Syaella:50 Shaera Saera Shaena
			Visenya:200 Vaera:50 Vaella:200 Viserra
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 5
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 5
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
    Tully_dyn = {
		graphical_cultures = { tullygfx } # portraits
		
		used_for_random = no
		alternate_start = { always = no }
		color = { 0.15 0.15 0.15 }
		allow_in_ruler_designer = no
		male_names = {
			Aerys:400 Aegon:1100 Aegor Aelix Aemon:400 Aemond Aenar Aenys:200 Aerion:300 Aeryn Aelor
			Baelor:200 Baelon:300
			Daegar:50 Daemon:400 Daeron:600 Daemion
			Gaemon:200 Haegon Haerys:50
			Jaekar:50 Jaehaerys:400 Jacaerys
			Lucerys Laenor
			Maelys Maegon Maegor:200 Maekar Maerys:50 Matarys Maelor
			Orys:50 Otherys:50
			Qoherys:50
			Rhaegar Rhaegel Rhaekar:50
			Valarr Viserys:400 Vaeron Vaekar:50 Vaermon Vaemond:50 Vaegon Valerion
		}
		female_names = {
			Allyria:50 Alearys:50 Aelinor Alysanne Aerea Alyssa Aelora
			Baela
			Cyeana Cymella:50 Calla		
			Daella:200 Daena Daeoril Daenerys:200 Daenys Daenaera Daenora
			Elaena:200
			Gael
			Helaena Haera 
			Jaehaera
			Leaysa:50 Laena 
			Maera:50 Maega:50 Maegelle
			Naerys Rhae Rhaelle Rhaena:300 Rhaeys:50 Rhaenyra Rhaelinor:50 Rhaenys:300 Rhalla
			Saenrys:50 Syaella:50 Shaera Saera Shaena
			Visenya:200 Vaera:50 Vaella:200 Viserra
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 5
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 5
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
    Stark_dyn = {
		graphical_cultures = { starkgfx } # portraits
		
		used_for_random = no
		allow_in_ruler_designer = no
		alternate_start = { always = no }
		color = { 0.15 0.15 0.15 }
		
		male_names = {
			Aerys:400 Aegon:1100 Aegor Aelix Aemon:400 Aemond Aenar Aenys:200 Aerion:300 Aeryn Aelor
			Baelor:200 Baelon:300
			Daegar:50 Daemon:400 Daeron:600 Daemion
			Gaemon:200 Haegon Haerys:50
			Jaekar:50 Jaehaerys:400 Jacaerys
			Lucerys Laenor
			Maelys Maegon Maegor:200 Maekar Maerys:50 Matarys Maelor
			Orys:50 Otherys:50
			Qoherys:50
			Rhaegar Rhaegel Rhaekar:50
			Valarr Viserys:400 Vaeron Vaekar:50 Vaermon Vaemond:50 Vaegon Valerion
		}
		female_names = {
			Allyria:50 Alearys:50 Aelinor Alysanne Aerea Alyssa Aelora
			Baela
			Cyeana Cymella:50 Calla		
			Daella:200 Daena Daeoril Daenerys:200 Daenys Daenaera Daenora
			Elaena:200
			Gael
			Helaena Haera 
			Jaehaera
			Leaysa:50 Laena 
			Maera:50 Maega:50 Maegelle
			Naerys Rhae Rhaelle Rhaena:300 Rhaeys:50 Rhaenyra Rhaelinor:50 Rhaenys:300 Rhalla
			Saenrys:50 Syaella:50 Shaera Saera Shaena
			Visenya:200 Vaera:50 Vaella:200 Viserra
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 5
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 5
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
    Greyjoy_dyn = {
		graphical_cultures = { greyjoygfx } # portraits
		
		used_for_random = no
		allow_in_ruler_designer = no
		alternate_start = { always = no }
		color = { 0.15 0.15 0.15 }
		
		male_names = {
			Aerys:400 Aegon:1100 Aegor Aelix Aemon:400 Aemond Aenar Aenys:200 Aerion:300 Aeryn Aelor
			Baelor:200 Baelon:300
			Daegar:50 Daemon:400 Daeron:600 Daemion
			Gaemon:200 Haegon Haerys:50
			Jaekar:50 Jaehaerys:400 Jacaerys
			Lucerys Laenor
			Maelys Maegon Maegor:200 Maekar Maerys:50 Matarys Maelor
			Orys:50 Otherys:50
			Qoherys:50
			Rhaegar Rhaegel Rhaekar:50
			Valarr Viserys:400 Vaeron Vaekar:50 Vaermon Vaemond:50 Vaegon Valerion
		}
		female_names = {
			Allyria:50 Alearys:50 Aelinor Alysanne Aerea Alyssa Aelora
			Baela
			Cyeana Cymella:50 Calla		
			Daella:200 Daena Daeoril Daenerys:200 Daenys Daenaera Daenora
			Elaena:200
			Gael
			Helaena Haera 
			Jaehaera
			Leaysa:50 Laena 
			Maera:50 Maega:50 Maegelle
			Naerys Rhae Rhaelle Rhaena:300 Rhaeys:50 Rhaenyra Rhaelinor:50 Rhaenys:300 Rhalla
			Saenrys:50 Syaella:50 Shaera Saera Shaena
			Visenya:200 Vaera:50 Vaella:200 Viserra
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 5
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 5
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
    Martell_dyn = {
		graphical_cultures = { martellgfx } # portraits
		
		used_for_random = no
		allow_in_ruler_designer = no
		alternate_start = { always = no }
		color = { 0.15 0.15 0.15 }
		
		male_names = {
			Aerys:400 Aegon:1100 Aegor Aelix Aemon:400 Aemond Aenar Aenys:200 Aerion:300 Aeryn Aelor
			Baelor:200 Baelon:300
			Daegar:50 Daemon:400 Daeron:600 Daemion
			Gaemon:200 Haegon Haerys:50
			Jaekar:50 Jaehaerys:400 Jacaerys
			Lucerys Laenor
			Maelys Maegon Maegor:200 Maekar Maerys:50 Matarys Maelor
			Orys:50 Otherys:50
			Qoherys:50
			Rhaegar Rhaegel Rhaekar:50
			Valarr Viserys:400 Vaeron Vaekar:50 Vaermon Vaemond:50 Vaegon Valerion
		}
		female_names = {
			Allyria:50 Alearys:50 Aelinor Alysanne Aerea Alyssa Aelora
			Baela
			Cyeana Cymella:50 Calla		
			Daella:200 Daena Daeoril Daenerys:200 Daenys Daenaera Daenora
			Elaena:200
			Gael
			Helaena Haera 
			Jaehaera
			Leaysa:50 Laena 
			Maera:50 Maega:50 Maegelle
			Naerys Rhae Rhaelle Rhaena:300 Rhaeys:50 Rhaenyra Rhaelinor:50 Rhaenys:300 Rhalla
			Saenrys:50 Syaella:50 Shaera Saera Shaena
			Visenya:200 Vaera:50 Vaella:200 Viserra
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 5
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 5
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
    Tyrell_dyn = {
		graphical_cultures = { tyrellgfx } # portraits
		
		used_for_random = no
		allow_in_ruler_designer = no
		alternate_start = { always = no }
		color = { 0.15 0.15 0.15 }
		
		male_names = {
			Aerys:400 Aegon:1100 Aegor Aelix Aemon:400 Aemond Aenar Aenys:200 Aerion:300 Aeryn Aelor
			Baelor:200 Baelon:300
			Daegar:50 Daemon:400 Daeron:600 Daemion
			Gaemon:200 Haegon Haerys:50
			Jaekar:50 Jaehaerys:400 Jacaerys
			Lucerys Laenor
			Maelys Maegon Maegor:200 Maekar Maerys:50 Matarys Maelor
			Orys:50 Otherys:50
			Qoherys:50
			Rhaegar Rhaegel Rhaekar:50
			Valarr Viserys:400 Vaeron Vaekar:50 Vaermon Vaemond:50 Vaegon Valerion
		}
		female_names = {
			Allyria:50 Alearys:50 Aelinor Alysanne Aerea Alyssa Aelora
			Baela
			Cyeana Cymella:50 Calla		
			Daella:200 Daena Daeoril Daenerys:200 Daenys Daenaera Daenora
			Elaena:200
			Gael
			Helaena Haera 
			Jaehaera
			Leaysa:50 Laena 
			Maera:50 Maega:50 Maegelle
			Naerys Rhae Rhaelle Rhaena:300 Rhaeys:50 Rhaenyra Rhaelinor:50 Rhaenys:300 Rhalla
			Saenrys:50 Syaella:50 Shaera Saera Shaena
			Visenya:200 Vaera:50 Vaella:200 Viserra
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 5
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 5
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
    Hoare_dyn = {
		graphical_cultures = { hoaregfx } # portraits
		
		used_for_random = no
		allow_in_ruler_designer = no
		alternate_start = { always = no }
		color = { 0.15 0.15 0.15 }
		
		male_names = {
			Aerys:400 Aegon:1100 Aegor Aelix Aemon:400 Aemond Aenar Aenys:200 Aerion:300 Aeryn Aelor
			Baelor:200 Baelon:300
			Daegar:50 Daemon:400 Daeron:600 Daemion
			Gaemon:200 Haegon Haerys:50
			Jaekar:50 Jaehaerys:400 Jacaerys
			Lucerys Laenor
			Maelys Maegon Maegor:200 Maekar Maerys:50 Matarys Maelor
			Orys:50 Otherys:50
			Qoherys:50
			Rhaegar Rhaegel Rhaekar:50
			Valarr Viserys:400 Vaeron Vaekar:50 Vaermon Vaemond:50 Vaegon Valerion
		}
		female_names = {
			Allyria:50 Alearys:50 Aelinor Alysanne Aerea Alyssa Aelora
			Baela
			Cyeana Cymella:50 Calla		
			Daella:200 Daena Daeoril Daenerys:200 Daenys Daenaera Daenora
			Elaena:200
			Gael
			Helaena Haera 
			Jaehaera
			Leaysa:50 Laena 
			Maera:50 Maega:50 Maegelle
			Naerys Rhae Rhaelle Rhaena:300 Rhaeys:50 Rhaenyra Rhaelinor:50 Rhaenys:300 Rhalla
			Saenrys:50 Syaella:50 Shaera Saera Shaena
			Visenya:200 Vaera:50 Vaella:200 Viserra
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 5
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 5
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
    Arryn_dyn = {
		graphical_cultures = { arryngfx } # portraits
		
		used_for_random = no
		allow_in_ruler_designer = no
		alternate_start = { always = no }
		color = { 0.15 0.15 0.15 }
		
		male_names = {
			Aerys:400 Aegon:1100 Aegor Aelix Aemon:400 Aemond Aenar Aenys:200 Aerion:300 Aeryn Aelor
			Baelor:200 Baelon:300
			Daegar:50 Daemon:400 Daeron:600 Daemion
			Gaemon:200 Haegon Haerys:50
			Jaekar:50 Jaehaerys:400 Jacaerys
			Lucerys Laenor
			Maelys Maegon Maegor:200 Maekar Maerys:50 Matarys Maelor
			Orys:50 Otherys:50
			Qoherys:50
			Rhaegar Rhaegel Rhaekar:50
			Valarr Viserys:400 Vaeron Vaekar:50 Vaermon Vaemond:50 Vaegon Valerion
		}
		female_names = {
			Allyria:50 Alearys:50 Aelinor Alysanne Aerea Alyssa Aelora
			Baela
			Cyeana Cymella:50 Calla		
			Daella:200 Daena Daeoril Daenerys:200 Daenys Daenaera Daenora
			Elaena:200
			Gael
			Helaena Haera 
			Jaehaera
			Leaysa:50 Laena 
			Maera:50 Maega:50 Maegelle
			Naerys Rhae Rhaelle Rhaena:300 Rhaeys:50 Rhaenyra Rhaelinor:50 Rhaenys:300 Rhalla
			Saenrys:50 Syaella:50 Shaera Saera Shaena
			Visenya:200 Vaera:50 Vaella:200 Viserra
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 5
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 5
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
}