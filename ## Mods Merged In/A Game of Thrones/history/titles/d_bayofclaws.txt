900.1.1={
    liege="e_riverlands"
	law = succ_primogeniture
	law = cognatic_succession
}
#House Fisher
3306.1.1 = { holder=600174430 } # Ryman (nc) # House Fisher
3323.1.1 = { holder=601174430 } # Patrek (nc)
3331.1.1 = { holder=605174430 } # Addan (nc)
3390.1.1 = { holder=612174430 } # Tristan (nc)
3408.1.1 = { holder=613174430 } # Eyman (nc)
3442.1.1 = { holder=614174430 } # Edmure (nc)
3456.1.1 = { holder=0 } 

#House Mooton
6451.1.1={
	holder=501138 #Florian I
}
6488.1.1 = {
	holder=502138 #Florian II
}
6505.1.1 = {
	holder=503138 #Florian III
}
6527.1.1 = {
	holder=504138 #Florian IV
}
6538.1.1 = {
	holder=505138 #Florian V
}
6540.1.1 = {
	holder = 0
	law = succ_primogeniture
}

#House Harroway
7873.1.1 = { holder=344357 } # Halys (nc)
7882.1.1 = { holder=343357 } # Hendry (nc)
7904.1.1 = { holder=342357 } # Simon (nc)
7923.1.1 = { holder=341357 } # Perwyn (nc)
7957.1.1 = { holder=340357 } # Leslyn (nc)

7992.1.1 = { holder=6357 } # Symond (nc)
7998.2.1={
	liege="k_riverlands"
}
8009.1.1 = { holder=5357 } # Hoster (nc)
8032.1.1={
	holder=3357 # Lucas (C)
}
8042.1.1={ #Lucas became a Hand
	liege="e_iron_throne"
}

#House Darry
8044.6.1={
	holder=112137	
	liege="k_riverlands"
}
8062.1.1 = {
	holder=111137
}
8085.1.1 = {
	holder=110137
}
8100.1.1 = {
	holder=109137 # Lucias Darry
}
8119.1.1 = {
	holder=5500137 # Jonothor Darry
}
8129.7.1 = {
    holder=5510137 # Derrick Darry
}
8130.5.8={
	holder=107137 # Roland Darry
}
8131.1.1={
	holder=106137 # Desmond Darry
}
8158.8.1 = {
	holder=105137 # Jorin Darry
}
8193.1.1 = {
	holder=119137 # Deremond Darry
}
8196.3.1 = {
	holder=104137
}
8238.1.1 = {
	holder=103137
}
8263.1.1 = {
	holder=100137
}
8282.9.15 = {
	liege=0
}
8283.6.1={
	holder = 0 #Darrys stripped of power
}

#House Lannister
8299.11.12 = {
	holder=8190 #Lancel
	liege="k_riverlands"
}
8300.4.1 = {
	holder=9190 #Martyn
}

