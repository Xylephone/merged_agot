

# County Title
title = c_wythers

# Settlements
max_settlements = 3
b_bushy_hall = castle
b_nateby   = castle

#b_knutston = city





# Misc
culture = old_first_man
religion = old_gods

# History
1.1.1 = {
	b_bushy_hall = ca_asoiaf_reach_basevalue_1
	b_bushy_hall = ca_asoiaf_reach_basevalue_2
	b_bushy_hall = ca_asoiaf_reach_basevalue_3
}
1.1.1 = {
	b_nateby = ca_asoiaf_reach_basevalue_1
}
6700.1.1 = {
	culture = reachman
	religion = the_seven
}
