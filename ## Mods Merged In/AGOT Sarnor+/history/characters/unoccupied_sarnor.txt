#c_kyth
123477 = {
	name = " "
	
	religion="ruin_rel"
	culture="ruin"
	
	7958.1.1 = {
		birth="7958.1.1"
		effect = {
			if = {
				limit = { is_ruler = no }
				death = yes
			}	
			if = {
				limit = { is_ruler = yes }
				add_trait = ruin
				set_defacto_liege = THIS
				c_kyth = { 
					location = { 
						set_province_flag = ca_colony_5
						set_province_flag = ruined_province
					}
				}
				character_event = { id = unoccupied.101 }	
				create_character = {
					name = ""
					religion="ruin_rel"
					culture="ruin"	
					dynasty=none
					attributes = {
						martial = 0
						diplomacy = 0
						intrigue = 0
						stewardship = 0
						learning = 0
					}
					trait = ruin
				}
				new_character = {
					b_kyth1 = { gain_title = PREV }
				}
				create_character = {
					name = ""
					religion="ruin_rel"
					culture="ruin"	
					dynasty=none
					attributes = {
						martial = 0
						diplomacy = 0
						intrigue = 0
						stewardship = 0
						learning = 0
					}
					trait = ruin
				}
				new_character = {
					b_kyth2 = { gain_title = PREV }
				}
			}	
		}
	}
}
#c_mardosh
123478 = {
	name = " "
	
	religion="ruin_rel"
	culture="ruin"	
	
	7958.1.1 = {
		birth="7958.1.1"
		effect = {
			if = {
				limit = { is_ruler = no }
				death = yes
			}	
			if = {
				limit = { is_ruler = yes }
				add_trait = ruin
				set_defacto_liege = THIS
				c_mardosh = { 
					location = { 
						set_province_flag = ca_colony_6
						set_province_flag = ruined_province
					}
				}
				character_event = { id = unoccupied.101 }	
				
				create_character = {
					name = ""
					religion="ruin_rel"
					culture="ruin"	
					dynasty=none
					attributes = {
						martial = 0
						diplomacy = 0
						intrigue = 0
						stewardship = 0
						learning = 0
					}
					trait = ruin
				}
				new_character = {
					b_mardosh1 = { gain_title = PREV }
				}
				create_character = {
					name = ""
					religion="ruin_rel"
					culture="ruin"	
					dynasty=none
					attributes = {
						martial = 0
						diplomacy = 0
						intrigue = 0
						stewardship = 0
						learning = 0
					}
					trait = ruin
				}
				new_character = {
					b_mardosh2 = { gain_title = PREV }
				}
			}	
		}
	}
}
#c_n_hills_kasath
123479 = {
	name = " "
	
	religion="ruin_rel"
	culture="ruin"	
	
	7958.1.1 = {
		birth="7958.1.1"
		effect = {
			if = {
				limit = { is_ruler = no }
				death = yes
			}	
			if = {
				limit = { is_ruler = yes }
				add_trait = ruin
				set_defacto_liege = THIS
				c_n_hills_kasath = { 
					location = { 
						set_province_flag = ca_colony_3
						set_province_flag = ruined_province
					}
				}
				character_event = { id = unoccupied.101 }	
				
				create_character = {
					name = ""
					religion="ruin_rel"
					culture="ruin"	
					dynasty=none
					attributes = {
						martial = 0
						diplomacy = 0
						intrigue = 0
						stewardship = 0
						learning = 0
					}
					trait = ruin
				}
				new_character = {
					b_n_hills_kasath1 = { gain_title = PREV }
				}
				create_character = {
					name = ""
					religion="ruin_rel"
					culture="ruin"	
					dynasty=none
					attributes = {
						martial = 0
						diplomacy = 0
						intrigue = 0
						stewardship = 0
						learning = 0
					}
					trait = ruin
				}
				new_character = {
					b_n_hills_kasath2 = { gain_title = PREV }
				}
			}	
		}
	}
}